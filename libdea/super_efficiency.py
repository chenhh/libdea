# -*- coding: utf-8 -*-
"""
Authors: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

from __future__ import division
import numpy as np
from pyomo.environ import *
from pyomo.opt import TerminationCondition

from .basic_model import AbstractDEAModel
from . import (SOLVER_IO, KEEPFILES, DEFAULT_SOLVER)


class SuperEfficiencyCCR(AbstractDEAModel):
    def __init__(self, X, Y, solver=DEFAULT_SOLVER):
        """
        Parameters
        -----------
        X: numpy.array, shape: (n_dmu, dim_input)
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        Y: numpy.array, shape: (n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        solver: str,  the solver supported by pyomo (default: cplex)
        """
        super(SuperEfficiencyCCR, self).__init__(X, Y, solver,
                                                 "semi_positive")

    def solve_primal(self, dmu_idx, oriented="input"):
        """
        the solution in stage1 may not uniquely solutions of the in_slack and
        the out_slack. To determine whether the DMU is efficient or not, it
        requires to run the stage2 to ensure the sum of the in_slack and
        the out_slack is zero.

        Parameters:
        -------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}

        Returns
        ---------
        model_name: str, model_name,
        dmu_idx: integer, index of the DMU,
        efficiency": float, efficiency value of the DMU, 0< eff <=1
        intensity, numpy.array, shape:(n_dmu,),
            projection vector for input and output matrix to efficient frontier
        in_slack, numpy.array, shape:(dim_input,)
            distance of each dimension of the ith input from current position
            to the efficient frontier.
        out_slack, numpy.array, shape:(dim_output,)
            distance of each dimension of the jth output from current position
            to the efficient frontier.
        reference_dmu, list, reference DMUs on the efficient frontier.
        is_efficient, integer,
            - 1: efficient frontier
            - 0: technical efficiency
            - -1: inefficient
        projection_x, numpy.array, shape: (dim_input,)
        projection_y, numpy.array, shape: (dim_output,)
        """
        if not oriented in ('input', 'output'):
            raise ValueError('unknown super CCR oriented: {}'.format(oriented))

        # concrete model
        model_name = "Super_efficiency_CCR_{}".format(oriented)

        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.efficiency = Var(within=Reals)
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # disable dmu_idx th intensity elements
        instance.intensity[dmu_idx].value = 0
        instance.intensity[dmu_idx].fixed = True

        # common data in constraint
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        # common objective
        def stage1_objective_rule(model):
            return model.efficiency

        # stage 1
        if oriented == "input":
            # constraint
            def input_constraint_rule(model, idx):
                return (model.efficiency * self.X[dmu_idx, idx] -
                        model.in_slack[idx] - frontier_X[idx] == 0.)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                        model.out_slack[jdx] == 0.)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # objective
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                            sense=minimize)

        elif oriented == "output":
            # constraint
            def input_constraint_rule(model, idx):
                return (self.X[dmu_idx, idx] - frontier_X[idx] -
                        model.in_slack[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (model.efficiency * self.Y[dmu_idx, jdx] -
                        frontier_Y[jdx] + model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # stage 1 objective
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                            sense=maximize)

        # solve stage 1
        instance = self._solve_model_feasible(instance, model_name)

        # get efficiency
        efficiency = instance.efficiency.value

        # reset constraint and objective
        instance.efficiency.fixed = True
        instance.objective1.deactivate()

        def stage2_objective_rule(model):
            in_slack_sum = sum(model.in_slack[idx] for idx in self.input_arr)
            out_slack_sum = sum(model.out_slack[jdx] for jdx in self.output_arr)
            return in_slack_sum + out_slack_sum

        # both input and output oriented are to maximize slack sum
        instance.objective2 = Objective(rule=stage2_objective_rule,
                                        sense=maximize)
        instance.objective2.activate()

        # solve stage 2
        instance = self._solve_model_feasible(instance, model_name)

        # release variable
        instance.efficiency.fixed = False
        instance.intensity[dmu_idx].fixed = False

        # extract the results
        intensity = np.fromiter((
            instance.intensity[rdx].value
            for rdx in self.dmu_arr), np.float)
        reference_dmu = self.dmu_arr[intensity > 0]

        in_slack = np.fromiter((
            instance.in_slack[idx].value
            for idx in self.input_arr), np.float)
        out_slack = np.fromiter((
            instance.out_slack[jdx].value
            for jdx in self.output_arr), np.float)

        # projection
        if oriented == "input":
            projection_x = efficiency * self.X[dmu_idx] - in_slack
            projection_y = self.Y[dmu_idx] + out_slack
        elif oriented == "output":
            projection_x = self.X[dmu_idx] - in_slack
            projection_y = efficiency * self.Y[dmu_idx] + out_slack

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "efficiency": efficiency,
            "intensity": intensity,
            "in_slack": in_slack,
            "out_slack": out_slack,
            "reference_dmu": reference_dmu,
            "projection_x": projection_x,
            "projection_y": projection_y,
        }

    def solve_batch_primal(self, oriented="input"):

        if oriented in ("input", "output"):
            model_name = "Super_efficiency_CCR_batch_{}".format(oriented)
        else:
            raise ValueError(
                "unknown Super_efficiency_CCR_batch oriented: {}".format(
                    oriented))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # result arrays
        efficiency_arr = np.zeros(self.n_dmu)
        intensity_arr = np.zeros((self.n_dmu, self.n_dmu))
        reference_dmu_arr = []
        in_slack_arr = np.zeros((self.n_dmu, self.dim_input))
        out_slack_arr = np.zeros((self.n_dmu, self.dim_output))
        projection_x_arr = np.zeros((self.n_dmu, self.dim_input))
        projection_y_arr = np.zeros((self.n_dmu, self.dim_output))

        # decision variables
        instance.efficiency = Var(within=Reals)
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # common data in constraint
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        # stage 1 common objective
        def stage1_objective_rule(model):
            return model.efficiency

        for dmu_idx in self.dmu_arr:
            # disable dmu_idx th intensity elements
            instance.intensity[dmu_idx].value = 0
            instance.intensity[dmu_idx].fixed = True

            if oriented == "input":
                # constraint
                def input_constraint_rule(model, idx):
                    return (model.efficiency * self.X[dmu_idx, idx] -
                            frontier_X[idx] - model.in_slack[idx] == 0.)

                instance.input_constraint = Constraint(
                    self.input_arr, rule=input_constraint_rule)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                            model.out_slack[jdx] == 0.)

                instance.output_constraint = Constraint(
                    self.output_arr, rule=output_constraint_rule)

                # objective
                instance.objective1 = Objective(rule=stage1_objective_rule,
                                                sense=minimize)

            elif oriented == "output":
                # constraint
                def input_constraint_rule(model, idx):
                    return (self.X[dmu_idx, idx] - frontier_X[idx] -
                            model.in_slack[idx] == 0)

                instance.input_constraint = Constraint(
                    self.input_arr, rule=input_constraint_rule)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (model.efficiency * self.Y[dmu_idx, jdx] -
                            frontier_Y[jdx] + model.out_slack[jdx] == 0)

                instance.output_constraint = Constraint(
                    self.output_arr, rule=output_constraint_rule)

                # objective
                instance.objective1 = Objective(rule=stage1_objective_rule,
                                                sense=maximize)

            # solve stage 1
            instance = self._solve_model_feasible(instance, model_name)

            # get efficiency
            efficiency_arr[dmu_idx] = instance.efficiency.value

            # reset constraint and objective
            instance.efficiency.fixed = True
            instance.objective1.deactivate()

            def stage2_objective_rule(model):
                in_slack_sum = sum(model.in_slack[idx]
                                   for idx in self.input_arr)
                out_slack_sum = sum(model.out_slack[jdx]
                                    for jdx in self.output_arr)
                return in_slack_sum + out_slack_sum

            # both input and output oriented are to maximize slack sum
            instance.objective2 = Objective(rule=stage2_objective_rule,
                                            sense=maximize)
            instance.objective2.activate()

            # solve stage 2
            instance = self._solve_model_feasible(instance, model_name)

            # release variables
            instance.efficiency.fixed = False
            instance.intensity[dmu_idx].fixed = False

            # extract result
            intensity_arr[dmu_idx] = np.fromiter((
                instance.intensity[rdx].value
                for rdx in self.dmu_arr), np.float)
            reference_dmu_arr.append(self.dmu_arr[intensity_arr[dmu_idx] > 0])

            in_slack_arr[dmu_idx] = np.fromiter((
                instance.in_slack[idx].value
                for idx in self.input_arr), np.float)
            out_slack_arr[dmu_idx] = np.fromiter((
                instance.out_slack[jdx].value
                for jdx in self.output_arr), np.float)

            # projection
            if oriented == "input":
                projection_x_arr[dmu_idx] = (efficiency_arr[dmu_idx] *
                                             self.X[dmu_idx] -
                                             in_slack_arr[dmu_idx])
                projection_y_arr[dmu_idx] = (self.Y[dmu_idx] +
                                             out_slack_arr[dmu_idx])
            elif oriented == "output":
                projection_x_arr[dmu_idx] = (self.X[dmu_idx] -
                                             in_slack_arr[dmu_idx])
                projection_y_arr[dmu_idx] = (efficiency_arr[dmu_idx] *
                                             self.Y[dmu_idx] +
                                             out_slack_arr[dmu_idx])

            # delete constraint and resolve the problem
            instance.del_component("input_constraint")
            instance.del_component("input_constraint_index")
            instance.del_component("output_constraint")
            instance.del_component("output_constraint_index")
            instance.del_component("objective1")
            instance.del_component("objective2")

        return {
            "model_name": model_name,
            "efficiency_arr": efficiency_arr,
            "intensity_arr": intensity_arr,
            "in_slack_arr": in_slack_arr,
            "out_slack_arr": out_slack_arr,
            "reference_dmu_arr": reference_dmu_arr,
            "projection_x_arr": projection_x_arr,
            "projection_y_arr": projection_y_arr,
        }


class SuperEfficiencyBCC(AbstractDEAModel):
    """ the VRS (BCC) may have no solution (infeasible). """

    def __init__(self, X, Y, solver=DEFAULT_SOLVER):
        """
        Parameters
        -----------
        X: numpy.array, shape: (n_dmu, dim_input)
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        Y: numpy.array, shape: (n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        solver: str,  the solver supported by pyomo (default: cplex)
        """
        super(SuperEfficiencyBCC, self).__init__(X, Y, solver,
                                                 "semi_positive")

    def solve_primal(self, dmu_idx, oriented="input"):
        """
        Parameters:
        -------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}

        Returns
        ---------
        model_name: str, model_name,
        dmu_idx: integer, index of the DMU,
        efficiency": float, efficiency value of the DMU
        intensity, numpy.array, shape:(n_dmu,),
            projection vector for input and output matrix to efficient frontier
        in_slack, numpy.array, shape:(dim_input,)
            distance of each dimension of the ith input from current position
            to the efficient frontier.
        out_slack, numpy.array, shape:(dim_output,)
            distance of each dimension of the jth output from current position
            to the efficient frontier.
        reference_dmu, list, reference DMUs on the efficient frontier.
        is_efficient, integer,
            - 1: efficient frontier
            - 0: technical efficiency
            - -1: inefficient
        projection_x, numpy.array, shape: (dim_input,)
        projection_y, numpy.array, shape: (dim_output,)
        """
        if oriented in ("input", "output"):
            model_name = "Super_efficiency_BCC_primal_{}".format(oriented)
        else:
            raise ValueError("unknown super BCC primal oriented: {}".format(
                oriented))

        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.efficiency = Var(within=Reals)
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # disable dmu_idx th intensity elements
        instance.intensity[dmu_idx].value = 0
        instance.intensity[dmu_idx].fixed = True

        # common data in constraint
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        # common constraint
        def cvx_boundary_constraint_rule(model):
            return (sum(model.intensity[rdx] for rdx in self.dmu_arr) == 1)

        instance.cvx_boundary_constraint = Constraint(
            rule=cvx_boundary_constraint_rule)

        # common objective
        def stage1_objective_rule(model):
            return instance.efficiency

        # stage1
        if oriented == "input":
            # constraint
            def input_constraint_rule(model, idx):
                return (model.efficiency * self.X[dmu_idx, idx] -
                        model.in_slack[idx] - frontier_X[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                        model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # objective
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                            sense=minimize)

        elif oriented == "output":
            # constraint
            def input_constraint_rule(model, idx):
                return (self.X[dmu_idx, idx] - frontier_X[idx] -
                        model.in_slack[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (model.efficiency * self.Y[dmu_idx, jdx] -
                        frontier_Y[jdx] + model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # objective
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                            sense=maximize)

        # solve stage 1
        opt = SolverFactory(self.solver, solver_io=SOLVER_IO)
        results = opt.solve(instance, keepfiles=KEEPFILES)
        instance.solutions.load_from(results)

        # the super BCC model may be infeasible
        if (results.solver.termination_condition ==
                TerminationCondition.infeasible):
            return None

        # get efficiency
        efficiency = instance.efficiency.value

        # reset constraint and objective
        instance.efficiency.fixed = True
        instance.objective1.deactivate()

        def stage2_objective_rule(model):
            in_slack_sum = sum(model.in_slack[idx] for idx in self.input_arr)
            out_slack_sum = sum(model.out_slack[jdx] for jdx in self.output_arr)
            return in_slack_sum + out_slack_sum

        # both input and output oriented are to maximize slack sum
        instance.objective2 = Objective(rule=stage2_objective_rule,
                                        sense=maximize)
        instance.objective2.activate()

        # solve stage 2
        opt = SolverFactory(self.solver, solver_io=SOLVER_IO)
        results = opt.solve(instance, keepfiles=KEEPFILES)
        instance.solutions.load_from(results)
        # display(instance)

        # release variables
        instance.efficiency.fixed = False
        instance.intensity[dmu_idx].fixed = False

        # extract the results
        intensity = np.fromiter((
            instance.intensity[rdx].value
            for rdx in self.dmu_arr), np.float)

        reference_dmu = self.dmu_arr[intensity > 0]

        in_slack = np.fromiter((
            instance.in_slack[idx].value
            for idx in self.input_arr), np.float)
        out_slack = np.fromiter((
            instance.out_slack[jdx].value
            for jdx in self.output_arr), np.float)

        # projection
        if oriented == "input":
            projection_x = efficiency * self.X[dmu_idx] - in_slack
            projection_y = self.Y[dmu_idx] + out_slack
        elif oriented == "output":
            projection_x = self.X[dmu_idx] - in_slack
            projection_y = efficiency * self.Y[dmu_idx] + out_slack

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "efficiency": efficiency,
            "intensity": intensity,
            "in_slack": in_slack,
            "out_slack": out_slack,
            "reference_dmu": reference_dmu,
            "projection_x": projection_x,
            "projection_y": projection_y,
        }

    def solve_batch_primal(self, oriented="input"):
        """
        Parameters:
        ---------------
        oriented: str, {"input", "output"}.
        """

        if oriented in ("input", "output"):
            model_name = "Super_efficiency_BCC_batch_{}".format(oriented)
        else:
            raise ValueError("unknown super BCC batch dual oriented: {}".format(
                oriented))

        instance = ConcreteModel(name=model_name)

        # result arrays
        feasible_arr = [False for rdx in self.dmu_arr]
        efficiency_arr = np.zeros(self.n_dmu)
        intensity_arr = np.zeros((self.n_dmu, self.n_dmu))
        reference_dmu_arr = []
        in_slack_arr = np.zeros((self.n_dmu, self.dim_input))
        out_slack_arr = np.zeros((self.n_dmu, self.dim_output))
        projection_x_arr = np.zeros((self.n_dmu, self.dim_input))
        projection_y_arr = np.zeros((self.n_dmu, self.dim_output))

        # decision variables
        instance.efficiency = Var(within=Reals)
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)


        # common data in constraint
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        # common objective
        def stage1_objective_rule(model):
            return model.efficiency

        for dmu_idx in self.dmu_arr:
            # disable dmu_idx th intensity elements
            instance.intensity[dmu_idx].value = 0
            instance.intensity[dmu_idx].fixed = True

            # constraint
            def cvx_boundary_constraint_rule(model):
                return (sum(model.intensity[rdx] for rdx in self.dmu_arr) == 1)

            instance.cvx_boundary_constraint = Constraint(
                rule=cvx_boundary_constraint_rule)

            if oriented == "input":
                # constraint
                def input_constraint_rule(model, idx):
                    return (model.efficiency * self.X[dmu_idx, idx] -
                            frontier_X[idx] - model.in_slack[idx] == 0.)

                instance.input_constraint = Constraint(
                    self.input_arr, rule=input_constraint_rule)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                            model.out_slack[jdx] == 0.)

                instance.output_constraint = Constraint(
                    self.output_arr, rule=output_constraint_rule)

                # objective
                instance.objective1 = Objective(rule=stage1_objective_rule,
                                                sense=minimize)

            elif oriented == "output":
                # constraint
                def input_constraint_rule(model, idx):
                    return (self.X[dmu_idx, idx] - frontier_X[idx] -
                            model.in_slack[idx] == 0)

                instance.input_constraint = Constraint(
                    self.input_arr, rule=input_constraint_rule)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (model.efficiency * self.Y[dmu_idx, jdx] -
                            frontier_Y[jdx] + model.out_slack[jdx] == 0)

                instance.output_constraint = Constraint(
                    self.output_arr, rule=output_constraint_rule)

                # objective
                instance.objective1 = Objective(rule=stage1_objective_rule,
                                                sense=maximize)

            # solve stage 1
            opt = SolverFactory(self.solver, solver_io=SOLVER_IO)
            results = opt.solve(instance, keepfiles=KEEPFILES)
            instance.solutions.load_from(results)

            # infeasible
            if (results.solver.termination_condition ==
                    TerminationCondition.infeasible):
                # reset variable
                instance.intensity[dmu_idx].fixed = False

                # delete constraint and goto next iteration
                instance.del_component("cvx_boundary_constraint")
                instance.del_component("input_constraint")
                instance.del_component("input_constraint_index")
                instance.del_component("output_constraint")
                instance.del_component("output_constraint_index")
                instance.del_component("objective1")
                continue
            else:
                # feasible
                feasible_arr[dmu_idx] = True

            # get efficiency
            efficiency_arr[dmu_idx] = instance.efficiency.value

            # reset constraint and objective
            instance.efficiency.fixed = True
            instance.objective1.deactivate()

            def stage2_objective_rule(model):
                in_slack_sum = sum(model.in_slack[idx]
                                   for idx in self.input_arr)
                out_slack_sum = sum(model.out_slack[jdx]
                                    for jdx in self.output_arr)
                return in_slack_sum + out_slack_sum

            # both input and output oriented are to maximize slack sum
            instance.objective2 = Objective(rule=stage2_objective_rule,
                                            sense=maximize)
            instance.objective2.activate()

            # solve stage 2
            opt = SolverFactory(self.solver, solver_io=SOLVER_IO)
            results = opt.solve(instance, keepfiles=KEEPFILES)
            instance.solutions.load_from(results)
            # display(instance)

            if (results.solver.termination_condition ==
                    TerminationCondition.infeasible):
                return None

            # release variable
            instance.intensity[dmu_idx].fixed = False
            instance.efficiency.fixed = False

            # extract result
            intensity_arr[dmu_idx] = np.fromiter((
                instance.intensity[rdx].value
                for rdx in self.dmu_arr), np.float)

            reference_dmu_arr.append(self.dmu_arr[intensity_arr[dmu_idx] > 0])

            in_slack_arr[dmu_idx] = np.fromiter((
                instance.in_slack[idx].value
                for idx in self.input_arr), np.float)
            out_slack_arr[dmu_idx] = np.fromiter((
                instance.out_slack[jdx].value
                for jdx in self.output_arr), np.float)

            # projection
            if oriented == "input":
                projection_x_arr[dmu_idx] = (efficiency_arr[dmu_idx] *
                                             self.X[dmu_idx] -
                                             in_slack_arr[dmu_idx])
                projection_y_arr[dmu_idx] = (self.Y[dmu_idx] +
                                             out_slack_arr[dmu_idx])
            elif oriented == "output":
                projection_x_arr[dmu_idx] = (self.X[dmu_idx] -
                                             in_slack_arr[dmu_idx])
                projection_y_arr[dmu_idx] = (efficiency_arr[dmu_idx] *
                                             self.Y[dmu_idx] +
                                             out_slack_arr[dmu_idx])


            # delete constraint and resolve the problem
            instance.del_component("cvx_boundary_constraint")
            instance.del_component("input_constraint")
            instance.del_component("input_constraint_index")
            instance.del_component("output_constraint")
            instance.del_component("output_constraint_index")
            instance.del_component("objective1")
            instance.del_component("objective2")

        return {
            "model_name": model_name,
            "feasible_arr": feasible_arr,
            "efficiency_arr": efficiency_arr,
            "intensity_arr": intensity_arr,
            "in_slack_arr": in_slack_arr,
            "out_slack_arr": out_slack_arr,
            "reference_dmu_arr": reference_dmu_arr,
            "projection_x_arr": projection_x_arr,
            "projection_y_arr": projection_y_arr,
        }


class SuperEfficiencySBM(AbstractDEAModel):
    """
    Tone02, unit invariant, SBM likely，
    the Super_efficiency SBM VRS must have solution, but
    its input and output oriented model may infeasible.
     """

    def __init__(self, X, Y, solver="cplex"):
        """
        Parameters
        -----------
        X: numpy.array, shape: (n_dmu, dim_input)
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        Y: numpy.array, shape: (n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        solver: str,  the solver supported by pyomo (default: cplex)
        """
        super(SuperEfficiencySBM, self).__init__(X, Y, solver,
                                                 "semi_positive")

    def solve_primal(self, dmu_idx, oriented=None):
        """
        Parameters:
        -------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {None, "input", "output"}
        """
        if oriented in ('input', 'output'):
            return self._solve_primal_oriented(dmu_idx, oriented)

        model_name = "Super_efficiency_SBM_primal_CRS"
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.auxiliary = Var(within=PositiveReals)
        instance.aux_intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.aux_in_ratio = Var(self.input_arr, within=NonNegativeReals)
        instance.aux_out_ratio = Var(self.output_arr,
                                     within=NonNegativeReals)

        # disable dmu_idx th intensity elements
        instance.aux_intensity[dmu_idx].value = 0
        instance.aux_intensity[dmu_idx].fixed = True

        # constraint
        def aux_out_ratio_constraint_rule(model):
            aux_out_ratio_sum = sum(model.aux_out_ratio[jdx]
                                    for jdx in self.output_arr)
            aux_out_ratio_mean = aux_out_ratio_sum / self.dim_output
            return model.auxiliary - aux_out_ratio_mean - 1. == 0

        instance.aux_out_ratio_constraint = Constraint(
            rule=aux_out_ratio_constraint_rule)

        # constraint
        def aux_input_constraint_rule(model, idx):
            aux_frontier = sum(self.X[rdx, idx] * model.aux_intensity[rdx]
                               for rdx in self.dmu_arr)
            return (aux_frontier -
                    model.aux_in_ratio[idx] * self.X[dmu_idx, idx] -
                    model.auxiliary * self.X[dmu_idx, idx] <= 0)

        instance.aux_input_constraint = Constraint(
            self.input_arr, rule=aux_input_constraint_rule)

        # constraint
        def aux_output_constraint_rule(model, jdx):
            aux_frontier = sum(self.Y[rdx, jdx] * model.aux_intensity[rdx]
                               for rdx in self.dmu_arr)
            return (aux_frontier +
                    model.aux_out_ratio[jdx] * self.Y[dmu_idx, jdx] -
                    model.auxiliary * self.Y[dmu_idx, jdx] >= 0)

        instance.aux_output_constraint = Constraint(
            self.output_arr, rule=aux_output_constraint_rule)

        def objective_rule(model):
            aux_in_ratio_sum = sum(model.aux_in_ratio[idx]
                                   for idx in self.input_arr)
            aux_in_ratio_mean = aux_in_ratio_sum / self.dim_input
            return model.auxiliary + aux_in_ratio_mean

        instance.objective = Objective(rule=objective_rule, sense=minimize)

        # solve
        instance = self._solve_model_feasible(instance, model_name)

        # reset variable
        instance.aux_intensity[dmu_idx].fixed = False

        # intensity to original unit and computing the reference dmu_idx
        aux = instance.auxiliary.value
        intensity = np.fromiter(
            (instance.aux_intensity[rdx].value / aux
             for rdx in self.dmu_arr), np.float)

        reference_dmu = self.dmu_arr[intensity > 0]

        in_ratio = np.fromiter(
            (instance.aux_in_ratio[idx].value / aux
             for idx in self.input_arr), np.float)
        out_ratio = np.fromiter(
            (instance.aux_out_ratio[jdx].value / aux
             for jdx in self.output_arr), np.float)

        # data projection
        projection_x = self.X[dmu_idx] * (1 + in_ratio)
        projection_y = self.Y[dmu_idx] * (1 - out_ratio)

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "auxiliary": aux,
            "projection_x": projection_x,
            "projection_y": projection_y,
            "intensity": intensity,
            "in_ratio": in_ratio,
            "out_ratio": out_ratio,
            "reference_dmu": reference_dmu,
            "efficiency": instance.objective(),
        }

    def _solve_primal_oriented(self, dmu_idx, oriented="input"):
        if oriented in ("input", "output"):
            model_name = "Super_efficiency_SBM_primal_CRS_{}_oriented".format(
                oriented)

        # concrete model
        instance = ConcreteModel(name=model_name)

        # common decision variables
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)

        # disable dmu_idx th intensity elements
        instance.intensity[dmu_idx].value = 0
        instance.intensity[dmu_idx].fixed = True

        # common data in constraint
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        if oriented == "input":
            # input decision variable
            instance.in_ratio = Var(self.input_arr, within=NonNegativeReals)

            # constraint
            def input_constraint_rule(model, idx):
                return (frontier_X[idx] -
                        model.in_ratio[idx] * self.X[dmu_idx, idx] -
                        self.X[dmu_idx, idx] <= 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (frontier_Y[jdx] - self.Y[dmu_idx, jdx] >= 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            def objective_rule(model):
                in_ratio_sum = sum(model.in_ratio[idx]
                                   for idx in self.input_arr)
                in_ratio_mean = in_ratio_sum / self.dim_input
                return 1 + in_ratio_mean

            instance.objective = Objective(rule=objective_rule, sense=minimize)

        elif oriented == "output":
            # output decision variable
            instance.out_ratio = Var(self.output_arr,
                                     within=NonNegativeReals)

            # constraint
            def input_constraint_rule(model, idx):
                return (frontier_X[idx] - self.X[dmu_idx, idx] <= 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (frontier_Y[jdx] +
                        model.out_ratio[jdx] * self.Y[dmu_idx, jdx] -
                        self.Y[dmu_idx, jdx] >= 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            def objective_rule(model):
                out_ratio_sum = sum(model.out_ratio[jdx]
                                    for jdx in self.output_arr)
                out_ratio_mean = out_ratio_sum / self.dim_output
                return (1 - out_ratio_mean)

            instance.objective = Objective(rule=objective_rule, sense=maximize)

        # solve
        instance = self._solve_model_feasible(instance, model_name)

        # reset variable
        instance.intensity[dmu_idx].fixed = False

        # extract results
        intensity = np.fromiter(
            (instance.intensity[rdx].value
             for rdx in self.dmu_arr), np.float)

        reference_dmu = self.dmu_arr[intensity > 0]

        # data projection
        if oriented == "input":
            in_ratio = np.fromiter(
                (instance.in_ratio[idx].value
                 for idx in self.input_arr), np.float)
            out_ratio = 0
            efficiency = instance.objective()

            projection_x = self.X[dmu_idx] * (1 + in_ratio)
            projection_y = self.Y[dmu_idx]
        elif oriented == "output":
            in_ratio = 0
            out_ratio = np.fromiter(
                (instance.out_ratio[idx].value
                 for idx in self.output_arr), np.float)

            efficiency = 1. / instance.objective()

            projection_x = self.X[dmu_idx]
            projection_y = self.Y[dmu_idx] * (1 + out_ratio)

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "projection_x": projection_x,
            "projection_y": projection_y,
            "intensity": intensity,
            "in_ratio": in_ratio,
            "out_ratio": out_ratio,
            "reference_dmu": reference_dmu,
            "efficiency": efficiency,
        }

    def solve_batch_primal(self, oriented=None):
        """
        Parameters:
        -------------
        oriented: str, {None, "input", "output"}
        """
        if oriented in ("input", "output"):
            return self._solve_batch_primal_oriented(oriented)

        # concrete model
        model_name = "Super_efficiency_SBM_batch_primal_CRS"
        instance = ConcreteModel(name=model_name)

        # result arrays
        efficiency_arr = np.zeros(self.n_dmu)
        intensity_arr = np.zeros((self.n_dmu, self.n_dmu))
        reference_dmu_arr = []
        aux_arr = np.zeros(self.n_dmu)
        in_ratio_arr = np.zeros((self.n_dmu, self.dim_input))
        out_ratio_arr = np.zeros((self.n_dmu, self.dim_output))
        projection_x_arr = np.zeros((self.n_dmu, self.dim_input))
        projection_y_arr = np.zeros((self.n_dmu, self.dim_output))

        # decision variables
        instance.auxiliary = Var(within=PositiveReals)
        instance.aux_intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.aux_in_ratio = Var(self.input_arr, within=NonNegativeReals)
        instance.aux_out_ratio = Var(self.output_arr,
                                     within=NonNegativeReals)

        # common constraint
        def aux_out_ratio_constraint_rule(model):
            aux_out_ratio_sum = sum(model.aux_out_ratio[jdx]
                                    for jdx in self.output_arr)
            aux_out_ratio_mean = aux_out_ratio_sum / self.dim_output
            return model.auxiliary - aux_out_ratio_mean - 1. == 0

        instance.aux_out_ratio_constraint = Constraint(
            rule=aux_out_ratio_constraint_rule)

        # common objective
        def objective_rule(model):
            aux_in_ratio_sum = sum(model.aux_in_ratio[idx]
                                   for idx in self.input_arr)
            aux_in_ratio_mean = aux_in_ratio_sum / self.dim_input
            return model.auxiliary + aux_in_ratio_mean

        instance.objective = Objective(rule=objective_rule, sense=minimize)

        # common data in constraint
        aux_frontier_X = [sum(self.X[rdx, idx] * instance.aux_intensity[rdx]
                              for rdx in self.dmu_arr)
                          for idx in self.input_arr]
        aux_frontier_Y = [sum(self.Y[rdx, jdx] * instance.aux_intensity[rdx]
                              for rdx in self.dmu_arr)
                          for jdx in self.output_arr]

        for dmu_idx in self.dmu_arr:
            # disable dmu_idx th intensity elements
            instance.aux_intensity[dmu_idx].value = 0
            instance.aux_intensity[dmu_idx].fixed = True

            # constraint
            def aux_input_constraint_rule(model, idx):
                return (aux_frontier_X[idx] -
                        model.aux_in_ratio[idx] * self.X[dmu_idx, idx] -
                        model.auxiliary * self.X[dmu_idx, idx] <= 0)

            instance.aux_input_constraint = Constraint(
                self.input_arr, rule=aux_input_constraint_rule)

            # constraint
            def aux_output_constraint_rule(model, jdx):
                return (aux_frontier_Y[jdx] +
                        model.aux_out_ratio[jdx] * self.Y[dmu_idx, jdx] -
                        model.auxiliary * self.Y[dmu_idx, jdx] >= 0)

            instance.aux_output_constraint = Constraint(
                self.output_arr, rule=aux_output_constraint_rule)

            # solve
            instance = self._solve_model_feasible(instance, model_name)

            # reset variable
            instance.aux_intensity[dmu_idx].fixed = False

            # extract results
            # intensity to original unit and computing the reference dmu_idx
            efficiency = instance.objective()
            aux = instance.auxiliary.value
            intensity = np.fromiter(
                (instance.aux_intensity[rdx].value / aux
                 for rdx in self.dmu_arr), np.float)

            reference_dmu = self.dmu_arr[intensity > 0]

            in_ratio = np.fromiter(
                (instance.aux_in_ratio[idx].value / aux
                 for idx in self.input_arr), np.float)
            out_ratio = np.fromiter(
                (instance.aux_out_ratio[jdx].value / aux
                 for jdx in self.output_arr), np.float)

            # data projection
            projection_x = self.X[dmu_idx] * (1 + in_ratio)
            projection_y = self.Y[dmu_idx] * (1 - out_ratio)

            efficiency_arr[dmu_idx] = efficiency
            aux_arr[dmu_idx] = aux
            intensity_arr[dmu_idx] = intensity
            reference_dmu_arr.append(reference_dmu)
            in_ratio_arr[dmu_idx] = in_ratio
            out_ratio_arr[dmu_idx] = out_ratio
            projection_x_arr[dmu_idx] = projection_x
            projection_y_arr[dmu_idx] = projection_y

            # delete constraint and resolve the problem
            instance.del_component("aux_input_constraint")
            instance.del_component("aux_input_constraint_index")
            instance.del_component("aux_output_constraint")
            instance.del_component("aux_output_constraint_index")

        return {
            "model_name": model_name,
            "auxiliary_arr": aux_arr,
            "efficiency_arr": efficiency_arr,
            "intensity_arr": intensity_arr,
            "in_ratio_arr": in_ratio_arr,
            "out_ratio_arr": out_ratio_arr,
            "reference_dmu_arr": reference_dmu_arr,
            "projection_x_arr": projection_x_arr,
            "projection_y_arr": projection_y_arr,
        }

    def _solve_batch_primal_oriented(self, oriented):
        """
        Parameters:
        -------------
        oriented: str, {"input", "output"}
        """
        if oriented in ("input", "output"):
            model_name = "Super_efficiency_SBM_batch_primal" \
                         "_CRS_{}_oriented".format(oriented)

        # concrete model
        instance = ConcreteModel(name=model_name)

        # result arrays
        efficiency_arr = np.zeros(self.n_dmu)
        intensity_arr = np.zeros((self.n_dmu, self.n_dmu))
        reference_dmu_arr = []
        in_ratio_arr = np.zeros((self.n_dmu, self.dim_input))
        out_ratio_arr = np.zeros((self.n_dmu, self.dim_output))
        projection_x_arr = np.zeros((self.n_dmu, self.dim_input))
        projection_y_arr = np.zeros((self.n_dmu, self.dim_output))

        # common decision variables
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)

        if oriented == "input":
            instance.in_ratio = Var(self.input_arr, within=NonNegativeReals)

            # objective
            def objective_rule(model):
                in_ratio_sum = sum(model.in_ratio[idx]
                                   for idx in self.input_arr)
                in_ratio_mean = in_ratio_sum / self.dim_input
                return 1 + in_ratio_mean

            instance.objective = Objective(rule=objective_rule, sense=minimize)


        elif oriented == "output":
            instance.out_ratio = Var(self.output_arr,
                                     within=NonNegativeReals)
            # objective
            def objective_rule(model):
                out_ratio_sum = sum(model.out_ratio[jdx]
                                    for jdx in self.output_arr)
                out_ratio_mean = out_ratio_sum / self.dim_output
                return (1 - out_ratio_mean)

            instance.objective = Objective(rule=objective_rule, sense=maximize)

        # common data in constraint
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        for dmu_idx in self.dmu_arr:
            # disable dmu_idx th intensity elements
            instance.intensity[dmu_idx].value = 0
            instance.intensity[dmu_idx].fixed = True

            if oriented == "input":
                # constraint
                def input_constraint_rule(model, idx):
                    return (frontier_X[idx] -
                            model.in_ratio[idx] * self.X[dmu_idx, idx] -
                            self.X[dmu_idx, idx] <= 0)

                instance.input_constraint = Constraint(
                    self.input_arr, rule=input_constraint_rule)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (frontier_Y[jdx] - self.Y[dmu_idx, jdx] >= 0)

                instance.output_constraint = Constraint(
                    self.output_arr, rule=output_constraint_rule)

            elif oriented == "output":
                # constraint
                def input_constraint_rule(model, idx):
                    return (frontier_X[idx] - self.X[dmu_idx, idx] <= 0)

                instance.input_constraint = Constraint(
                    self.input_arr, rule=input_constraint_rule)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (frontier_Y[jdx] +
                            model.out_ratio[jdx] * self.Y[dmu_idx, jdx] -
                            self.Y[dmu_idx, jdx] >= 0)

                instance.output_constraint = Constraint(
                    self.output_arr, rule=output_constraint_rule)

            # solve
            instance = self._solve_model_feasible(instance, model_name)

            # reset variable
            instance.intensity[dmu_idx].fixed = False

            # extract results
            intensity = np.fromiter(
                (instance.intensity[rdx].value
                 for rdx in self.dmu_arr), np.float)

            reference_dmu = self.dmu_arr[intensity > 0]

            # data projection
            if oriented == "input":
                in_ratio = np.fromiter(
                    (instance.in_ratio[idx].value
                     for idx in self.input_arr), np.float)
                out_ratio = 0
                efficiency = instance.objective()

                projection_x = self.X[dmu_idx] * (1 + in_ratio)
                projection_y = self.Y[dmu_idx]
            elif oriented == "output":
                in_ratio = 0
                out_ratio = np.fromiter(
                    (instance.out_ratio[idx].value
                     for idx in self.output_arr), np.float)

                efficiency = 1. / instance.objective()

                projection_x = self.X[dmu_idx]
                projection_y = self.Y[dmu_idx] * (1 + out_ratio)

            efficiency_arr[dmu_idx] = efficiency
            intensity_arr[dmu_idx] = intensity
            reference_dmu_arr.append(reference_dmu)
            in_ratio_arr[dmu_idx] = in_ratio
            out_ratio_arr[dmu_idx] = out_ratio
            projection_x_arr[dmu_idx] = projection_x
            projection_y_arr[dmu_idx] = projection_y

            # delete constraint and resolve the problem
            instance.del_component("input_constraint")
            instance.del_component("input_constraint_index")
            instance.del_component("output_constraint")
            instance.del_component("output_constraint_index")

        return {
            "model_name": model_name,
            "efficiency_arr": efficiency_arr,
            "intensity_arr": intensity_arr,
            "in_ratio_arr": in_ratio_arr,
            "out_ratio_arr": out_ratio_arr,
            "reference_dmu_arr": reference_dmu_arr,
            "projection_x_arr": projection_x_arr,
            "projection_y_arr": projection_y_arr,
        }

    def solve_primal_vrs(self, dmu_idx, oriented=None):
        """ the model must be feasible, but the input or output oriented
        versions may be infeasible
        """
        if oriented in ("input", "output"):
            return self._solve_primal_vrs_oriented(dmu_idx, oriented)

        model_name = "Super_efficiency_SBM_primal_VRS"
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.auxiliary = Var(within=PositiveReals)
        instance.aux_intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.aux_in_ratio = Var(self.input_arr, within=NonNegativeReals)
        instance.aux_out_ratio = Var(self.output_arr,
                                     within=NonNegativeReals)

        # disable dmu_idx th intensity elements
        instance.aux_intensity[dmu_idx].value = 0
        instance.aux_intensity[dmu_idx].fixed = True

        # constraint
        def aux_out_ratio_constraint_rule(model):
            aux_out_ratio_sum = sum(model.aux_out_ratio[jdx]
                                    for jdx in self.output_arr)
            aux_out_ratio_mean = aux_out_ratio_sum / self.dim_output
            return model.auxiliary - aux_out_ratio_mean - 1. == 0

        instance.aux_out_ratio_constraint = Constraint(
            rule=aux_out_ratio_constraint_rule)

        # constraint
        def aux_input_constraint_rule(model, idx):
            aux_frontier = sum(self.X[rdx, idx] * model.aux_intensity[rdx]
                               for rdx in self.dmu_arr)
            return (aux_frontier -
                    model.aux_in_ratio[idx] * self.X[dmu_idx, idx] -
                    model.auxiliary * self.X[dmu_idx, idx] <= 0)

        instance.aux_input_constraint = Constraint(
            self.input_arr, rule=aux_input_constraint_rule)

        # constraint
        def aux_output_constraint_rule(model, jdx):
            aux_frontier = sum(self.Y[rdx, jdx] * model.aux_intensity[rdx]
                               for rdx in self.dmu_arr)
            return (aux_frontier +
                    model.aux_out_ratio[jdx] * self.Y[dmu_idx, jdx] -
                    model.auxiliary * self.Y[dmu_idx, jdx] >= 0)

        instance.aux_output_constraint = Constraint(
            self.output_arr, rule=aux_output_constraint_rule)

        # constraint
        def aux_cvx_boundary_constraint_rule(model, jdx):
            aux_trans_sum = sum(model.aux_intensity[rdx]
                                for rdx in self.dmu_arr)
            return (model.auxiliary - aux_trans_sum == 0)

        instance.aux_cvx_boundary_constraint = Constraint(
            self.output_arr, rule=aux_cvx_boundary_constraint_rule)

        def objective_rule(model):
            aux_in_ratio_sum = sum(model.aux_in_ratio[idx]
                                   for idx in self.input_arr)
            aux_in_ratio_mean = aux_in_ratio_sum / self.dim_input
            return model.auxiliary + aux_in_ratio_mean

        instance.objective = Objective(rule=objective_rule, sense=minimize)

        # solve
        instance = self._solve_model_feasible(instance, model_name)

        # reset variable
        instance.aux_intensity[dmu_idx].fixed = False

        # intensity to original unit and computing the reference dmu_idx
        aux = instance.auxiliary.value
        intensity = np.fromiter(
            (instance.aux_intensity[rdx].value / aux
             for rdx in self.dmu_arr), np.float)

        reference_dmu = self.dmu_arr[intensity > 0]

        in_ratio = np.fromiter(
            (instance.aux_in_ratio[idx].value / aux
             for idx in self.input_arr), np.float)
        out_ratio = np.fromiter(
            (instance.aux_out_ratio[jdx].value / aux
             for jdx in self.output_arr), np.float)

        # data projection
        projection_x = self.X[dmu_idx] * (1 + in_ratio)
        projection_y = self.Y[dmu_idx] * (1 - out_ratio)

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "auxiliary": aux,
            "projection_x": projection_x,
            "projection_y": projection_y,
            "intensity": intensity,
            "in_ratio": in_ratio,
            "out_ratio": out_ratio,
            "reference_dmu": reference_dmu,
            "efficiency": instance.objective(),
        }

    def _solve_primal_vrs_oriented(self, dmu_idx, oriented):
        """ the model may be infeasible. """
        if oriented in ("input", "output"):
            model_name = "Super_efficiency_SBM_primal_VRS_{}_oriented".format(
                oriented)

        # concrete model
        instance = ConcreteModel(name=model_name)

        feasible_arr = [False for rdx in self.dmu_arr]

        # common  decision variables
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)

        # disable dmu_idx th intensity elements
        instance.intensity[dmu_idx].value = 0
        instance.intensity[dmu_idx].fixed = True

        # common data in constraint
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        # common constraint
        def cvx_boundary_constraint_rule(model):
            return (sum(model.intensity[rdx] for rdx in self.dmu_arr) == 1)

        instance.cvx_boundary_constraint = Constraint(
            rule=cvx_boundary_constraint_rule)

        if oriented == "input":
            # input decision variable
            instance.in_ratio = Var(self.input_arr, within=NonNegativeReals)

            # constraint
            def input_constraint_rule(model, idx):
                return (frontier_X[idx] -
                        model.in_ratio[idx] * self.X[dmu_idx, idx] -
                        self.X[dmu_idx, idx] <= 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (frontier_Y[jdx] - self.Y[dmu_idx, jdx] >= 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            def objective_rule(model):
                in_ratio_sum = sum(model.in_ratio[idx]
                                   for idx in self.input_arr)
                in_ratio_mean = in_ratio_sum / self.dim_input
                return 1 + in_ratio_mean

            instance.objective = Objective(rule=objective_rule, sense=minimize)

        elif oriented == "output":
            # output decision variable
            instance.out_ratio = Var(self.output_arr,
                                     within=NonNegativeReals)

            # constraint
            def input_constraint_rule(model, idx):
                return (frontier_X[idx] - self.X[dmu_idx, idx] <= 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (frontier_Y[jdx] +
                        model.out_ratio[jdx] * self.Y[dmu_idx, jdx] -
                        self.Y[dmu_idx, jdx] >= 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            def objective_rule(model):
                out_ratio_sum = sum(model.out_ratio[jdx]
                                    for jdx in self.output_arr)
                out_ratio_mean = out_ratio_sum / self.dim_output
                return (1 - out_ratio_mean)

            instance.objective = Objective(rule=objective_rule, sense=maximize)

        # solve
        opt = SolverFactory(self.solver, solver_io=SOLVER_IO)
        results = opt.solve(instance, keepfiles=KEEPFILES)
        instance.solutions.load_from(results)

        # the VRS model may be infeasible
        if (results.solver.termination_condition ==
                TerminationCondition.infeasible):
            return None

        # reset variable
        instance.intensity[dmu_idx].fixed = False

        # extract results
        intensity = np.fromiter(
            (instance.intensity[rdx].value
             for rdx in self.dmu_arr), np.float)

        reference_dmu = self.dmu_arr[intensity > 0]

        # data projection
        if oriented == "input":
            in_ratio = np.fromiter(
                (instance.in_ratio[idx].value
                 for idx in self.input_arr), np.float)
            out_ratio = 0
            efficiency = instance.objective()

            projection_x = self.X[dmu_idx] * (1 + in_ratio)
            projection_y = self.Y[dmu_idx]
        elif oriented == "output":
            in_ratio = 0
            out_ratio = np.fromiter(
                (instance.out_ratio[idx].value
                 for idx in self.output_arr), np.float)

            efficiency = 1. / instance.objective()

            projection_x = self.X[dmu_idx]
            projection_y = self.Y[dmu_idx] * (1 + out_ratio)

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "projection_x": projection_x,
            "projection_y": projection_y,
            "intensity": intensity,
            "in_ratio": in_ratio,
            "out_ratio": out_ratio,
            "reference_dmu": reference_dmu,
            "efficiency": efficiency,
        }

    def solve_batch_primal_vrs(self, oriented=None):
        if oriented in ("input", "output"):
            return self._solve_batch_primal_vrs_oriented(oriented)

        model_name = "Super_efficiency_SBM_batch_primal_VRS"
        # concrete model
        instance = ConcreteModel(name=model_name)

        # result arrays
        efficiency_arr = np.zeros(self.n_dmu)
        intensity_arr = np.zeros((self.n_dmu, self.n_dmu))
        reference_dmu_arr = []
        aux_arr = np.zeros(self.n_dmu)
        in_ratio_arr = np.zeros((self.n_dmu, self.dim_input))
        out_ratio_arr = np.zeros((self.n_dmu, self.dim_output))
        projection_x_arr = np.zeros((self.n_dmu, self.dim_input))
        projection_y_arr = np.zeros((self.n_dmu, self.dim_output))

        # decision variables
        instance.auxiliary = Var(within=PositiveReals)
        instance.aux_intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.aux_in_ratio = Var(self.input_arr, within=NonNegativeReals)
        instance.aux_out_ratio = Var(self.output_arr,
                                     within=NonNegativeReals)

        # common constraint
        def aux_out_ratio_constraint_rule(model):
            aux_out_ratio_sum = sum(model.aux_out_ratio[jdx]
                                    for jdx in self.output_arr)
            aux_out_ratio_mean = aux_out_ratio_sum / self.dim_output
            return model.auxiliary - aux_out_ratio_mean - 1. == 0

        instance.aux_out_ratio_constraint = Constraint(
            rule=aux_out_ratio_constraint_rule)

        # common constraint
        def aux_cvx_boundary_constraint_rule(model, jdx):
            aux_trans_sum = sum(model.aux_intensity[rdx]
                                for rdx in self.dmu_arr)
            return (model.auxiliary - aux_trans_sum == 0)

        instance.aux_cvx_boundary_constraint = Constraint(
            self.output_arr, rule=aux_cvx_boundary_constraint_rule)

        # common objective
        def objective_rule(model):
            aux_in_ratio_sum = sum(model.aux_in_ratio[idx]
                                   for idx in self.input_arr)
            aux_in_ratio_mean = aux_in_ratio_sum / self.dim_input
            return model.auxiliary + aux_in_ratio_mean

        instance.objective = Objective(rule=objective_rule, sense=minimize)

        # common data in constraint
        aux_frontier_X = [sum(self.X[rdx, idx] * instance.aux_intensity[rdx]
                              for rdx in self.dmu_arr)
                          for idx in self.input_arr]
        aux_frontier_Y = [sum(self.Y[rdx, jdx] * instance.aux_intensity[rdx]
                              for rdx in self.dmu_arr)
                          for jdx in self.output_arr]

        for dmu_idx in self.dmu_arr:
            # disable dmu_idx th intensity elements
            instance.aux_intensity[dmu_idx].value = 0
            instance.aux_intensity[dmu_idx].fixed = True

            # constraint
            def aux_input_constraint_rule(model, idx):
                return (aux_frontier_X[idx] -
                        model.aux_in_ratio[idx] * self.X[dmu_idx, idx] -
                        model.auxiliary * self.X[dmu_idx, idx] <= 0)

            instance.aux_input_constraint = Constraint(
                self.input_arr, rule=aux_input_constraint_rule)

            # constraint
            def aux_output_constraint_rule(model, jdx):
                return (aux_frontier_Y[jdx] +
                        model.aux_out_ratio[jdx] * self.Y[dmu_idx, jdx] -
                        model.auxiliary * self.Y[dmu_idx, jdx] >= 0)

            instance.aux_output_constraint = Constraint(
                self.output_arr, rule=aux_output_constraint_rule)

            # solve
            instance = self._solve_model_feasible(instance, model_name)

            # reset variable
            instance.aux_intensity[dmu_idx].fixed = False

            # extract results
            # intensity to original unit and computing the reference dmu_idx
            efficiency = instance.objective()
            aux = instance.auxiliary.value
            intensity = np.fromiter(
                (instance.aux_intensity[rdx].value / aux
                 for rdx in self.dmu_arr), np.float)

            reference_dmu = self.dmu_arr[intensity > 0]

            in_ratio = np.fromiter(
                (instance.aux_in_ratio[idx].value / aux
                 for idx in self.input_arr), np.float)
            out_ratio = np.fromiter(
                (instance.aux_out_ratio[jdx].value / aux
                 for jdx in self.output_arr), np.float)

            # data projection
            projection_x = self.X[dmu_idx] * (1 + in_ratio)
            projection_y = self.Y[dmu_idx] * (1 - out_ratio)

            efficiency_arr[dmu_idx] = efficiency
            aux_arr[dmu_idx] = aux
            intensity_arr[dmu_idx] = intensity
            reference_dmu_arr.append(reference_dmu)
            in_ratio_arr[dmu_idx] = in_ratio
            out_ratio_arr[dmu_idx] = out_ratio
            projection_x_arr[dmu_idx] = projection_x
            projection_y_arr[dmu_idx] = projection_y

            # delete constraint and resolve the problem
            instance.del_component("aux_input_constraint")
            instance.del_component("aux_input_constraint_index")
            instance.del_component("aux_output_constraint")
            instance.del_component("aux_output_constraint_index")

        return {
            "model_name": model_name,
            "auxiliary_arr": aux_arr,
            "efficiency_arr": efficiency_arr,
            "intensity_arr": intensity_arr,
            "in_ratio_arr": in_ratio_arr,
            "out_ratio_arr": out_ratio_arr,
            "reference_dmu_arr": reference_dmu_arr,
            "projection_x_arr": projection_x_arr,
            "projection_y_arr": projection_y_arr,
        }

    def _solve_batch_primal_vrs_oriented(self, oriented):
        """
        Parameters:
        -------------
        oriented: str, {"input", "output"}
        """
        if oriented in ("input", "output"):
            model_name = "Super_efficiency_SBM_batch_" \
                         "primal_VRS_{}_oriented".format(oriented)

        # concrete model
        instance = ConcreteModel(name=model_name)

        # result arrs
        feasible_arr = [False for rdx in self.dmu_arr]
        efficiency_arr = np.zeros(self.n_dmu)
        intensity_arr = np.zeros((self.n_dmu, self.n_dmu))
        reference_dmu_arr = []
        in_ratio_arr = np.zeros((self.n_dmu, self.dim_input))
        out_ratio_arr = np.zeros((self.n_dmu, self.dim_output))
        projection_x_arr = np.zeros((self.n_dmu, self.dim_input))
        projection_y_arr = np.zeros((self.n_dmu, self.dim_output))

        # common  decision variables
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)

        # common constraint
        def cvx_boundary_constraint_rule(model):
            return (sum(model.intensity[rdx] for rdx in self.dmu_arr) == 1)

        instance.cvx_boundary_constraint = Constraint(
            rule=cvx_boundary_constraint_rule)

        if oriented == "input":
            # input decision variable
            instance.in_ratio = Var(self.input_arr, within=NonNegativeReals)

            def objective_rule(model):
                in_ratio_sum = sum(model.in_ratio[idx]
                                   for idx in self.input_arr)
                in_ratio_mean = in_ratio_sum / self.dim_input
                return 1 + in_ratio_mean

            instance.objective = Objective(rule=objective_rule, sense=minimize)

        elif oriented == "output":
            # output decision variable
            instance.out_ratio = Var(self.output_arr,
                                     within=NonNegativeReals)

            def objective_rule(model):
                out_ratio_sum = sum(model.out_ratio[jdx]
                                    for jdx in self.output_arr)
                out_ratio_mean = out_ratio_sum / self.dim_output
                return (1 - out_ratio_mean)

            instance.objective = Objective(rule=objective_rule, sense=maximize)


        # common data in constraint
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        for dmu_idx in self.dmu_arr:
            # disable dmu_idx th intensity elements
            instance.intensity[dmu_idx].value = 0
            instance.intensity[dmu_idx].fixed = True

            if oriented == "input":
                # constraint
                def input_constraint_rule(model, idx):
                    return (frontier_X[idx] -
                            model.in_ratio[idx] * self.X[dmu_idx, idx] -
                            self.X[dmu_idx, idx] <= 0)

                instance.input_constraint = Constraint(
                    self.input_arr, rule=input_constraint_rule)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (frontier_Y[jdx] - self.Y[dmu_idx, jdx] >= 0)

                instance.output_constraint = Constraint(
                    self.output_arr, rule=output_constraint_rule)


            elif oriented == "output":
                # constraint
                def input_constraint_rule(model, idx):
                    return (frontier_X[idx] - self.X[dmu_idx, idx] <= 0)

                instance.input_constraint = Constraint(
                    self.input_arr, rule=input_constraint_rule)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (frontier_Y[jdx] +
                            model.out_ratio[jdx] * self.Y[dmu_idx, jdx] -
                            self.Y[dmu_idx, jdx] >= 0)

                instance.output_constraint = Constraint(
                    self.output_arr, rule=output_constraint_rule)

            # solve
            opt = SolverFactory(self.solver, solver_io=SOLVER_IO)
            results = opt.solve(instance, keepfiles=KEEPFILES)
            instance.solutions.load_from(results)

            # reset variable
            instance.intensity[dmu_idx].fixed = False

            # the VRS model may be infeasible
            if (results.solver.termination_condition ==
                    TerminationCondition.infeasible):

                # delete constraint and goto next iteration
                instance.del_component("input_constraint")
                instance.del_component("input_constraint_index")
                instance.del_component("output_constraint")
                instance.del_component("output_constraint_index")
                continue
            else:
                # feasible
                feasible_arr[dmu_idx] = True

            # extract results
            intensity = np.fromiter(
                (instance.intensity[rdx].value
                 for rdx in self.dmu_arr), np.float)

            reference_dmu = self.dmu_arr[intensity > 0]

            # data projection
            if oriented == "input":
                in_ratio = np.fromiter(
                    (instance.in_ratio[idx].value
                     for idx in self.input_arr), np.float)
                out_ratio = 0
                efficiency = instance.objective()

                projection_x = self.X[dmu_idx] * (1 + in_ratio)
                projection_y = self.Y[dmu_idx]

            elif oriented == "output":
                in_ratio = 0
                out_ratio = np.fromiter(
                    (instance.out_ratio[idx].value
                     for idx in self.output_arr), np.float)

                efficiency = 1. / instance.objective()

                projection_x = self.X[dmu_idx]
                projection_y = self.Y[dmu_idx] * (1 + out_ratio)

            efficiency_arr[dmu_idx] = efficiency
            intensity_arr[dmu_idx] = intensity
            reference_dmu_arr.append(reference_dmu)
            in_ratio_arr[dmu_idx] = in_ratio
            out_ratio_arr[dmu_idx] = out_ratio
            projection_x_arr[dmu_idx] = projection_x
            projection_y_arr[dmu_idx] = projection_y

            # delete constraint and resolve the problem
            instance.del_component("input_constraint")
            instance.del_component("input_constraint_index")
            instance.del_component("output_constraint")
            instance.del_component("output_constraint_index")

        return {
            "model_name": model_name,
            "feasible_arr": feasible_arr,
            "efficiency_arr": efficiency_arr,
            "intensity_arr": intensity_arr,
            "in_ratio_arr": in_ratio_arr,
            "out_ratio_arr": out_ratio_arr,
            "reference_dmu_arr": reference_dmu_arr,
            "projection_x_arr": projection_x_arr,
            "projection_y_arr": projection_y_arr,
        }

    def solve_vrs(self, dmu_idx, prob_type="primal", oriented=None):
        """
        Parameters
        -----------
        dmu_idx: integer, index of the DMU
        prob_type: str, {"primal",}
        oriented: str, {None, "input", "output"}
        """
        if prob_type == "primal":
            return self.solve_primal_vrs(dmu_idx, oriented)
        else:
            raise ValueError("unknown CRS problem type: {}".format(prob_type))

    def solve_batch_vrs(self, prob_type="primal", oriented=None):
        """
        Parameters
        -----------
        dmu_idx: integer, index of the DMU
        prob_type: str, {"primal", }
        oriented: str, {None, "input", "output"}
        """

        if prob_type == "primal":
            return self.solve_batch_primal_vrs(oriented)
            raise ValueError("unknown CRS batch problem type: {}.".format(
                prob_type))
