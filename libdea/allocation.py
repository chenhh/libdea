# -*- coding: utf-8 -*-
"""
Authors: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2

Allocation model considers not only the number of input and output, but
also the unit cost and unit price of input and output, respectively.
"""

from __future__ import division
import numpy as np
from pyomo.environ import *
from .basic_model import AbstractDEAModel
from . import (DEFAULT_SOLVER, )


class TraditionalAllocation(AbstractDEAModel):
    """ Farrel-Debreu model, it has unacceptable property """

    def __init__(self, X, Y, unit_costs=None, unit_prices=None,
                 solver=DEFAULT_SOLVER):
        """
        Parameters
        -----------
        X: numpy.array, shape: (n_dmu, dim_input)
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        Y: numpy.array, shape: (n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        unit_costs: numpy.array, shape: (n_dmu, dim_input)
        unit_prices: numpy.array, shape: (n_dmu, dim_output)

        solver: str,  the solver supported by pyomo (default: cplex)
        """
        super(TraditionalAllocation, self).__init__(X, Y, solver,
                                                    "semi_positive")

        if unit_costs is not None:
            self.valid_dimension(unit_costs, self.X)
            self.valid_semi_positive(unit_costs)
            self.unit_costs = unit_costs

        if unit_prices is not None:
            self.valid_dimension(unit_prices, self.Y)
            self.valid_semi_positive(unit_prices)
            self.unit_prices = unit_prices

    def solve(self, dmu_idx, prob_type):
        """
        Parameters:
        ----------------
        dmu_idx: integer
        target, str, {"cost", "price", "profit"}
        """
        if prob_type in ("cost", "price", "profit"):
            model_name = "Traditional_allocation_{}_efficiency".format(
                prob_type)
        else:
            raise ValueError("unknown traditional allocation "
                             "target: {}".format(prob_type))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # common decision variable
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # common expression
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        if prob_type == "cost":
            # cost decision variable
            instance.input = Var(self.input_arr, within=NonNegativeReals)

            # constraint
            def input_constraint_rule(model, idx):
                return (model.input[idx] - model.in_slack[idx] -
                        frontier_X[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                        model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # objective
            def objective_rule(model):
                total_cost = sum(model.input[rdx] *
                                 self.unit_costs[dmu_idx, rdx]
                                 for rdx in self.input_arr)
                return total_cost

            instance.objective = Objective(rule=objective_rule, sense=minimize)

        elif prob_type == "price":
            # price decision variable
            instance.output = Var(self.output_arr, within=NonNegativeReals)

            def input_constraint_rule(model, idx):
                return (self.X[dmu_idx, idx] - frontier_X[idx] -
                        model.in_slack[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (model.output[jdx] - frontier_Y[jdx] +
                        model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # objective
            def objective_rule(model):
                total_price = sum(
                    model.output[rdx] * self.unit_prices[dmu_idx, rdx]
                    for rdx in self.output_arr)
                return total_price

            instance.objective = Objective(rule=objective_rule, sense=maximize)

        elif prob_type == "profit":
            # profit decision variable
            instance.input = Var(self.input_arr, within=NonNegativeReals)
            instance.output = Var(self.output_arr, within=NonNegativeReals)

            # constraint
            def input_constraint_rule(model, idx):
                return (self.X[dmu_idx, idx] - model.in_slack[idx] -
                        frontier_X[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def input_feasible_constraint_rule(model, idx):
                return (model.input[idx] - frontier_X[idx] == 0)

            instance.input_feasible_constraint = Constraint(
                self.input_arr, rule=input_feasible_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                        model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # constraint
            def output_feasible_constraint_rule(model, jdx):
                return (model.output[jdx] - frontier_Y[jdx] == 0)

            instance.output_feasible_constraint = Constraint(
                self.output_arr, rule=output_feasible_constraint_rule)

            # objective
            def objective_rule(model):
                total_price = sum(
                    model.output[rdx] * self.unit_prices[dmu_idx, rdx]
                    for rdx in self.output_arr)
                total_cost = sum(
                    model.input[rdx] * self.unit_costs[dmu_idx, rdx]
                    for rdx in self.input_arr)
                return total_price - total_cost

            instance.objective = Objective(rule=objective_rule, sense=maximize)

        # solve
        instance = self._solve_model_feasible(instance, model_name)

        # data output
        intensity = np.fromiter(
            (instance.intensity[idx].value
             for idx in self.dmu_arr), np.float)
        in_slack = np.fromiter(
            (instance.in_slack[idx].value
             for idx in self.input_arr), np.float)
        out_slack = np.fromiter(
            (instance.out_slack[idx].value
             for idx in self.output_arr), np.float)

        input, output = 0, 0
        if prob_type == "cost":
            input = np.fromiter(
                (instance.input[idx].value
                 for idx in self.input_arr), np.float)

            # cost efficiency
            eff_value = instance.objective()
            efficiency = eff_value / np.dot(self.X[dmu_idx],
                                            self.unit_costs[dmu_idx])
        elif prob_type == "price":
            output = np.fromiter(
                (instance.output[idx].value
                 for idx in self.output_arr), np.float)

            # price efficiency
            eff_value = instance.objective()
            efficiency = (np.dot(self.Y[dmu_idx], self.unit_prices[dmu_idx]) /
                          eff_value)

        elif prob_type == "profit":
            input = np.fromiter(
                (instance.input[idx].value
                 for idx in self.input_arr), np.float)

            output = np.fromiter(
                (instance.output[idx].value
                 for idx in self.output_arr), np.float)

            # profit efficiency
            eff_value = instance.objective()
            efficiency = (
                (np.dot(self.Y[dmu_idx], self.unit_prices[dmu_idx]) -
                 np.dot(self.X[dmu_idx], self.unit_costs[dmu_idx])) /
                eff_value)

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "input": input,
            "output": output,
            "intensity": intensity,
            "in_slack": in_slack,
            "out_slack": out_slack,
            "eff_{}".format(prob_type): eff_value,
            "efficiency": efficiency,
        }

    def solve_batch(self, prob_type):
        """
        Parameters:
        ----------------
        target, str, {"cost", "price", "profit"}
        """
        if prob_type in ("cost", "price", "profit"):
            model_name = "Traditional_allocation_batch_{}_efficiency".format(
                prob_type)
        else:
            raise ValueError("unknown traditional allocation batch"
                             "target: {}".format(prob_type))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # results array
        intensity_arr = np.zeros((self.n_dmu, self.n_dmu))
        in_slack_arr = np.zeros((self.n_dmu, self.dim_input))
        out_slack_arr = np.zeros((self.n_dmu, self.dim_output))
        input_arr = np.zeros((self.n_dmu, self.dim_input))
        output_arr = np.zeros((self.n_dmu, self.dim_output))
        eff_value_arr = np.zeros(self.n_dmu)
        efficiency_arr = np.zeros(self.n_dmu)

        # common decision variable
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # common expression
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        if prob_type == "cost":
            # cost decision variable
            instance.input = Var(self.input_arr, within=NonNegativeReals)

            # constraint
            def input_constraint_rule(model, idx):
                return (model.input[idx] - model.in_slack[idx] -
                        frontier_X[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

        elif prob_type == "price":
            # price decision variable
            instance.output = Var(self.output_arr, within=NonNegativeReals)

            # constraint
            def output_constraint_rule(model, jdx):
                return (model.output[jdx] - frontier_Y[jdx] +
                        model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

        elif prob_type == "profit":
            # profit decision variable
            instance.input = Var(self.input_arr, within=NonNegativeReals)
            instance.output = Var(self.output_arr, within=NonNegativeReals)

            # constraint
            def input_feasible_constraint_rule(model, idx):
                return (model.input[idx] - frontier_X[idx] == 0)

            instance.input_feasible_constraint = Constraint(
                self.input_arr, rule=input_feasible_constraint_rule)

            # constraint
            def output_feasible_constraint_rule(model, jdx):
                return (model.output[jdx] - frontier_Y[jdx] == 0)

            instance.output_feasible_constraint = Constraint(
                self.output_arr, rule=output_feasible_constraint_rule)

        for dmu_idx in self.dmu_arr:
            if prob_type == "cost":
                # constraint
                def output_constraint_rule(model, jdx):
                    return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                            model.out_slack[jdx] == 0)

                instance.output_constraint = Constraint(
                    self.output_arr, rule=output_constraint_rule)

                # objective
                def objective_rule(model):
                    total_cost = sum(model.input[rdx] *
                                     self.unit_costs[dmu_idx, rdx]
                                     for rdx in self.input_arr)
                    return total_cost

                instance.objective = Objective(rule=objective_rule,
                                               sense=minimize)

            elif prob_type == "price":
                # constraint
                def input_constraint_rule(model, idx):
                    return (self.X[dmu_idx, idx] - frontier_X[idx] -
                            model.in_slack[idx] == 0)

                instance.input_constraint = Constraint(
                    self.input_arr, rule=input_constraint_rule)

                # objective
                def objective_rule(model):
                    total_price = sum(
                        model.output[rdx] * self.unit_prices[dmu_idx, rdx]
                        for rdx in self.output_arr)
                    return total_price

                instance.objective = Objective(rule=objective_rule,
                                               sense=maximize)

            elif prob_type == "profit":
                # constraint
                def input_constraint_rule(model, idx):
                    return (self.X[dmu_idx, idx] - model.in_slack[idx] -
                            frontier_X[idx] == 0)

                instance.input_constraint = Constraint(
                    self.input_arr, rule=input_constraint_rule)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                            model.out_slack[jdx] == 0)

                instance.output_constraint = Constraint(
                    self.output_arr, rule=output_constraint_rule)

                # objective
                def objective_rule(model):
                    total_price = sum(
                        model.output[rdx] * self.unit_prices[dmu_idx, rdx]
                        for rdx in self.output_arr)
                    total_cost = sum(
                        model.input[rdx] * self.unit_costs[dmu_idx, rdx]
                        for rdx in self.input_arr)
                    return total_price - total_cost

                instance.objective = Objective(rule=objective_rule,
                                               sense=maximize)

            # solve
            instance = self._solve_model_feasible(instance, model_name)

            # data output
            intensity = np.fromiter(
                (instance.intensity[idx].value
                 for idx in self.dmu_arr), np.float)
            in_slack = np.fromiter(
                (instance.in_slack[idx].value
                 for idx in self.input_arr), np.float)
            out_slack = np.fromiter(
                (instance.out_slack[idx].value
                 for idx in self.output_arr), np.float)

            input, output = 0, 0

            if prob_type == "cost":
                input = np.fromiter(
                    (instance.input[idx].value
                     for idx in self.input_arr), np.float)

                # cost efficiency
                eff_value = instance.objective()
                efficiency = eff_value / np.dot(self.X[dmu_idx],
                                                self.unit_costs[dmu_idx])

                # reset constraints
                instance.del_component('output_constraint')
                instance.del_component('output_constraint_index')
                instance.del_component('objective')

            elif prob_type == "price":
                output = np.fromiter(
                    (instance.output[idx].value
                     for idx in self.output_arr), np.float)

                # price efficiency
                eff_value = instance.objective()
                efficiency = (
                np.dot(self.Y[dmu_idx], self.unit_prices[dmu_idx]) /
                eff_value)

                # reset constraints
                instance.del_component('input_constraint')
                instance.del_component('input_constraint_index')
                instance.del_component('objective')

            elif prob_type == "profit":
                input = np.fromiter(
                    (instance.input[idx].value
                     for idx in self.input_arr), np.float)

                output = np.fromiter(
                    (instance.output[idx].value
                     for idx in self.output_arr), np.float)

                # profit efficiency
                eff_value = instance.objective()
                efficiency = (
                    (np.dot(self.Y[dmu_idx], self.unit_prices[dmu_idx]) -
                     np.dot(self.X[dmu_idx], self.unit_costs[dmu_idx])) /
                    eff_value)

                # reset constraints
                instance.del_component('input_constraint')
                instance.del_component('input_constraint_index')
                instance.del_component('output_constraint')
                instance.del_component('output_constraint_index')
                instance.del_component('objective')

            intensity_arr[dmu_idx] = intensity
            in_slack_arr[dmu_idx] = in_slack
            out_slack_arr[dmu_idx] = out_slack
            input_arr[dmu_idx] = input
            output_arr[dmu_idx] = output
            eff_value_arr[dmu_idx] = eff_value
            efficiency_arr[dmu_idx] = efficiency

        return {
            "model_name": model_name,
            "input_arr": input_arr,
            "output_arr": output_arr,
            "intensity_arr": intensity_arr,
            "in_slack_arr": in_slack_arr,
            "out_slack_arr": out_slack_arr,
            "eff_{}_arr".format(prob_type): eff_value_arr,
            "efficiency_arr": efficiency_arr,
        }


class Allocation(AbstractDEAModel):
    """ new allocation model """

    def __init__(self, X, Y, unit_costs=None, unit_prices=None, solver="cplex"):
        """
        Parameters
        -----------
        X: numpy.array, shape: (n_dmu, dim_input)
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        Y: numpy.array, shape: (n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        unit_costs: numpy.array, shape: (n_dmu, dim_input)
        unit_prices: numpy.array, shape: (n_dmu, dim_output)

        solver: str,  the solver supported by pyomo (default: cplex)
        """
        super(Allocation, self).__init__(X, Y, solver,
                                         "semi_positive")

        self.newX = None
        self.newY = None

        if unit_costs is not None:
            self.valid_dimension(unit_costs, self.X)
            self.valid_semi_positive(unit_costs)
            self.newX = self.X * unit_costs

        if unit_prices is not None:
            self.valid_dimension(unit_prices, self.Y)
            self.valid_semi_positive(unit_prices)
            self.newY = self.Y * unit_prices

    def solve(self, dmu_idx, prob_type):
        """
        Parameters:
        ----------------
        dmu_idx: integer,
        prob_type: str, {"tech", "cost", "price", "profit"}
        """
        if prob_type in ("tech", "cost", "price", "profit"):
            model_name = "Allocation_{}_efficiency".format(prob_type)
        else:
            raise ValueError("unknown Allocation problem type: {}".format(
                prob_type))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # common expression
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        if self.newX is not None:
            new_frontier_X = [sum(self.newX[rdx, idx] * instance.intensity[rdx]
                                  for rdx in self.dmu_arr)
                              for idx in self.input_arr]
        if self.newY is not None:
            new_frontier_Y = [sum(self.newY[rdx, jdx] * instance.intensity[rdx]
                                  for rdx in self.dmu_arr)
                              for jdx in self.output_arr]

        if prob_type == "tech":
            # tech decision variable
            instance.efficiency = Var(within=Reals)

            # constraint
            def input_constraint_rule(model, idx):
                return (model.efficiency * self.newX[dmu_idx, idx] -
                        new_frontier_X[idx] - model.in_slack[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                        model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # objective
            def objective_rule(model):
                return model.efficiency

            instance.objective = Objective(rule=objective_rule, sense=minimize)

        elif prob_type == "cost":
            # cost decision variable
            instance.input = Var(self.input_arr, within=NonNegativeReals)

            # constraint
            def input_constraint_rule(model, idx):
                return (model.input[idx] - new_frontier_X[idx] -
                        model.in_slack[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)  # constraint

            def output_constraint_rule(model, jdx):
                frontier = sum(self.Y[rdx, jdx] * model.intensity[rdx]
                               for rdx in self.dmu_arr)
                return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                        model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # objective
            def objective_rule(model):
                total_cost = sum(model.input[rdx] for rdx in self.input_arr)
                return total_cost

            instance.objective = Objective(rule=objective_rule, sense=minimize)

        elif prob_type == "price":
            # price decision variable
            instance.output = Var(self.output_arr, within=NonNegativeReals)

            # constraint
            def input_constraint_rule(model, idx):
                return (self.X[dmu_idx, idx] - frontier_X[idx] -
                        model.in_slack[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (model.output[jdx] - new_frontier_Y[jdx] +
                        model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # objective
            def objective_rule(model):
                total_price = sum(model.output[jdx] for jdx in self.output_arr)
                return total_price

            instance.objective = Objective(rule=objective_rule, sense=maximize)

        elif prob_type == "profit":
            # profit decision variables
            instance.input = Var(self.input_arr, within=NonNegativeReals)
            instance.output = Var(self.output_arr, within=NonNegativeReals)

            # constraint
            def input_constraint_rule(model, idx):
                return (self.newX[dmu_idx, idx] - model.in_slack[idx] -
                        new_frontier_X[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def input_feasible_constraint_rule(model, idx):
                return (model.input[idx] - new_frontier_X[idx] == 0)

            instance.input_feasible_constraint = Constraint(
                self.input_arr, rule=input_feasible_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (self.newY[dmu_idx, jdx] - new_frontier_Y[jdx] +
                        model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # constraint
            def output_feasible_constraint_rule(model, jdx):
                return (model.output[jdx] - new_frontier_Y[jdx] == 0)

            instance.output_feasible_constraint = Constraint(
                self.output_arr, rule=output_feasible_constraint_rule)

            # objective
            def objective_rule(model):
                total_price = sum(model.output[jdx] for jdx in self.output_arr)
                total_cost = sum(model.input[idx] for idx in self.input_arr)
                return total_price - total_cost

            instance.objective = Objective(rule=objective_rule, sense=maximize)

        # solve
        instance = self._solve_model_feasible(instance, model_name)

        # data output
        intensity = np.fromiter(
            (instance.intensity[idx].value
             for idx in self.dmu_arr), np.float)
        in_slack = np.fromiter(
            (instance.in_slack[idx].value
             for idx in self.input_arr), np.float)
        out_slack = np.fromiter(
            (instance.out_slack[idx].value
             for idx in self.output_arr), np.float)

        input, output, eff_value = 0, 0, 0
        if prob_type == "tech":
            efficiency = instance.objective()
            eff_value = efficiency

        elif prob_type == "cost":
            input = np.fromiter(
                (instance.input[idx].value
                 for idx in self.input_arr), np.float)

            # cost efficiency
            eff_value = instance.objective()
            efficiency = eff_value / self.newX[dmu_idx].sum()

        elif prob_type == "price":
            output = np.fromiter(
                (instance.output[jdx].value
                 for jdx in self.output_arr), np.float)

            # price efficiency
            eff_value = instance.objective()
            efficiency = self.newY[dmu_idx].sum() / eff_value

        elif prob_type == "profit":
            input = np.fromiter(
                (instance.input[idx].value
                 for idx in self.input_arr), np.float)
            output = np.fromiter(
                (instance.output[jdx].value
                 for jdx in self.output_arr), np.float)

            # profit efficiency
            eff_value = instance.objective()
            efficiency = (self.newY[dmu_idx].sum() - self.newX[dmu_idx].sum() /
                          eff_value)

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "input": input,
            "output": output,
            "intensity": intensity,
            "in_slack": in_slack,
            "out_slack": out_slack,
            "eff_{}".format(prob_type): eff_value,
            "efficiency": efficiency,
        }

    def solve_batch(self, prob_type):
        """
        Parameters:
        -------------
        prob_type: str, {"tech", "cost", "price", "profit"}
        """

        if prob_type in ("tech", "cost", "price", "profit"):
            model_name = "Allocation_batch_{}_efficiency".format(prob_type)
        else:
            raise ValueError("unknown Allocation batch "
                             "problem type: {}".format(prob_type))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # results array
        input_arr = np.zeros((self.n_dmu, self.dim_input))
        output_arr = np.zeros((self.n_dmu, self.dim_output))
        intensity_arr = np.zeros((self.n_dmu, self.n_dmu))
        in_slack_arr = np.zeros((self.n_dmu, self.dim_input))
        out_slack_arr = np.zeros((self.n_dmu, self.dim_output))
        efficiency_arr = np.zeros(self.n_dmu)
        eff_value_arr = np.zeros(self.n_dmu)

        # decision variables
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # common expression
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        if self.newX is not None:
            new_frontier_X = [sum(self.newX[rdx, idx] * instance.intensity[rdx]
                                  for rdx in self.dmu_arr)
                              for idx in self.input_arr]
        if self.newY is not None:
            new_frontier_Y = [sum(self.newY[rdx, jdx] * instance.intensity[rdx]
                                  for rdx in self.dmu_arr)
                              for jdx in self.output_arr]

        if prob_type == "tech":
            # tech decision variable
            instance.efficiency = Var(within=Reals)

            # objective
            def objective_rule(model):
                return model.efficiency

            instance.objective = Objective(rule=objective_rule, sense=minimize)

        elif prob_type == "cost":
            # cost decision variable
            instance.input = Var(self.input_arr, within=NonNegativeReals)

            # objective
            def objective_rule(model):
                total_cost = sum(model.input[rdx] for rdx in self.input_arr)
                return total_cost

            instance.objective = Objective(rule=objective_rule, sense=minimize)

        elif prob_type == "price":
            # price decision variable
            instance.output = Var(self.output_arr, within=NonNegativeReals)

            # objective
            def objective_rule(model):
                total_price = sum(model.output[jdx] for jdx in self.output_arr)
                return total_price

            instance.objective = Objective(rule=objective_rule, sense=maximize)

        elif prob_type == "profit":
            # profit decision variables
            instance.input = Var(self.input_arr, within=NonNegativeReals)
            instance.output = Var(self.output_arr, within=NonNegativeReals)

            # constraint
            def input_feasible_constraint_rule(model, idx):
                return (model.input[idx] - new_frontier_X[idx] == 0)

            instance.input_feasible_constraint = Constraint(
                self.input_arr, rule=input_feasible_constraint_rule)

            # constraint
            def output_feasible_constraint_rule(model, jdx):
                return (model.output[jdx] - new_frontier_Y[jdx] == 0)

            instance.output_feasible_constraint = Constraint(
                self.output_arr, rule=output_feasible_constraint_rule)

            # objective
            def objective_rule(model):
                total_price = sum(model.output[jdx] for jdx in self.output_arr)
                total_cost = sum(model.input[idx] for idx in self.input_arr)
                return total_price - total_cost

            instance.objective = Objective(rule=objective_rule, sense=maximize)

        for dmu_idx in self.dmu_arr:
            if prob_type == "tech":
                # constraint
                def input_constraint_rule(model, idx):
                    return (model.efficiency * self.newX[dmu_idx, idx] -
                            new_frontier_X[idx] - model.in_slack[idx] == 0)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                            model.out_slack[jdx] == 0)

            elif prob_type == "cost":
                # constraint
                def input_constraint_rule(model, idx):
                    return (model.input[idx] - new_frontier_X[idx] -
                            model.in_slack[idx] == 0)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                            model.out_slack[jdx] == 0)

            elif prob_type == "price":
                # constraint
                def input_constraint_rule(model, idx):
                    return (self.X[dmu_idx, idx] - frontier_X[idx] -
                            model.in_slack[idx] == 0)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (model.output[jdx] - new_frontier_Y[jdx] +
                            model.out_slack[jdx] == 0)

            elif prob_type == "profit":
                # constraint
                def input_constraint_rule(model, idx):
                    return (self.newX[dmu_idx, idx] - model.in_slack[idx] -
                            new_frontier_X[idx] == 0)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (self.newY[dmu_idx, jdx] - new_frontier_Y[jdx] +
                            model.out_slack[jdx] == 0)

            # build constraints
            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # solve
            instance = self._solve_model_feasible(instance, model_name)

            # extract results
            intensity = np.fromiter(
                (instance.intensity[idx].value
                 for idx in self.dmu_arr), np.float)
            in_slack = np.fromiter(
                (instance.in_slack[idx].value
                 for idx in self.input_arr), np.float)
            out_slack = np.fromiter(
                (instance.out_slack[idx].value
                 for idx in self.output_arr), np.float)

            input, output, eff_value = 0, 0, 0
            if prob_type == "tech":
                efficiency = instance.objective()
                eff_value = efficiency

            elif prob_type == "cost":
                input = np.fromiter(
                    (instance.input[idx].value
                     for idx in self.input_arr), np.float)

                # cost efficiency
                eff_value = instance.objective()
                efficiency = eff_value / self.newX[dmu_idx].sum()

            elif prob_type == "price":
                output = np.fromiter(
                    (instance.output[jdx].value
                     for jdx in self.output_arr), np.float)

                # price efficiency
                eff_value = instance.objective()
                efficiency = self.newY[dmu_idx].sum() / eff_value

            elif prob_type == "profit":
                input = np.fromiter(
                    (instance.input[idx].value
                     for idx in self.input_arr), np.float)
                output = np.fromiter(
                    (instance.output[jdx].value
                     for jdx in self.output_arr), np.float)

                # profit efficiency
                eff_value = instance.objective()
                efficiency = (self.newY[dmu_idx].sum() -
                              self.newX[dmu_idx].sum() / eff_value)

            input_arr[dmu_idx] = input
            output_arr[dmu_idx] = output
            intensity_arr[dmu_idx] = intensity
            in_slack_arr[dmu_idx] = in_slack
            out_slack_arr[dmu_idx] = out_slack
            efficiency_arr[dmu_idx] = efficiency
            eff_value_arr[dmu_idx] = eff_value

            # reset constraints
            instance.del_component("input_constraint")
            instance.del_component("input_constraint_index")
            instance.del_component("output_constraint")
            instance.del_component("output_constraint_index")

        return {
            "model_name": model_name,
            "input_arr": input_arr,
            "output_arr": output_arr,
            "intensity_arr": intensity_arr,
            "in_slack_arr": in_slack_arr,
            "out_slack_arr": out_slack_arr,
            "eff_{}_arr".format(prob_type): eff_value_arr,
            "efficiency_arr": efficiency_arr,
        }
