# -*- coding: utf-8 -*-
'''
.. codeauthor:: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
'''
__author__ = 'Hung-Hsin Chen'
import os

import pandas as pd

from libdea.sandbox.experiment import (STOCK_PKL_DIR)


def read_symbol():
    """
    axis 0: time
    axis 1: stock_id
    axis 2: stock_properties
    """
    path = os.path.join(STOCK_PKL_DIR, "panel_largest50stocks.pkl")
    pnl = pd.read_pickle(path)
    print pnl




if __name__ == '__main__':
    read_symbol()

