# -*- coding: utf-8 -*-
'''
.. codeauthor:: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
'''

import os

import pandas as pd
import numpy as np

from libdea.sandbox.experiment import (STOCK_PKL_DIR, EXP_SYMBOLS)


def read_exp_data():
    """
    axis 0: time
    axis 1: stock_id
    axis 2: stock_properties
    """
    path = os.path.join(STOCK_PKL_DIR, "panel_largest50stocks.pkl")
    pnl = pd.read_pickle(path)
    return EXP_SYMBOLS, pnl


def sharpe(series):
    '''ROI series
    the numpy std() function is the population estimator
    '''
    s = np.asarray(series)
    try:
        val =  s.mean()/s.std()
    except FloatingPointError:
        val = 0
    return val


def sortino_full(series, mar=0):
    '''ROI series
    MAR, minimum acceptable return
    '''
    s = np.asarray(series)
    mean = s.mean()
    semi_std = np.sqrt( ((s*((s-mar)<0))**2).mean() )
    try:
        val =  mean/semi_std
    except FloatingPointError:
        val = 0
    return val, semi_std


def sortino_partial(series, mar=0):
    '''ROI series
    MAR, minimum acceptable return
    '''
    s = np.asarray(series)
    mean = s.mean()
    neg_periods = (s-mar) < 0
    n_neg_periods = neg_periods.sum()
    try:
        semi_std = np.sqrt(((s * n_neg_periods)**2).sum()/n_neg_periods)
        val =  mean/semi_std
    except FloatingPointError:
        val, semi_std = 0, 0
    return val, semi_std

