# -*- coding: utf-8 -*-
'''
.. codeauthor:: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
'''
__author__ = 'Hung-Hsin Chen'


import os
import platform

pf = platform.system()
if pf == "Windows":
    SEAFILE_DIR = r'C:\Users\chen1\Seafile'
    DROPBOX_DIR = r'c:\Users\chen1\Dropbox'
    TMP_DIR = r'e:'

elif pf == "Linux":
    SEAFILE_DIR = r'/home/chenhh/Seafile'
    DROPBOX_DIR = r'/home/chenhh/Dropbox'
    TMP_DIR = r'/tmp'

else:
    raise ValueError("{} unknown operating system.".format(pf))

STOCK_PKL_DIR = os.path.join(SEAFILE_DIR, 'financial_data', 'pkl', 'up_dev')

STOCK_INDICATOR_PKL_DIR = os.path.join(SEAFILE_DIR, 'financial_data', 'pkl',
                                       'up_dev_indicator')

STOCK_DJIA_PKL_DIR = os.path.join(SEAFILE_DIR, 'financial_data', 'pkl',
                                  'DJIA_20050318')

ONEDRIVE_DIR = os.path.join(r'C:\Users', 'chenhh', 'OneDrive')

DROPBOX_UP_EXPERIMENT_DIR = os.path.join(DROPBOX_DIR,
                                         'experiment_online_portfolio')

SEAFILE_UP_EXPERIMENT_DIR = os.path.join(SEAFILE_DIR, 'paper',
                                 'experiment_online_portfolio')

TEJ_CSV_DIR = os.path.join(SEAFILE_DIR, 'financial_data', 'data_source',
                           'TEJ')
PARALLEL_SHARE_DIR = r'/home/chenhh/share_parallel'

EXP_SYMBOLS = [
   "2330", "2412", "2882", "6505", "2317",
   "2303", "2002", "1303", "1326", "1301",
   "2881", "2886", "2409", "2891", "2357",
   "2382", "3045", "2883", "2454", "2880",
   "2892", "4904", "2887", "2353", "2324",
   "2801", "1402", "2311", "2475", "2888",
   "2408", "2308", "2301", "2352", "2603",
   "2884", "2890", "2609", "9904", "2610",
   "1216", "1101", "2325", "2344", "2323",
   "2371", "2204", "1605", "2615", "2201",
]

DJIA_20150318_SYMBOLS = [
    "MMM", "AXP", "AAPL", "BA", "CAT",
    "CVX", "CSCO", "KO", "DD", "XOM",
    "GE", "GS", "IBM", "INTC", "JNJ",
    "JPM", "MCD", "MRK", "MSFT", "NKE",
    "PFE", "PG", "HD", "TRV", "UTX",
    "UNH", "VZ", "V", "WMT", "DIS",
]

# https://en.wikipedia.org/wiki/Historical_components_of_the_Dow_Jones_Industrial_Average

# 2005/01/03 ~ 2015/5/21 unchanged stocks
DJIA_SINCE_2005_SYMBOlS =[
    "MMM", "AXP", "BA", "CAT",
    "KO", "DD", "XOM",
    "GE",  "IBM", "INTC", "JNJ",
    "JPM", "MCD", "MRK", "MSFT",
    "PFE", "PG", "HD",  "UTX",
    "VZ", "WMT", "DIS",
]

# http://www.cs.technion.ac.il/~rani/portfolios/
# http://www.cs.bme.hu/~oti/portfolio/
# http://www.cais.ntu.edu.sg/~libin/
# http://www.mysmu.edu.sg/faculty/chhoi/olps/

# http://52.74.10.55/
# http://olps.stevenhoi.org
# 1962/7/3 ~ 1984/12/31, 5651 days, 36 stocks
BENCHMARK_NYSE_O =['AHP', 'AA', ]

# 1985/1/1 ~ 2009/6/30, 6179 days, 23 stocks
BENCHMARK_NYSE_N = []

# 1998/1/2 ~ 2003/1/31, 1276 days, 25 stocks
# 25 stocks from SP500 which (as of Apr. 2003) had the largest market cap.
BENCHMARK_SP500 = []

# 2001/1/14 ~ 2003/1/14, 507 days, 30 stocks
# C:\Seafile\financial_data\exp_data\DJIA.zip
BENCHMARK_DJIA = ['AA', 'GE', 'JNJ', 'MSFT', 'AXP',
                  'GM', 'JPM', 'PG', 'BA', 'HD',
                  'KO', 'SBC', 'C', 'HON', 'MCD',
                  'T', 'CAT', 'HP', 'MMM', 'UTX',
                  'DD', 'IBM', 'PM', 'WMT', 'DIS',
                  'INTC', 'MRK', 'XOM', 'EK', 'IP',
                  ]

# 2005/9/9 ~ 2009/9/7, 1042 days, 3 stocks
# It contains three indices which represent the equity markets of
# Paciﬁc, North America, and Europe
BENCHMARK_MSCI = []
