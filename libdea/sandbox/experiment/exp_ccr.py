# -*- coding: utf-8 -*-
'''
.. codeauthor:: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
'''
__author__ = 'Hung-Hsin Chen'

import os
from datetime import date
from time import time

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from utils import (read_exp_data, sharpe, sortino_full, sortino_partial)
from libdea.additive import Additive
from libdea.sandbox.experiment import (TMP_DIR,)


def ratios(func, win_length=20, start_date=date(2005,1,1),
                   end_date=date(2014,12,31)):
    symbols, pnl = read_exp_data()
    trans_dates = pnl[start_date:end_date].items

    n_symbol, n_period = len(symbols), len(trans_dates)

    # results dataframe
    ratio_df = pd.DataFrame(np.zeros((n_period, n_symbol)), index=trans_dates,
                      columns=symbols)

    t0 = time()
    for trans_date in trans_dates:
        t1 = time()
        today_tdx = pnl.items.get_loc(trans_date)
        # containing today
        sdx, edx = today_tdx-win_length -1, today_tdx - 1

        # shape: (n_symbol, win_length)
        hist_rois = pnl.ix[sdx:edx, :, 'adj_roi']/100.
        ratios = np.apply_along_axis(func, 1, hist_rois)
        ratio_df.loc[trans_date] = ratios

        print trans_date, "{}_w{}, {:.3f} secs".format(
            func.__name__, win_length, time()-t1)
    print "{:.3f} secs".format(time()-t0)

    path = os.path.join(TMP_DIR, 'exp_{}_{}-{}_w{}.csv'.format(
        func.__name__, start_date.strftime("%Y%m%d"),
        end_date.strftime("%Y%m%d"), win_length))
    ratio_df.to_csv(path)
    # ratio_df.plot()
    # plt.show()

def additive_efficiency(win_length=20, start_date=date(2005,1,1),
                   end_date=date(2005,12,31)):
    symbols, pnl = read_exp_data()

    trans_dates = pnl[start_date:end_date].items
    scores = pd.Series(np.zeros(len(trans_dates)), index=trans_dates)

    t0 = time()
    for trans_date in trans_dates:
        t1 = time()
        today_tdx = pnl.items.get_loc(trans_date)
        sdx, edx = today_tdx-win_length, today_tdx

        # input
        hist_rois = pnl.ix[sdx:edx, :, 'adj_roi']/100.
        stdev_hist_rois = hist_rois.std(axis=1)
        X = stdev_hist_rois[:, np.newaxis]

        # output
        today_rois = pnl.ix[today_tdx, :, 'adj_roi']/100.

        #shift to 0
        today_rois = today_rois - np.min(today_rois) + 1e-4

        Y = today_rois[:, np.newaxis]
        # print X.shape, Y.shape

        # only compute 2330
        rdx = pnl.major_axis.get_loc('2330')

        instance = Additive(X, Y)
        res_p = instance.solve(rdx, 'primal')
        res_d = instance.solve(rdx, "dual")
        scores.loc[trans_date] = res_d['efficiency']

        # res_p['reference_dmu'] to symbol
        r_dmus = pnl.major_axis[res_p['reference_dmu']]

        print trans_date,  res_d['efficiency'], r_dmus
        print "{:.3f} secs".format(time()-t1)
    print "{:.3f} secs".format(time()-t0)
    scores.plot()
    plt.show()

if __name__ == '__main__':
    # additive_efficiency()
    for func in (sharpe, sortino_full, sortino_partial):
        for win in (20, 40, 60, 80, 100, 120):
            ratios(func, win)


