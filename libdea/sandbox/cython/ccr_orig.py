# -*- coding: utf-8 -*-
'''
.. codeauthor:: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
'''
__author__ = 'Hung-Hsin Chen'

import numpy as np
from pyomo.environ import *



def primal_input_oriented(dmu_idx,
                          X,
                          Y,
                          solver="cplex"):
    n_dmu = X.shape[0]
    dim_input = X.shape[1]
    dim_output = Y.shape[1]

    model_name = "CCR_primal_input_oriented"
    instance = ConcreteModel(name=model_name)  # Set, directly using X, and Y

    # decision variables
    instance.in_weight = Var(np.arange(dim_input),
                             within=NonNegativeReals)
    instance.out_weight = Var(np.arange(dim_output),
                              within=NonNegativeReals)

    # constraint
    def efficient_constraint_rule(model, rdx):
        in_sum = sum(model.in_weight[jdx] * X[rdx, jdx] for jdx
                     in xrange(dim_input))
        out_sum = sum(model.out_weight[jdx] * Y[rdx, jdx] for
                      jdx in xrange(dim_output))
        return out_sum - in_sum <= 0

    instance.efficient_constraint = Constraint(
        np.arange(n_dmu), rule=efficient_constraint_rule)

    # constraint
    def normalized_constraint_rule(model):
        in_sum = sum(model.in_weight[jdx] * X[dmu_idx, jdx] for jdx
                     in xrange(dim_input))
        return in_sum == 1

    instance.normalized_constraint = Constraint(
        rule=normalized_constraint_rule)

    # objective
    def objective_rule(model):
        out_sum = sum(model.out_weight[jdx] * Y[dmu_idx, jdx] for jdx
                      in xrange(dim_output))
        return out_sum

    instance.objective = Objective(rule=objective_rule, sense=maximize)

    opt = SolverFactory(solver)

    results = opt.solve(instance)
    instance.solutions.load_from(results)
    # display(instance)

    return {
        "model_name": model_name,
        "dmu_idx": dmu_idx,
        "in_weight": np.fromiter(
            (instance.in_weight[idx].value for idx in
             xrange(dim_input)), np.float),
        "out_weight": np.fromiter(
            (instance.out_weight[idx].value for idx in
             xrange(dim_output)), np.float),
        "efficiency": instance.objective(),
    }
