import sys
sys.path.append('../../')
from time import time
import numpy as np
from libdea.ccr import CCR
from time import time


def test_ccr_table_4_4(precision=3):
    X = np.array([[1,1,1],[1,2,1],[1,1,2],[1,1,1],
                  [1,1,1],[2,2,2]])
    Y = np.array([[2,2,2],[2,2,2],[1,2,2],[1,2,1],
                  [2,1,0.5],[.5, .5, 1]])

    eff = np.array([[1,],[1,],[1,],[1,],[1,],[.25,]])

    instance = CCR(X,Y, "cplex")
    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "dual", "input")
        print res['efficiency'], res['in_slack']

    res2 = instance.solve_batch("dual", "input")
    print res2['efficiency_arr']
    print res2['in_slack_arr']



def test_random(precision=3):
    n_dmu = 100
    dim_input, dim_output = 10, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)


    instance = CCR(X,Y, "cplex")
    effs = []
    t0 = time()
    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "dual", "input")
        effs.append(res['efficiency'])
    print "orig CCR {} secs".format(time()-t0)

    t1 = time()
    effs2 = instance.solve_batch_dual_input_oriented()
    # print effs
    print "CCR2 {} secs".format(time()-t1)

    for c1, c2 in zip(effs, effs2['efficiency_arr']):
        assert round(c1-c2, precision) == 0

if __name__ == '__main__':
    # test_ccr_table_4_4()
    test_random()