# -*- coding: utf-8 -*-
#!python
#cython: boundscheck=False
#cython: wraparound=False
#cython: infer_types=True
#cython: cdivision=True

# Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>

__author__ = 'Hung-Hsin Chen'

include "basic_model.pxi"
from pyomo.environ import *

cdef class CCR(AbstractDEAModel):

    def __init__(self, X, Y, solver="cplex"):
        """
        :param X: numpy.array, shape: (N, M)
            N is the number of data (DMU).
             M is the input dimension of an observation.

        :param Y: numpy.array, shape(N, S)
          N is the number of data (DMU).
          S is the output dimension of an observation.

        :param solver: the solver supported by pyomo
        """
        super(CCR, self).__init__(X, Y, solver, "semi_positive")


    def solve_primal_input_oriented(self, int dmu_idx):
        """
        overload basic_model method
        param dmu_idx:
        return
        """
        # Model
        model_name = "CCR_primal_input_oriented"
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.in_weight = Var(self.input_arr, within=NonNegativeReals)
        instance.out_weight = Var(self.output_arr, within=NonNegativeReals)

        # constraint
        def efficient_constraint_rule(model, int rdx):
            in_sum = sum(model.in_weight[idx] * self.X[rdx, idx]
                         for idx in self.input_arr)
            out_sum = sum(model.out_weight[jdx] * self.Y[rdx, jdx]
                          for jdx in self.output_arr)
            return out_sum - in_sum <= 0.

        instance.efficient_constraint = Constraint(
            self.dmu_arr, rule=efficient_constraint_rule)

        # constraint
        def normalized_constraint_rule(model):
            in_sum = sum(model.in_weight[idx] * self.X[dmu_idx, idx]
                         for idx in self.input_arr)
            return in_sum == 1.

        instance.normalized_constraint = Constraint(
            rule=normalized_constraint_rule)

        # objective
        def objective_rule(model):
            out_sum = sum(model.out_weight[jdx] * self.Y[dmu_idx, jdx]
                          for jdx in self.output_arr)
            return out_sum

        instance.objective = Objective(rule=objective_rule, sense=maximize)

        opt = SolverFactory(self.solver, solver_io="python")
        results = opt.solve(instance, tee=False, keepfiles=True)
        instance.solutions.load_from(results)
        # display(instance)


        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "in_weight": np.fromiter(
                (instance.in_weight[idx].value
                 for idx in self.input_arr), np.float),
            "out_weight": np.fromiter(
                (instance.out_weight[jdx].value
                 for jdx in self.output_arr), np.float),
            "efficiency": instance.objective(),
        }

    def solve_dual_input_oriented(self, int dmu_idx):
        """
        solve the efficiency value and the projection vector, and the
        objective value is the efficiency
        """

        model_name = "CCR_dual_input_oriented"
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.efficiency = Var(within=Reals)
        instance.transform = Var(self.dmu_arr, within=NonNegativeReals)
        instance.input_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.output_slack = Var(self.output_arr, within=NonNegativeReals)

        # constraint
        def input_constraint_rule(model, int idx):
            frontier = sum(self.X[rdx, idx] * model.transform[rdx]
                           for rdx in self.dmu_arr)
            return (model.efficiency * self.X[dmu_idx, idx] -
                    model.input_slack[idx] - frontier == 0.)

        instance.input_constraint = Constraint(
            self.input_arr, rule=input_constraint_rule)

        # constraint
        def output_constraint_rule(model, int jdx):
            frontier = sum(self.Y[rdx, jdx] * model.transform[rdx]
                           for rdx in self.dmu_arr)
            return (self.Y[dmu_idx, jdx] - frontier +
                    model.output_slack[jdx] == 0.)

        instance.output_constraint = Constraint(
            self.output_arr, rule=output_constraint_rule)

        # objective
        def objective_rule(model):
            return instance.efficiency

        instance.objective = Objective(rule=objective_rule, sense=minimize)

        opt = SolverFactory(self.solver)

        results = opt.solve(instance)
        instance.solutions.load_from(results)
        # display(instance)

        # computing the reference dmu_idx
        transform = np.fromiter((
            instance.transform[rdx].value
            for rdx in self.dmu_arr), np.float)
        reference_dmu = np.arange(self.n_dmu)[transform > 0]
        input_slack = np.fromiter((
            instance.input_slack[idx].value
            for idx in self.input_arr), np.float)
        output_slack = np.fromiter((
            instance.output_slack[jdx].value
            for jdx in self.output_arr), np.float)
        slack_sum = input_slack.sum() + output_slack.sum()

        # data projection
        efficiency = instance.efficiency.value
        in_projection = self.X[dmu_idx] *efficiency - input_slack
        out_projection = self.Y[dmu_idx] + output_slack

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "efficiency": efficiency,
            "transform": transform,
            "in_projection": in_projection,
            "out_projection": out_projection,
            "in_slack": input_slack,
            "out_slack": output_slack,
            "reference_dmu": reference_dmu,
            "slack_sum": slack_sum,
        }

    def solve_primal_output_oriented(self, int dmu_idx):
        """
        only solve the input_weight and output_weight vectors, and
        the objective value is the efficiency.

        output-oriented means given fixed input, comparing the ratio of the
        output among others, and the larger output the better it is.
        """
        # Model
        model_name = "CCR_primal_output_oriented"
        instance = ConcreteModel(name=model_name)

        # Set, directly using X, and Y

        # decision variables
        instance.in_weight = Var(self.input_arr, within=NonNegativeReals)
        instance.out_weight = Var(self.output_arr, within=NonNegativeReals)

        # constraint
        def efficient_constraint_rule(model, int rdx):
            in_sum = sum(model.in_weight[idx] * self.X[rdx, idx]
                         for idx in self.input_arr)
            out_sum = sum(model.out_weight[jdx] * self.Y[rdx, jdx]
                          for jdx in self.output_arr)
            return out_sum - in_sum <= 0.

        instance.efficient_constraint = Constraint(
            self.dmu_arr, rule=efficient_constraint_rule)

        # constraint
        def normalized_constraint_rule(model):
            out_sum = sum(model.out_weight[jdx] * self.Y[dmu_idx, jdx]
                          for jdx in self.output_arr)
            return out_sum == 1.

        instance.normalized_constraint = Constraint(
            rule=normalized_constraint_rule)

        # objective
        def objective_rule(model):
            in_sum = sum(model.in_weight[jdx] * self.X[dmu_idx, jdx] for jdx
                         in self.input_arr)
            return in_sum

        instance.objective = Objective(rule=objective_rule, sense=minimize)

        opt = SolverFactory(self.solver)

        results = opt.solve(instance)
        instance.solutions.load_from(results)
        display(instance)

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "in_weight": np.fromiter((
                instance.in_weight[idx].value
                for idx in self.input_arr), np.float),
            "out_weight": np.fromiter((
                instance.out_weight[jdx].value
                for jdx in self.output_arr), np.float),
            "efficiency": instance.objective(),
        }

    def solve_dual_output_oriented(self, int dmu_idx):
        # Model
        model_name = "CCR_dual_output_oriented"
        instance = ConcreteModel(name=model_name)

        # Set, directly using X, and Y

        # decision variables
        instance.efficiency = Var(within=Reals)
        instance.transform = Var(self.dmu_arr, within=NonNegativeReals)
        instance.input_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.output_slack = Var(self.output_arr, within=NonNegativeReals)

        # constraint
        def input_constraint_rule(model, int idx):
            frontier = sum(self.X[rdx, idx] * model.transform[rdx]
                           for rdx in self.dmu_arr)
            return (self.X[dmu_idx, idx] - frontier -
                    model.input_slack[idx] == 0.)

        instance.input_constraint = Constraint(
            self.input_arr, rule=input_constraint_rule)

        # constraint
        def output_constraint_rule(model, int jdx):
            frontier = sum(self.Y[rdx, jdx] * model.transform[rdx]
                           for rdx in self.dmu_arr)
            return (model.efficiency * self.Y[dmu_idx, jdx] - frontier
                    + model.output_slack[jdx] == 0.)

        instance.output_constraint = Constraint(
            self.output_arr, rule=output_constraint_rule)

        # objective
        def objective_rule(model):
            return instance.efficiency

        instance.objective = Objective(rule=objective_rule, sense=maximize)

        opt = SolverFactory(self.solver)

        results = opt.solve(instance)
        instance.solutions.load_from(results)
        # display(instance)

        # computing the reference dmu_idx
        transform = np.fromiter((
            instance.transform[rdx].value
            for rdx in self.dmu_arr), np.float)
        reference_dmu = self.dmu_arr[transform > 0]

        input_slack = np.fromiter((
            instance.input_slack[idx].value
            for idx in self.input_arr), np.float)
        output_slack = np.fromiter((
            instance.output_slack[jdx].value
            for jdx in self.output_arr), np.float)
        slack_sum = input_slack.sum() + output_slack.sum()

        # data projection
        efficiency = instance.efficiency.value
        in_projection = self.X[dmu_idx]  - input_slack
        out_projection = self.Y[dmu_idx]*efficiency + output_slack


        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "efficiency": efficiency,
            "transform": transform,
            "in_projection": in_projection,
            "out_projection": out_projection,
            "input_slack": input_slack,
            "out_slack": output_slack,
            "reference_dmu": reference_dmu,
            "slack_sum": slack_sum,
        }
