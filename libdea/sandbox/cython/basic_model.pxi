# -*- coding: utf-8 -*-
#!python
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True

# Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
# http://docs.cython.org/src/reference/extension_types.html
import numpy as np
cimport numpy as cnp

__author__ = 'Hung-Hsin Chen'

cdef class AbstractDEAModel:
    cdef:
        public Py_ssize_t  n_dmu
        public Py_ssize_t  dim_input
        public Py_ssize_t  dim_output
        double [:, :] X
        double [:, :] Y
        int [:] dmu_arr
        int [:] input_arr
        int [:] output_arr
        public str solver

    def __cinit__(self):
        """
        Use cinit to initialize all arrays to empty: this will prevent memory
        errors and seg-faults in rare cases where __init__ is not called
        """
        self.n_dmu = 0
        self.dim_input = 0
        self.dim_output = 0
        self.X = np.empty((0, 0))
        self.Y = np.empty((0, 0))
        self.dmu_arr = np.zeros(0, dtype=np.int32)
        self.input_arr = np.zeros(0, dtype=np.int32)
        self.output_arr = np.zeros(0, dtype=np.int32)


    def __init__(self, double [:, :] X, double [:, :] Y, solver="cplex",
                 data_constraint=None,
                 ):
        """
        :param X: numpy.array, shape: (N, M)
            N is the number of data (DMU).
            M is the input dimension of an observation.

        :param Y: numpy.array, shape(N, S)
            N is the number of data (DMU).
            S is the output dimension of an observation.

        :param solver: the solver supported by pyomo
        """
        # check  dimension
        if X.ndim != 2:
            raise ValueError("wrong input dimension: X:{}".format(X.ndim))

        if Y.ndim != 2:
            raise ValueError("wrong output dimension: Y:{}".format(Y.ndim))

        if X.shape[0] != Y.shape[0]:
            raise ValueError("mismatch DMU dimension, X:{}, Y:{}".format(
                X.shape[0], Y.shape[0]))

        # check positive
        if data_constraint is None:
            pass
        elif data_constraint == "positive":
            self.valid_positive(X)
            self.valid_positive(Y)
        elif data_constraint == "semi_positive":
            self.valid_semi_positive(X)
            self.valid_semi_positive(Y)
        else:
            raise ValueError(
                "Unknown data constraint: {}".format(data_constraint))

        self.n_dmu = X.shape[0]
        self.dim_input = X.shape[1]
        self.dim_output = Y.shape[1]
        self.X = X
        self.Y = Y
        self.solver = solver
        self.dmu_arr = np.arange(self.n_dmu)
        self.input_arr = np.arange(self.dim_input)
        self.output_arr = np.arange(self.dim_output)

    cdef valid_positive(self, double [:, :] data):
        """
         param data: numpy.array, shape: (M, N)
         M is the number of observations, and
         N is the dimension of an observation,
         each row is an observation, and all values in an observation >0,
        """
        # if not np.all(np.all(data > 0, axis=1)):
        #     raise ValueError("data is not all positive.\n {}".format(data))

    cdef valid_semi_positive(self, double [:, :] data):
        """
         param data: numpy.array, shape: (M, N)
         M is the number of observations, and
         N is the dimension of an observation,
         each row is an observation, and all values in an observation >=0,
         but, the observation is not zero.
        """
        # if not np.all(np.all(data >= 0, axis=1)):
        #     raise ValueError("data is not all positive.\n {}".format(data))
        #
        # if np.any(np.all(data == 0, axis=1)):
        #     raise ValueError("some data is zero vector.\n {}".format(data))

    cdef valid_dimension(self, int [:,:] arr1, int [:,:] arr2):
        if arr1.shape[0] != arr2.shape[0] or arr1.shape[1] != arr2.shape[1]:
            raise ValueError('mismatch dimension {}, {}'.format(arr1.shape,
                                                                arr2.shape))

    def solve_primal(self, int dmu_idx):
        print "solve_primal from basic model，dmu_idx:{}".format(dmu_idx)

    def solve_dual(self, int dmu_idx):
         print "solve_dual from basic model，dmu_idx:{}".format(dmu_idx)

    def solve_primal_input_oriented(self, int dmu_idx):
        pass

    def solve_dual_input_oriented(self, int dmu_idx):
        pass

    def solve_primal_output_oriented(self, int dmu_idx):
        pass

    def solve_dual_output_oriented(self, int dmu_idx):
        pass

    def solve(self, int dmu_idx, prob_type="primal", oriented=None):
        """
        dispatch pattern

        :param dmu_idx: 0 <=integer <= self.n_dmu
        :prob_type: {"primal", "dual"}
        :oriented: {"input", "output"}
        """
        if prob_type == "primal":
            if oriented is None:
                return self.solve_primal(dmu_idx)
            elif oriented == "input":
                return self.solve_primal_input_oriented(dmu_idx)
            elif oriented == "output":
                return self.solve_primal_output_oriented(dmu_idx)
            else:
                raise ValueError(
                    "unknown primal oriented: {}.".format(oriented))
        elif prob_type == "dual":
            if oriented is None:
                return self.solve_dual(dmu_idx)
            elif oriented == "input":
                return self.solve_dual_input_oriented(dmu_idx)
            elif oriented == "output":
                return self.solve_dual_output_oriented(dmu_idx)
            else:
                raise ValueError("unknown dual oriented: {}.".format(oriented))
        else:
            raise ValueError("unknown problem type: {}.".format(prob_type))
