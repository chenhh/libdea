import numpy as np
from ccr import CCR

# n_dmu = 100
# dim_input, dim_output = 4, 3
# X = np.random.rand(n_dmu, dim_input)
# Y = np.random.rand(n_dmu, dim_output)
#
# inst = CCR(X,Y)
# print inst.n_dmu
# print inst.dim_input
# print inst.dim_output
# print inst.solver
# print inst.solve(0, "primal", "input")
#
#
import sys
sys.path.append('../../')
from time import time
import ccr
from libdea.ccr import CCR as CCR_py

def test_ccr_table_5_2(precision=2):
    n_dmu = 100
    dim_input, dim_output = 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    cum_c, cum_py, cum_py2 = 0, 0, 0
    cnt = 3
    for idx in xrange(cnt):
        t0 = time()
	instance = ccr.CCR(X,Y)
        for rdx in xrange(X.shape[0]):
            res = instance.solve(rdx, "primal", "input")
            # assert round(eff[rdx, 0] - res['efficiency'], precision) == 0
        print "c CCR: {} secs".format(time()-t0)
        cum_c += time()-t0

        t1 = time()
        instance = CCR_py(X,Y)
        for rdx in xrange(X.shape[0]):
            res = instance.solve(rdx, "primal", "input")
        print "py CCR: {} secs".format(time()-t1)
        cum_py += time()-t1

        t2 = time()
        instance.solve_batch_primal_input_oriented()
        print "py batch CCR: {} secs".format(time()-t2)
        cum_py2 += time() -t2

    print "c CCR, py_CCR, batch_py_CCR avg:{}, {} {} secs".format(
        cum_c/cnt, cum_py/cnt, cum_py2/cnt)

if __name__ == '__main__':


    test_ccr_table_5_2()
