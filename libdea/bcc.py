# -*- coding: utf-8 -*-
"""
Authors: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

from __future__ import division
import numpy as np
from pyomo.environ import *
from .basic_model import (AbstractDEAModel, AbstractTemporalDEAModel)
from . import (DEFAULT_SOLVER,)

class BCC(AbstractDEAModel):
    """
    data envelopment analysis model, which was proposed by Banker, Charnes,
    and Cooper.
    """

    def __init__(self, X, Y, solver=DEFAULT_SOLVER):
        """
        Parameters
        -----------
        X: numpy.array, shape: (n_dmu, dim_input)
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        Y: numpy.array, shape: (n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        solver: str,  the solver supported by pyomo (default: cplex)
        """
        super(BCC, self).__init__(X, Y, solver, "semi_positive")

    def solve_primal(self, dmu_idx, oriented="input"):
        """
        Parameters:
        -------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}

        Returns
        ---------
        model_name: str, model_name,
        dmu_idx: integer, index of the DMU,
        efficiency": float, efficiency value of the DMU
        intensity, numpy.array, shape:(n_dmu,),
            projection vector for input and output matrix to efficient frontier
        in_slack, numpy.array, shape:(dim_input,)
            distance of each dimension of the ith input from current position
            to the efficient frontier.
        out_slack, numpy.array, shape:(dim_output,)
            distance of each dimension of the jth output from current position
            to the efficient frontier.
        reference_dmu, list, reference DMUs on the efficient frontier.
        is_efficient, integer,
            - 1: efficient frontier
            - 0: technical efficiency
            - -1: inefficient
        projection_x, numpy.array, shape: (dim_input,)
        projection_y, numpy.array, shape: (dim_output,)
        """
        if oriented in ("input", "output"):
            model_name = "BCC_primal_{}_oriented".format(oriented)
        else:
            raise ValueError("unknown BCC primal oriented: {}".format(oriented))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.efficiency = Var(within=Reals)
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # common data in constraint
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        # common constraint
        def cvx_boundary_constraint_rule(model):
            return (sum(model.intensity[rdx] for rdx in self.dmu_arr) == 1)

        instance.cvx_boundary_constraint = Constraint(
            rule=cvx_boundary_constraint_rule)

        # common objective
        def stage1_objective_rule(model):
            return instance.efficiency

        # stage1
        if oriented == "input":
            # constraint
            def input_constraint_rule(model, idx):
                return (model.efficiency * self.X[dmu_idx, idx] -
                        model.in_slack[idx] - frontier_X[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                        model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # objective
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                            sense=minimize)

        elif oriented == "output":
            # constraint
            def input_constraint_rule(model, idx):
                return (self.X[dmu_idx, idx] - frontier_X[idx] -
                        model.in_slack[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (model.efficiency * self.Y[dmu_idx, jdx] -
                        frontier_Y[jdx] + model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # objective
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                            sense=maximize)

        # solve stage 1
        instance = self._solve_model_feasible(instance, model_name)

        # get efficiency
        efficiency = instance.efficiency.value

        # reset constraint and objective
        instance.efficiency.fixed = True
        instance.objective1.deactivate()

        def stage2_objective_rule(model):
            in_slack_sum = sum(model.in_slack[idx] for idx in self.input_arr)
            out_slack_sum = sum(model.out_slack[jdx] for jdx in self.output_arr)
            return in_slack_sum + out_slack_sum

        # both input and output oriented are to maximize slack sum
        instance.objective2 = Objective(rule=stage2_objective_rule,
                                        sense=maximize)
        instance.objective2.activate()

        # solve stage 2
        instance = self._solve_model_feasible(instance, model_name)

        # release variable
        instance.efficiency.fixed = False

        # extract the results
        intensity = np.fromiter((
            instance.intensity[rdx].value
            for rdx in self.dmu_arr), np.float)
        reference_dmu = self.dmu_arr[intensity > 0]

        in_slack = np.fromiter((
            instance.in_slack[idx].value
            for idx in self.input_arr), np.float)
        out_slack = np.fromiter((
            instance.out_slack[jdx].value
            for jdx in self.output_arr), np.float)

        # projection
        if oriented == "input":
            projection_x = efficiency * self.X[dmu_idx] - in_slack
            projection_y = self.Y[dmu_idx] + out_slack
        elif oriented == "output":
            projection_x = self.X[dmu_idx] - in_slack
            projection_y = efficiency * self.Y[dmu_idx] + out_slack
            efficiency = 1./efficiency

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "efficiency": efficiency,
            "intensity": intensity,
            "in_slack": in_slack,
            "out_slack": out_slack,
            "reference_dmu": reference_dmu,
            "is_efficient": self.is_efficient(efficiency, in_slack, out_slack),
            "projection_x": projection_x,
            "projection_y": projection_y,
        }

    def solve_batch_primal(self, oriented="input"):

        if oriented in ("input", "output"):
            model_name = "BCC_batch_primal_{}_oriented".format(oriented)
        else:
            raise ValueError("unknown BCC batch primal oriented: {}".format(
                oriented))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # result arrays
        efficiency_arr = np.zeros(self.n_dmu)
        intensity_arr = np.zeros((self.n_dmu, self.n_dmu))
        reference_dmu_arr = []
        in_slack_arr = np.zeros((self.n_dmu, self.dim_input))
        out_slack_arr = np.zeros((self.n_dmu, self.dim_output))
        is_efficient_arr = np.zeros(self.n_dmu)
        projection_x_arr = np.zeros((self.n_dmu, self.dim_input))
        projection_y_arr = np.zeros((self.n_dmu, self.dim_output))

        # decision variables
        instance.efficiency = Var(within=Reals)
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # common data in constraint
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        # common constraint
        def cvx_boundary_constraint_rule(model):
            return (sum(model.intensity[rdx] for rdx in self.dmu_arr) == 1)

        instance.cvx_boundary_constraint = Constraint(
            rule=cvx_boundary_constraint_rule)

        # common objective
        def stage1_objective_rule(model):
            return model.efficiency

        if oriented == "input":
            # objective
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                                sense=minimize)

        elif oriented == "output":
            # objective
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                                sense=maximize)

        def stage2_objective_rule(model):
            in_slack_sum = sum(model.in_slack[idx]
                               for idx in self.input_arr)
            out_slack_sum = sum(model.out_slack[jdx]
                                for jdx in self.output_arr)
            return in_slack_sum + out_slack_sum

        # both input and output oriented are to maximize slack sum
        instance.objective2 = Objective(rule=stage2_objective_rule,
                                        sense=maximize)

        for dmu_idx in self.dmu_arr:
            if oriented == "input":
                # constraint
                def input_constraint_rule(model, idx):
                    return (model.efficiency * self.X[dmu_idx, idx] -
                            frontier_X[idx] - model.in_slack[idx] == 0.)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                            model.out_slack[jdx] == 0.)

            elif oriented == "output":
                # constraint
                def input_constraint_rule(model, idx):
                    return (self.X[dmu_idx, idx] - frontier_X[idx] -
                            model.in_slack[idx] == 0)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (model.efficiency * self.Y[dmu_idx, jdx] -
                            frontier_Y[jdx] + model.out_slack[jdx] == 0)

            # build constraints
            instance.input_constraint = Constraint(
                    self.input_arr, rule=input_constraint_rule)

            instance.output_constraint = Constraint(
                    self.output_arr, rule=output_constraint_rule)

            # solve stage 1
            instance.objective1.activate()
            instance.objective2.deactivate()
            instance = self._solve_model_feasible(instance, model_name)

            # get efficiency
            efficiency = instance.efficiency.value

            # reset constraint and objective
            instance.efficiency.fixed = True
            instance.objective1.deactivate()
            instance.objective2.activate()

            # solve stage 2
            instance = self._solve_model_feasible(instance, model_name)

            # release variable
            instance.efficiency.fixed = False

            # extract result
            intensity_arr[dmu_idx] = np.fromiter((
                instance.intensity[rdx].value
                for rdx in self.dmu_arr), np.float)
            reference_dmu_arr.append(self.dmu_arr[intensity_arr[dmu_idx] > 0])

            in_slack_arr[dmu_idx] = np.fromiter((
                instance.in_slack[idx].value
                for idx in self.input_arr), np.float)
            out_slack_arr[dmu_idx] = np.fromiter((
                instance.out_slack[jdx].value
                for jdx in self.output_arr), np.float)

            is_efficient_arr[dmu_idx] = self.is_efficient(
                efficiency, in_slack_arr[dmu_idx],
                out_slack_arr[dmu_idx])

            # projection
            if oriented == "input":
                projection_x_arr[dmu_idx] = (efficiency *
                                             self.X[dmu_idx] -
                                             in_slack_arr[dmu_idx])
                projection_y_arr[dmu_idx] = (self.Y[dmu_idx] +
                                             out_slack_arr[dmu_idx])
                efficiency_arr[dmu_idx] = efficiency
            elif oriented == "output":
                projection_x_arr[dmu_idx] = (self.X[dmu_idx] -
                                             in_slack_arr[dmu_idx])
                projection_y_arr[dmu_idx] = (efficiency *
                                             self.Y[dmu_idx] +
                                             out_slack_arr[dmu_idx])
                efficiency_arr[dmu_idx] = 1./efficiency

            # delete constraint and resolve the problem
            instance.del_component("input_constraint")
            instance.del_component("input_constraint_index")
            instance.del_component("output_constraint")
            instance.del_component("output_constraint_index")

        return {
            "model_name": model_name,
            "efficiency_arr": efficiency_arr,
            "intensity_arr": intensity_arr,
            "in_slack_arr": in_slack_arr,
            "out_slack_arr": out_slack_arr,
            "reference_dmu_arr": reference_dmu_arr,
            "is_efficient_arr": is_efficient_arr,
            "projection_x_arr": projection_x_arr,
            "projection_y_arr": projection_y_arr,
        }

    def solve_dual(self, dmu_idx, oriented):
        """
        Parameters
        --------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}

        Returns
        ---------
        model_name: str, model_name,
        dmu_idx: integer, index of the DMU,
        in_weight: numpy.array, shape:(dim_input,),
        out_weight: numpy.array, shape:(dim_output,),
        boundary_slack: float, slack value of projection exceed or surplus
        efficiency": float, efficiency value of the DMU
        """
        if oriented in ("input", "output"):
            model_name = "BCC_dual_{}_oriented".format(oriented)
        else:
            raise ValueError("unknown BCC dual oriented: {}".format(oriented))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.in_weight = Var(self.input_arr, within=NonNegativeReals)
        instance.out_weight = Var(self.output_arr, within=NonNegativeReals)
        instance.boundary_slack = Var(within=Reals)

        # common data
        in_sum_arr = [sum(instance.in_weight[idx] * self.X[rdx, idx]
                          for idx in self.input_arr)
                      for rdx in self.dmu_arr]
        out_sum_arr = [sum(instance.out_weight[jdx] * self.Y[rdx, jdx]
                           for jdx in self.output_arr)
                       for rdx in self.dmu_arr]

        if oriented == "input":
            # constraint
            def efficient_constraint_rule(model, rdx):
                return (out_sum_arr[rdx] - in_sum_arr[rdx] -
                        model.boundary_slack <= 0)

            instance.efficient_constraint = Constraint(
                self.dmu_arr, rule=efficient_constraint_rule)

            # constraint
            def normalized_constraint_rule(model):
                return in_sum_arr[dmu_idx] == 1

            instance.normalized_constraint = Constraint(
                rule=normalized_constraint_rule)

            # objective
            def objective_rule(model):
                return out_sum_arr[dmu_idx] - model.boundary_slack

            instance.objective = Objective(rule=objective_rule, sense=maximize)

        elif oriented == "output":
            # constraint
            def efficient_constraint_rule(model, rdx):
                return (in_sum_arr[rdx] - out_sum_arr[rdx] -
                        model.boundary_slack >= 0)

            instance.efficient_constraint = Constraint(
                self.dmu_arr, rule=efficient_constraint_rule)

            # constraint
            def normalized_constraint_rule(model):
                return out_sum_arr[dmu_idx] == 1

            instance.normalized_constraint = Constraint(
                rule=normalized_constraint_rule)

            # objective
            def objective_rule(model):
                return in_sum_arr[dmu_idx] - model.boundary_slack

            instance.objective = Objective(rule=objective_rule, sense=minimize)

        # solve
        instance = self._solve_model_feasible(instance, model_name)

        boundary_slack = instance.boundary_slack.value
        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "in_weight": np.fromiter((
                instance.in_weight[idx].value
                for idx in self.input_arr), np.float),
            "out_weight": np.fromiter((
                instance.out_weight[jdx].value
                for jdx in self.output_arr), np.float),
            "boundary_slack": boundary_slack,
            "efficiency": instance.objective(),
        }

    def solve_batch_dual(self, oriented):
        if oriented in ("input", "output"):
            model_name = "BCC_batch_dual_{}_oriented".format(oriented)
        else:
            raise ValueError("unknown BCC batch dual oriented: {}".format(
                oriented))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # result arrays
        in_weight_arr = np.zeros((self.n_dmu, self.dim_input))
        out_weight_arr = np.zeros((self.n_dmu, self.dim_output))
        efficiency_arr = np.zeros(self.n_dmu)
        boundary_slack_arr = np.zeros(self.n_dmu)

        # decision variables
        instance.in_weight = Var(self.input_arr, within=NonNegativeReals)
        instance.out_weight = Var(self.output_arr, within=NonNegativeReals)
        instance.boundary_slack = Var(within=Reals)

        # common data in constraint
        in_sum_arr = [sum(instance.in_weight[idx] * self.X[rdx, idx]
                          for idx in self.input_arr)
                      for rdx in self.dmu_arr]
        out_sum_arr = [sum(instance.out_weight[jdx] * self.Y[rdx, jdx]
                           for jdx in self.output_arr)
                       for rdx in self.dmu_arr]

        for dmu_idx in self.dmu_arr:
            if oriented == "input":
                # constraint
                def efficient_constraint_rule(model, rdx):
                    return (out_sum_arr[rdx] - in_sum_arr[rdx] -
                            model.boundary_slack <= 0)

                instance.efficient_constraint = Constraint(
                    self.dmu_arr, rule=efficient_constraint_rule)

                # constraint
                def normalized_constraint_rule(model):
                    return in_sum_arr[dmu_idx] == 1

                instance.normalized_constraint = Constraint(
                    rule=normalized_constraint_rule)

                # objective
                def objective_rule(model):
                    return out_sum_arr[dmu_idx] - model.boundary_slack

                instance.objective = Objective(rule=objective_rule,
                                               sense=maximize)

            elif oriented == "output":
                # constraint
                def efficient_constraint_rule(model, rdx):
                    return (in_sum_arr[rdx] - out_sum_arr[rdx] -
                            model.boundary_slack >= 0)

                instance.efficient_constraint = Constraint(
                    self.dmu_arr, rule=efficient_constraint_rule)

                # constraint
                def normalized_constraint_rule(model):
                    return out_sum_arr[dmu_idx] == 1

                instance.normalized_constraint = Constraint(
                    rule=normalized_constraint_rule)

                # objective
                def objective_rule(model):
                    return in_sum_arr[dmu_idx] - model.boundary_slack

                instance.objective = Objective(rule=objective_rule,
                                               sense=minimize)

            # solve
            instance = self._solve_model_feasible(instance, model_name)

            # extract result
            in_weight_arr[dmu_idx] = np.fromiter((
                instance.in_weight[idx].value
                for idx in self.input_arr), np.float)

            out_weight_arr[dmu_idx] = np.fromiter((
                instance.out_weight[jdx].value
                for jdx in self.output_arr), np.float)

            efficiency_arr[dmu_idx] = instance.objective()
            boundary_slack_arr[dmu_idx] = instance.boundary_slack.value

            # delete constraint and resolve the problem
            instance.del_component('efficient_constraint')
            instance.del_component('efficient_constraint_index')
            instance.del_component("normalized_constraint")
            instance.del_component("objective")

        return {
            "model_name": model_name,
            "in_weight_arr": in_weight_arr,
            "out_weight_arr": out_weight_arr,
            "efficiency_arr": efficiency_arr,
            "boundary_slack_arr": boundary_slack_arr,
        }

    @staticmethod
    def is_efficient(efficiency, in_slack, out_slack, precision=4):
        """
        Parameters
        -------------
            - efficiency, float, efficiency value of an DMU
            - in_slack, numpy.array, float, shape:(dim_input,)
            - out_slack, numpy.array, float, shape:(dim_output,)

        Returns
        --------------
        return 1: efficient frontier
               0: technical efficiency
               -1: inefficient
        """
        # technical efficiency
        if round(efficiency - 1, precision) == 0:
            slack_sum = in_slack.sum() + out_slack.sum()
            if round(slack_sum, precision) == 0:
                return 1
            else:
                return 0
        else:
            # inefficient
            return -1

    def return_to_scale(self, dmu_idx, projection_x, projection_y,
                        orig_boundary_slack,oriented="input", precision=4):
        """
        Parameters
        -----------
        dmu_idx: integer, index of the DMU
        projection_x: numpy.array, shape: (dim_input,)
            The projection of input data to efficient frontier.
            It is gotten by solving primal problem.

        projection_y: numpy.array, shape: (dim_output,)
            The projection of output data to efficient frontier.
            It is gotten by solving primal problem.

        orig_boundary_slack: float,
            if orig_boundary_slack < 0, then frontier may be CRS or IRS
            if orig_boundary_slack > 0, then frontier may be CRS or DRS
            because the original model may get multiple solutions,
            it requires 2nd stage to ensure the RTS type.
            It is gotten by solving dual problem.

        Returns:
        ----------
            - "CRS" if boundary_slack = 0
            - "IRS" if boundary_slack < 0
            - "DRS" if boundary_slack > 0
        """
        if round(orig_boundary_slack, precision) == 0:
            return {
                "rts": "CRS",
                "boundary_slack": orig_boundary_slack
            }
        if orig_boundary_slack < 0:
            model_name = "RTS_BCC_CIRS_{}".format(oriented)
        elif orig_boundary_slack > 0:
            model_name = "RTS_BCC_CDRS_{}".format(oriented)

        # concrete model
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.in_weight = Var(self.input_arr, within=NonNegativeReals)
        instance.out_weight = Var(self.output_arr, within=NonNegativeReals)

        if orig_boundary_slack < 0:
            # CRS or IRS, to maximize boundary slack
            instance.boundary_slack = Var(within=NonPositiveReals)

        elif orig_boundary_slack > 0:
            # CRS or DRS, to minimize boundary slack
            instance.boundary_slack = Var(within=NonNegativeReals)

        # common data in constraint
        orig_weighted_X = [sum(self.X[rdx, idx] * instance.in_weight[idx]
                               for idx in self.input_arr)
                           for rdx in self.dmu_arr]
        orig_weighted_Y = [sum(self.Y[rdx, jdx] * instance.out_weight[jdx]
                               for jdx in self.output_arr)
                           for rdx in self.dmu_arr]
        proj_in_sum = sum(projection_x[idx] * instance.in_weight[idx]
                          for idx in self.input_arr)
        proj_out_sum = sum(projection_y[jdx] * instance.out_weight[jdx]
                           for jdx in self.output_arr)

        if oriented == "input":
            # constraint
            def orig_data_constraint_rule(model, rdx):
                return (-orig_weighted_X[rdx] + orig_weighted_Y[rdx]
                        - model.boundary_slack <= 0)

            # constraint
            def proj_data_constraint_rule(model):
                return (-proj_in_sum + proj_out_sum -
                        model.boundary_slack <= 0)

            # constraint
            def normalized_input_constraint_rule(model):
                return proj_in_sum == 1

            # constraint
            def normalized_output_constraint_rule(model):
                return proj_out_sum - model.boundary_slack == 1

        elif oriented == "output":
            # constraint
            def orig_data_constraint_rule(model, rdx):
                return (orig_weighted_X[rdx] - orig_weighted_Y[rdx]
                        - model.boundary_slack >= 0)

            # constraint
            def proj_data_constraint_rule(model):
                return (proj_in_sum - proj_out_sum -
                        model.boundary_slack >= 0)

            # constraint
            def normalized_input_constraint_rule(model):
                return proj_in_sum - model.boundary_slack == 1

            # constraint
            def normalized_output_constraint_rule(model):
                return proj_out_sum  == 1

        instance.orig_data_constraint = Constraint(
                [rdx for rdx in self.dmu_arr if rdx != dmu_idx],
                rule=orig_data_constraint_rule)

        instance.proj_data_constraint = Constraint(
                rule=proj_data_constraint_rule)

        instance.normalized_input_constraint = Constraint(
                rule=normalized_input_constraint_rule)

        instance.normalized_output_constraint = Constraint(
                rule=normalized_output_constraint_rule)

        # objective
        def objective_rule(model):
            return model.boundary_slack

        if orig_boundary_slack < 0:
            # CRS or IRS
            instance.objective = Objective(rule=objective_rule,
                                           sense=maximize)
        elif orig_boundary_slack > 0:
            # CRS or DRS
            instance.objective = Objective(rule=objective_rule,
                                           sense=minimize)

        # solve
        instance = self._solve_model_feasible(instance, model_name)

        boundary_slack = instance.boundary_slack.value

        if round(boundary_slack, precision) == 0:
            rts = "CRS"

        elif boundary_slack > 0:
            if oriented == "input":
                rts = "DRS"
            elif oriented == "output":
                rts = "IRS"

        elif boundary_slack < 0:
            if oriented == "input":
                rts = "IRS"
            elif oriented == "output":
                rts = "DRS"

        return {
            "rts": rts,
            "boundary_slack": boundary_slack
        }


    def batch_return_to_scale(self, projection_x_arr, projection_y_arr,
                              orig_boundary_slack_arr,
                              oriented= "input", precision=4):

        model_name = "batch_BCC_RTS_{}".format(oriented)
        # concrete model
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.in_weight = Var(self.input_arr, within=NonNegativeReals)
        instance.out_weight = Var(self.output_arr, within=NonNegativeReals)

        # common data in constraint
        orig_weighted_X = [sum(self.X[rdx, idx] * instance.in_weight[idx]
                               for idx in self.input_arr)
                           for rdx in self.dmu_arr]
        orig_weighted_Y = [sum(self.Y[rdx, jdx] * instance.out_weight[jdx]
                               for jdx in self.output_arr)
                           for rdx in self.dmu_arr]
        proj_in_sum_arr = [sum(projection_x_arr[rdx, idx] *
                               instance.in_weight[idx]
                               for idx in self.input_arr)
                           for rdx in self.dmu_arr]
        proj_out_sum_arr = [sum(projection_y_arr[rdx, jdx] *
                                instance.out_weight[jdx]
                                for jdx in self.output_arr)
                            for rdx in self.dmu_arr]

        rts_results = []
        for dmu_idx in self.dmu_arr:
            # redefine decision variable
            if round(orig_boundary_slack_arr[dmu_idx], precision) == 0:
                rts_results.append({
                    "rts": "CRS",
                    "boundary_slack": orig_boundary_slack_arr[dmu_idx]
                })
                continue

            elif orig_boundary_slack_arr[dmu_idx] < 0:
                # CRS or IRS, to maximize boundary slack
                instance.boundary_slack = Var(within=NonPositiveReals)

            elif orig_boundary_slack_arr[dmu_idx] > 0:
                # CRS or DRS, to minimize boundary slack
                instance.boundary_slack = Var(within=NonNegativeReals)

            if oriented == "input":
                # constraint
                def orig_data_constraint_rule(model, rdx):
                    return (-orig_weighted_X[rdx] + orig_weighted_Y[rdx]
                            - model.boundary_slack <= 0)

                # constraint
                def proj_data_constraint_rule(model):
                    return (-proj_in_sum_arr[dmu_idx] +
                            proj_out_sum_arr[dmu_idx] -
                            model.boundary_slack <= 0)

                # constraint
                def normalized_input_constraint_rule(model):
                    return proj_in_sum_arr[dmu_idx] == 1

                # constraint
                def normalized_output_constraint_rule(model):
                    return (proj_out_sum_arr[dmu_idx] -
                            model.boundary_slack == 1)

            elif oriented == "output":
                # constraint
                def orig_data_constraint_rule(model, rdx):
                    return (orig_weighted_X[rdx] - orig_weighted_Y[rdx]
                            - model.boundary_slack >= 0)

                # constraint
                def proj_data_constraint_rule(model):
                    return (proj_in_sum_arr[dmu_idx] -
                            proj_out_sum_arr[dmu_idx] -
                            model.boundary_slack >= 0)

                # constraint
                def normalized_input_constraint_rule(model):
                    return (proj_in_sum_arr[dmu_idx] -
                            model.boundary_slack == 1)

                # constraint
                def normalized_output_constraint_rule(model):
                    return proj_out_sum_arr[dmu_idx]  == 1

            instance.orig_data_constraint = Constraint(
                    [rdx for rdx in self.dmu_arr if rdx != dmu_idx],
                    rule=orig_data_constraint_rule)

            instance.proj_data_constraint = Constraint(
                    rule=proj_data_constraint_rule)

            instance.normalized_input_constraint = Constraint(
                    rule=normalized_input_constraint_rule)

            instance.normalized_output_constraint = Constraint(
                    rule=normalized_output_constraint_rule)

            # objective
            def objective_rule(model):
                return model.boundary_slack

            if orig_boundary_slack_arr[dmu_idx] < 0:
                # CRS or IRS
                instance.objective = Objective(rule=objective_rule,
                                               sense=maximize)

            elif orig_boundary_slack_arr[dmu_idx] > 0:
                # CRS or DRS
                instance.objective = Objective(rule=objective_rule,
                                               sense=minimize)

            # solve
            instance = self._solve_model_feasible(instance, model_name)

            # extract result
            boundary_slack = instance.boundary_slack.value

            if round(boundary_slack, precision) == 0:
                rts = "CRS"
            elif boundary_slack > 0:
                if oriented == "input":
                    rts = "DRS"
                elif oriented == "output":
                    rts = "IRS"
            else:
                if oriented == "input":
                    rts = "IRS"
                elif oriented == "output":
                    rts = "DRS"

            rts_results.append({
                "rts": rts,
                "boundary_slack": boundary_slack
            })

            # delete variable
            instance.del_component('boundary_slack')

            # delete constraints
            instance.del_component("orig_data_constraint")
            instance.del_component("orig_data_constraint_index")
            instance.del_component("proj_data_constraint")
            instance.del_component("normalized_input_constraint")
            instance.del_component("normalized_output_constraint")
            instance.del_component("objective")

        return rts_results


class Temporal_BCC(AbstractTemporalDEAModel):

     def __init__(self, X, Y, relative_X, relative_Y, solver=DEFAULT_SOLVER):
        """
        Parameters
        -----------
        X: numpy.array, shape: (n_dmu, dim_input)
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        Y: numpy.array, shape: (n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        relative_X: None or numpy.array, shape: (n_dmu, dim_input)
        relative_Y: None or numpy.array, shape: (n_dmu, dim_output)
            both variables are for providing reference frontier for current
            data X and Y.

        solver: str,  the solver supported by pyomo (default: cplex)
        """
        super(Temporal_BCC, self).__init__(X, Y, relative_X, relative_Y,
                                                solver, "semi_positive")


     def solve_batch(self, oriented):

        if oriented in ("input", "output"):
            model_name = "InterTemporal_BCC_batch_{}_oriented".format(oriented)
        else:
            raise ValueError("unknown InterTemporal_BCC "
                             "batch oriented: {}".format(oriented))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # result arrays
        efficiency_arr = np.zeros(self.n_dmu)

        # decision variables
        instance.efficiency = Var(within=Reals)
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)

        # common data in constraint
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        # common constraint
        def cvx_boundary_constraint_rule(model):
            return (sum(model.intensity[rdx] for rdx in self.dmu_arr) == 1)

        instance.cvx_boundary_constraint = Constraint(
            rule=cvx_boundary_constraint_rule)

        # common objective
        def stage1_objective_rule(model):
            return model.efficiency

        if oriented == "input":
            # objective
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                                sense=minimize)

        elif oriented == "output":
            # objective
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                                sense=maximize)

        for dmu_idx in self.dmu_arr:
            if oriented == "input":
                # constraint
                def input_constraint_rule(model, idx):
                    return (model.efficiency * self.X[dmu_idx, idx] -
                            frontier_X[idx] >= 0.)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] <= 0.)

            elif oriented == "output":
                # constraint
                def input_constraint_rule(model, idx):
                    return (self.X[dmu_idx, idx] - frontier_X[idx] >= 0)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (model.efficiency * self.Y[dmu_idx, jdx] -
                            frontier_Y[jdx] <= 0)

            # build constraints
            instance.input_constraint = Constraint(
                    self.input_arr, rule=input_constraint_rule)

            instance.output_constraint = Constraint(
                    self.output_arr, rule=output_constraint_rule)

            # solve stage 1
            instance.objective1.activate()
            instance = self._solve_model_feasible(instance, model_name)

            # get efficiency
            efficiency = instance.efficiency.value

            if oriented == "input":
                efficiency_arr[dmu_idx] = efficiency

            elif oriented == "output":
                efficiency_arr[dmu_idx] = 1./efficiency

            # delete constraint and resolve the problem
            instance.del_component("input_constraint")
            instance.del_component("input_constraint_index")
            instance.del_component("output_constraint")
            instance.del_component("output_constraint_index")

        return {
            "model_name": model_name,
            "efficiency_arr": efficiency_arr,
        }