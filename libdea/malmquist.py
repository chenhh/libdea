# -*- coding: utf-8 -*-
"""
Authors: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

from __future__ import division
import numpy as np
from pyomo.environ import *
from libdea import (ccr, bcc, additive, sbm)

from basic_model import AbstractDEAPanelModel
from . import (DEFAULT_SOLVER,)

class Malmquist(AbstractDEAPanelModel):

    def __init__(self, panel_X, panel_Y, solver=DEFAULT_SOLVER,
                 dea_model="CCR", oriented=None):
        """
        Parameters
        -----------
        panel_X: numpy.array, shape: (n_period, n_dmu, dim_input)
            - n_period is the number of period of data
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        panel_Y: numpy.array, shape: (n_period, n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        solver: str, the solver supported by pyomo (default: cplex)
        dea_model: str, {CCR. BCC, additive, SBM}
        oriented: str, {None, "input", "output"}
        """
        super(Malmquist, self).__init__(panel_X, panel_Y, solver)

        # pre-processing dea_model
        if dea_model == "CCR":
            self.dea_model = ccr.CCR
            if not oriented in ("input", "output"):
                raise ValueError("unknown CCR model oriented: {}".format(
                    oriented))

        elif dea_model == "BCC":
            self.dea_model = bcc.BCC
            if not oriented in ("input", "output"):
                raise ValueError("unknown CCR model oriented: {}".format(
                    oriented))

        elif dea_model  == "additive":
            self.dea_model = additive.Additive
            if oriented is not None:
                raise ValueError("unknown Additive model oriented: {}".format(
                    oriented))

        elif dea_model  == "SBM":
            self.dea_model = sbm.SBM
        else:
            raise ValueError("Unknown DEA model: {}".format(dea_model))

        self.oriented = oriented
        self.dea_model_name = dea_model


    def within_efficiency(self, period_idx):
        """
        DMU efficiency at s-th period according to s-th frontier.
        """
        if not 0<=period_idx <= self.n_period:
            raise ValueError("within_efficiency period index out of "
                             "range. {}".format(period_idx))

        X, Y = self.X[period_idx], self.Y[period_idx]

        # batch solve primal problem
        instance = self.dea_model(X, Y)
        res = instance.solve_batch("primal", self.oriented)

        return res


    def inter_temporal_efficiency(self, period_idx):
        """
        DMU efficiency at s-th period according to (s+1)-th frontier or
        DMU efficiency at (s+1)-th period according to s-th frontier
        """
        if not 0<=period_idx < self.n_period:
            raise ValueError("inter_temporal_efficiency period index out of "
                             "range. {}".format(period_idx))




    def catch_up(self, dmu_idx):
        """
        tracking an efficient ratio of a DMU in consecutive periods.
        """

    def frontier_shift(self):
        """
        tracking efficient frontier shift in consecutive periods.
        """



