# -*- coding: utf-8 -*-
"""
Authors: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

from __future__ import division
import numpy as np
from pyomo.environ import *
from pyomo.opt import (TerminationCondition, SolverStatus )
from . import (DEFAULT_SOLVER, SOLVER_IO, KEEPFILES)


class ValidMixin(object):
    @staticmethod
    def valid_positive(data):
        """
        Parameters
        ------------
        data: numpy.array,
             all values in an observation > 0,
        """
        if not np.all(data > 0):
            raise ValueError("The data is not all positive.\n {}".format(data))

    @staticmethod
    def valid_semi_positive(data):
        """
        Parameters
        ------------
        data: numpy.array, shape: (M, N)
            - M is the number of observations, and
            - N is the dimension of an observation,
         each row is an observation, and all values in an observation >=0,
         but, the observation is not zero.
        """
        if len(data.shape) != 2:
            raise ValueError('The dimension of data is not 2, but {}'.format(
                len(data.shape)))

        if not np.all(data >= 0):
            raise ValueError("The data is not all positive.\n {}".format(data))

        # a row in the data must not be a zero vector
        if np.any(np.all(data == 0, axis=1)):
            raise ValueError("Some data is zero vector.\n {}".format(data))

    @staticmethod
    def valid_dimension(arr1, arr2):
        """
        Parameters
        ------------
        arr1: numpy.array, shape: (a, b)
        arr2: numpy.array, shape: (c, d)
        """
        if arr1.shape != arr2.shape:
            raise ValueError('mismatch dimension {}, {}'.format(arr1.shape,
                                                                arr2.shape))


class AbstractDEAModel(ValidMixin):
    """ DEA model for cross-section data """

    def __init__(self, X, Y, solver=DEFAULT_SOLVER, data_constraint=None):
        """
        Parameters
        -----------
        X: numpy.array, shape: (n_dmu, dim_input)
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        Y: numpy.array, shape: (n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        solver: str,  the solver supported by pyomo (default: cplex)
        data_constraint:  {None, "positive", "semi_positive"}
        """
        # check  dimension
        if len(X.shape) != 2:
            raise ValueError("wrong input dimension: X:{}".format(X.shape))

        if len(Y.shape) != 2:
            raise ValueError("wrong output dimension: Y:{}".format(Y.shape))

        if X.shape[0] != Y.shape[0]:
            raise ValueError("mismatch DMU dimension, X:{}, Y:{}".format(
                X.shape[0], Y.shape[0]))

        # check data
        if data_constraint is None:
            pass
        elif data_constraint == "positive":
            self.valid_positive(X)
            self.valid_positive(Y)

        elif data_constraint == "semi_positive":
            self.valid_semi_positive(X)
            self.valid_semi_positive(Y)

        else:
            raise ValueError(
                "Unknown data constraint: {}".format(data_constraint))

        self.n_dmu, self.dim_input = X.shape
        self.dim_output = Y.shape[1]
        self.X = X
        self.Y = Y
        self.solver = solver
        self.dmu_arr = np.arange(self.n_dmu)
        self.input_arr = np.arange(self.dim_input)
        self.output_arr = np.arange(self.dim_output)


    def solve_primal(self, dmu_idx, oriented=None):
        """
        Parameters
        --------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {None, "input", "output"}
        """
        raise NotImplementedError('{} solve primal oriented: {}'.format(
            self.__name__), oriented)

    def solve_batch_primal(self, oriented=None):
        """
        solve all DMUs.

        Parameters
        --------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}
        """
        raise NotImplementedError('{} solve batch primal oriented: {}'.format(
            self.__name__), oriented)

    def solve_dual(self, dmu_idx, oriented=None):
        """
        Parameters
        --------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}
        """
        raise NotImplementedError('{} solve dual oriented: {}'.format(
            self.__name__), oriented)

    def solve_batch_dual(self, oriented=None):
        """
        solve all DMUs.

        Parameters
        --------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}
        """
        raise NotImplementedError('{} solve batch dual oriented: {}'.format(
            self.__name__), oriented)


    def solve(self, dmu_idx, prob_type="primal", oriented=None):
        """
        Parameters
        -----------
        dmu_idx: integer, index of the DMU
        prob_type: str, {"primal", "dual"}
        oriented: str, {None, "input", "output"}
        """
        if prob_type == "primal":
            return self.solve_primal(dmu_idx, oriented)
        elif prob_type == "dual":
            return self.solve_dual(dmu_idx, oriented)
        else:
            raise ValueError("unknown problem type: {}.".format(prob_type))

    def solve_batch(self, prob_type="primal", oriented=None):
        """
        Parameters
        -----------
        dmu_idx: integer, index of the DMU
        prob_type: str, {"primal", "dual"}
        oriented: str, {None, "input", "output"}
        """

        if prob_type == "primal":
            return self.solve_batch_primal(oriented)
        elif prob_type == "dual":
            return self.solve_batch_dual(oriented)
        else:
            raise ValueError("unknown batch problem type: {}.".format(
                prob_type))

    def _solve_model_feasible(self, instance, model_name):
        """
        for the model which must have solution.
        The method is very important, because it is called many times in
        almost all models.

        Parameters:
        -----------
        instance: Pyomo.ConcreteModel
        model_name: str, the model name for solving

        Returns:
        -----------
        instance: Pyomo.ConcreteModel, the object after solving
        """
        opt = SolverFactory(self.solver, solver_io=SOLVER_IO)
        results = opt.solve(instance, keepfiles=KEEPFILES)
        instance.solutions.load_from(results)

        if (results.solver.Status != SolverStatus.ok):
            raise ValueError("the solver status is not OK, but {}".format(
                results.solver.solver_status))

        if (results.solver.termination_condition ==
                TerminationCondition.infeasible):
            raise ValueError('the {} must have a solution, but the solver '
                         'return infeasible.'.format(model_name))
        return instance


class AbstractTemporalDEAModel(ValidMixin):
    """
    DEA model for different data and reference data,
    only solve the primal problem to get efficient value.
    """

    def __init__(self, X, Y, relative_X, relative_Y,
                 solver=DEFAULT_SOLVER, data_constraint=None):
        """
        Parameters
        -----------
        X: numpy.array, shape: (n_dmu, dim_input)
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        Y: numpy.array, shape: (n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        relative_X: None or numpy.array, shape: (n_dmu, dim_input)
        relative_Y: None or numpy.array, shape: (n_dmu, dim_output)
            both variables are for providing reference frontier for current
            data X and Y.

        solver: str,  the solver supported by pyomo (default: cplex)
        data_constraint:  {None, "positive", "semi_positive"}
        """
        # check  dimension
        if len(X.shape) != 2:
            raise ValueError("wrong input dimension: X:{}".format(X.shape))

        if len(Y.shape) != 2:
            raise ValueError("wrong output dimension: Y:{}".format(Y.shape))

        if X.shape[0] != Y.shape[0]:
            raise ValueError("mismatch DMU dimension, X:{}, Y:{}".format(
                X.shape[0], Y.shape[0]))

        if relative_X.shape != X.shape:
             raise ValueError("mismatch input and relative_input dimension, "
                              "X:{}, relative_X:{}".format(
                 X.shape, relative_X.shape))

        if relative_Y.shape != Y.shape:
             raise ValueError("mismatch output and relative_output dimension, "
                              "Y:{}, relative_Y:{}".format(
                 Y.shape, relative_Y.shape))

        # check data
        if data_constraint is None:
            pass
        elif data_constraint == "positive":
            self.valid_positive(X)
            self.valid_positive(Y)
            self.valid_positive(relative_X)
            self.valid_positive(relative_Y)

        elif data_constraint == "semi_positive":
            self.valid_semi_positive(X)
            self.valid_semi_positive(Y)
            self.valid_semi_positive(relative_X)
            self.valid_semi_positive(relative_Y)

        else:
            raise ValueError(
                "Unknown data constraint: {}".format(data_constraint))

        self.n_dmu, self.dim_input = X.shape
        self.dim_output = Y.shape[1]
        self.X = X
        self.Y = Y
        self.relative_X = X
        self.relative_Y = Y
        self.solver = solver
        self.dmu_arr = np.arange(self.n_dmu)
        self.input_arr = np.arange(self.dim_input)
        self.output_arr = np.arange(self.dim_output)

    def solve_batch(self, oriented=None):
        """
        Parameters
        -----------
        oriented: str, {None, "input", "output"}
        """
        raise NotImplementedError('{} batch solve oriented: {}'.format(
            self.__name__), oriented)


    def _solve_model_feasible(self, instance, model_name):
        """
        for the model which must have solution.
        The method is very important, because it is called many times in
        almost all models.

        Parameters:
        -----------
        instance: Pyomo.ConcreteModel
        model_name: str, the model name for solving

        Returns:
        -----------
        instance: Pyomo.ConcreteModel, the object after solving
        """
        opt = SolverFactory(self.solver, solver_io=SOLVER_IO)
        results = opt.solve(instance, keepfiles=KEEPFILES)
        instance.solutions.load_from(results)

        if (results.solver.Status != SolverStatus.ok):
            raise ValueError("the solver status is not OK, but {}".format(
                results.solver.solver_status))

        if (results.solver.termination_condition ==
                TerminationCondition.infeasible):
            raise ValueError('the {} must have a solution, but the solver '
                         'return infeasible.'.format(model_name))
        return instance


class AbstractDEAPanelModel(ValidMixin):
    """ DEA model for panel data """

    def __init__(self, panel_X, panel_Y, solver=DEFAULT_SOLVER):
        """
        Parameters
        -----------
        panel_X: numpy.array, shape: (n_period, n_dmu, dim_input)
            - n_period is the number of period of data
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        panel_Y: numpy.array, shape: (n_period, n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        solver: str, the solver supported by pyomo (default: cplex)
        """
        # valid data
        if len(panel_X.shape) != 3:
            raise ValueError("input data should be 3 dimension.")

        if len(panel_Y.shape) != 3:
            raise ValueError("output data should be 3 dimension.")

        if not np.all(panel_X >= 0):
            raise ValueError("input is not all positive.")

        if not np.all(panel_Y >= 0):
            raise ValueError("output is not all positive.")

        if not np.any(np.all(panel_X >= 0, axis=2)):
            raise ValueError("some input are zero vectors")

        if not np.any(np.all(panel_Y >= 0, axis=2)):
            raise ValueError("some input are zero vectors")

        if panel_X.shape[0] != panel_Y.shape[0]:
            raise ValueError("mismatch n_period between input and output.")

        if panel_X.shape[1] != panel_Y.shape[1]:
            raise ValueError("mismatch n_dmu between input and output.")

        self.n_period, self.n_dmu, self.dim_input = panel_X.shape
        self.dim_output = panel_Y.shape[2]

        self.X = panel_X
        self.Y = panel_Y
        self.solver = solver
        self.period_arr = np.arange(self.n_period)
        self.dmu_arr = np.arange(self.n_dmu)
        self.input_arr = np.arange(self.dim_input)
        self.output_arr = np.arange(self.dim_output)

    def solve_primal(self, periods, dmu_idx, oriented=None):
        """
        Parameters
        --------------
        periods: tuple, [start index of period, end index of period)
        dmu_idx: integer, index of the DMU
        oriented: str,  {None, "input", "output"}
        """
        raise NotImplementedError('{} solve primal oriented: {}'.format(
            self.__name__), oriented)

    def solve_batch_primal(self, periods, oriented=None):
        """
        solve all DMUs in the given periods.

        Parameters
        --------------
        periods: tuple, [start index of period, end index of period)
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}
        """
        raise NotImplementedError('{} solve batch primal oriented: {}'.format(
            self.__name__), oriented)

    def solve_dual(self, periods, dmu_idx, oriented=None):
        """
        Parameters
        --------------
        periods: tuple, [start index of period, end index of period)
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}
        """
        raise NotImplementedError('{} solve dual oriented: {}'.format(
            self.__name__), oriented)

    def solve_batch_dual(self, periods, oriented=None):
        """
        solve all DMUs in the given periods.

        Parameters
        --------------
        periods: tuple, [start index of period, end index of period)
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}
        """
        raise NotImplementedError('{} solve batch dual oriented: {}'.format(
            self.__name__), oriented)

    def solve(self, periods, dmu_idx, prob_type="primal", oriented=None):
        """
        solve dmu_idx efficiency in the given periods.

        Parameters
        -----------
        periods: tuple, [start index of period, end index of period)
        dmu_idx: integer, index of the DMU
        prob_type: str, {"primal", "dual"}
        oriented: str, {None, "input", "output"}
        """
        self.valid_periods(periods)

        if prob_type == "primal":
            return self.solve_primal(periods, dmu_idx, oriented)
        elif prob_type == "dual":
            return self.solve_dual(periods, dmu_idx, oriented)
        else:
            raise ValueError("unknown problem type: {}.".format(prob_type))

    def solve_batch(self, periods, prob_type="primal", oriented=None):
        """
        Parameters
        -----------
        periods: tuple, [start index of period, end index of period)
        dmu_idx: integer, index of the DMU
        prob_type: str, {"primal", "dual"}
        oriented: str, {None, "input", "output"}
        """
        self.valid_periods(periods)

        if prob_type == "primal":
            return self.solve_batch_primal(periods, oriented)
        elif prob_type == "dual":
            return self.solve_batch_dual(periods, oriented)
        else:
            raise ValueError("unknown batch problem type: {}.".format(
                prob_type))



    def valid_periods(self, periods):
        """
        the end_idx can be negative.

        Parameters
        -----------
        periods: tuple, [start index of period, end index of period)
        """
        if len(periods) != 2:
            raise ValueError("The dimension of periods should be 2, "
                             "but {}".format(len(periods)))
        start_idx, end_idx = periods
        if start_idx < 0:
            raise ValueError("Period start index {} must be >=0.".format(
                start_idx))

        if end_idx >= self.n_period:
            raise ValueError("Period end index {} must be < {}.".format(
                end_idx, self.n_period))
