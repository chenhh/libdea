# -*- coding: utf-8 -*-
"""
[Coopr07] Cooper, W. W., Seiford, L. M., & Tone, K. (2007). Data envelopment
analysis: a comprehensive text with models, applications, references and
DEA-solver software. Springer Science & Business Media.

"""

from __future__ import division
import numpy as np
from libdea import sbm

def test_sbm_primal(precision=3):
    """
    [Coopr07], table_4_3
    the input and output slack are not unique determined
    the results by cplex are the same as the book,
    but not consistent to cplex results.
    """
    X = np.array([[4,3],[7,3],[8,1],[4,2],[2,4],[10,1],[12,1],[10,1.5]])
    Y = np.array([[1,],[1,],[1,],[1,],[1,],[1,],[1,],[1,]])

    eff = np.array([[.833,],[.619,],[1,],[1,],[1,],[.9,],[.833,],[.733,]])
    ref_sets = np.array([[3,],[3,],[2,],[3,],[4,],[2,],[2,],[2,]])

    # single mode
    instance = sbm.SBM(X,Y)

    #batch mode
    res_bat = instance.solve_batch('primal')

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", None)
        assert ref_sets[rdx, 0] in res['reference_dmu']
        assert round(eff[rdx, 0] - res['efficiency'], precision) == 0
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                                                precision) == 0


def test_sbm_primal_2(precision=3):
    """  [Coopr07], table_4_4 """
    X = np.array([[1,1,1],[1,2,1],[1,1,2],[1,1,1],
                  [1,1,1],[2,2,2]])
    Y = np.array([[2,2,2],[2,2,2],[1,2,2],[1,2,1],
                  [2,1,0.5],[.5, .5, 1]])

    eff = np.array([[1,],[.833,],[.625,],[.6,],[.429,],[.15,]])

    instance = sbm.SBM(X,Y)

    #batch mode
    res_bat = instance.solve_batch('primal')

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", None)
        assert round(eff[rdx, 0] - res['efficiency'], precision) == 0
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                                                precision) == 0


def test_sbm_primal_input(precision=3):
    """ [Coopr07], table 5.1 """
    X = np.array([[20, 151], [19, 131], [25, 160], [27, 168], [22, 158],
                  [55, 255], [33, 235], [31, 206], [30, 244], [50, 268],
                  [53, 306], [38, 284]])

    Y = np.array([[100, 90], [150, 50], [160, 55], [180, 72], [94, 66],
                  [230, 90], [220, 88], [152, 80], [190, 100], [250, 100],
                  [260, 147], [250, 120]])

    eff = np.array([[1,], [1, ], [.852, ], [1, ], [.756, ], [.704, ],
                    [.895, ], [.774, ], [.905, ], [.781, ], [.866, ],
                    [.936, ],])

    instance = sbm.SBM(X,Y)

    #batch mode
    res_bat = instance.solve_batch('primal', 'input')

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "input")
        assert round(eff[rdx, 0] - res['efficiency'], precision) == 0
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                                                precision) == 0



def test_sbm_primal_batch_equality(precision=4):
    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    # single
    instance = sbm.SBM(X,Y)

    #batch mode
    res_bat = instance.solve_batch('primal')

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal")
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                                                precision) == 0
        assert round(res_bat['aux_arr'][rdx] - res['aux'],
                                                precision) == 0
        np.testing.assert_array_almost_equal(res_bat['intensity_arr'][rdx],
                                             res['intensity'])
        np.testing.assert_array_almost_equal(res_bat['in_slack_arr'][rdx],
                                             res['in_slack'])
        np.testing.assert_array_almost_equal(res_bat['out_slack_arr'][rdx],
                                             res['out_slack'])


def test_sbm_primal_input_batch_equality(precision=4):
    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    # single
    instance = sbm.SBM(X,Y)

    #batch mode
    res_bat = instance.solve_batch('primal', "input")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "input")
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                                                precision) == 0
        np.testing.assert_array_almost_equal(res_bat['intensity_arr'][rdx],
                                             res['intensity'])
        np.testing.assert_array_almost_equal(res_bat['in_slack_arr'][rdx],
                                             res['in_slack'])
        np.testing.assert_array_almost_equal(res_bat['out_slack_arr'][rdx],
                                             res['out_slack'])


def test_sbm_primal_output_batch_equality(precision=4):
    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    # single
    instance = sbm.SBM(X,Y)

    #batch mode
    res_bat = instance.solve_batch('primal', "output")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "output")
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                                                precision) == 0
        np.testing.assert_array_almost_equal(res_bat['intensity_arr'][rdx],
                                             res['intensity'])
        np.testing.assert_array_almost_equal(res_bat['in_slack_arr'][rdx],
                                             res['in_slack'])
        np.testing.assert_array_almost_equal(res_bat['out_slack_arr'][rdx],
                                             res['out_slack'])


def test_sbm_dual_batch_equality(precision=4):
    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    # single
    instance = sbm.SBM(X,Y)

    #batch mode
    res_bat = instance.solve_batch('dual')

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "dual")
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                                                precision) == 0
        np.testing.assert_array_almost_equal(res_bat['in_weight_arr'][rdx],
                                             res['in_weight'])
        np.testing.assert_array_almost_equal(res_bat['out_weight_arr'][rdx],
                                             res['out_weight'])

