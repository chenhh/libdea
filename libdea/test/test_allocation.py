# -*- coding: utf-8 -*-
"""
[Coopr07] Cooper, W. W., Seiford, L. M., & Tone, K. (2007). Data envelopment
analysis: a comprehensive text with models, applications, references and
DEA-solver software. Springer Science & Business Media.

"""

from __future__ import division
import numpy as np
from libdea import allocation


def test_traditional_allocation_1(precision=3):
    """[Coopr07], table_8_1"""
    X = np.array([[3,2],[1, 3],[4, 6]])
    Y = np.array([[3,],[5,],[6,]])
    costs = np.array([[4,2], [4,2], [4,2] ])
    prices = np.array([[6,],[6,],[6,]])

    cost_eff = np.array([.375, 1, .429])
    revenue_eff = np.array([.9, 1, .6])
    profit_eff = np.array([.15, 1, .2])

    instance =allocation.TraditionalAllocation(X,Y, costs, prices)

    for dmu_idx in xrange(3):
        res = instance.solve(dmu_idx, prob_type="cost")
        assert round(res['efficiency'] - cost_eff[dmu_idx], precision) == 0

        res2 = instance.solve(dmu_idx, prob_type="price")
        assert round(res2['efficiency'] - revenue_eff[dmu_idx], precision) == 0

        res3 = instance.solve(dmu_idx, prob_type="profit")
        assert round(res3['efficiency'] - profit_eff[dmu_idx], precision) == 0


def test_traditional_allocation_2(precision=3):
    """[Coopr07], table_8_3 """
    X = np.array([[10,10],[10, 10],[5, 2]])
    Y = np.array([[1,],[1,],[1,]])
    costs = np.array([[10,10], [1,1], [3,6] ])
    cost_eff = np.array([.35, .35, 1])

    instance =allocation.TraditionalAllocation(X,Y, costs, solver="cplex")

    for dmu_idx in xrange(3):
        res = instance.solve(dmu_idx, prob_type="cost")
        assert round(res['efficiency'] - cost_eff[dmu_idx], precision) == 0


def test_traditional_allocation_cost_batch_equality(precision=4):
    n_dmu, dim_input, dim_output =20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)
    costs = np.random.rand(n_dmu, dim_input) + 10
    prices = np.random.rand(n_dmu, dim_output) + 10

    instance =allocation.TraditionalAllocation(X,Y, costs, prices)

    # batch result
    res_bat = instance.solve_batch("cost")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, prob_type="cost")
        np.testing.assert_almost_equal(
            res['efficiency'], res_bat['efficiency_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['input'], res_bat['input_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['output'], res_bat['output_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['in_slack'], res_bat['in_slack_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['out_slack'], res_bat['out_slack_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['intensity'], res_bat['intensity_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['eff_cost'], res_bat['eff_cost_arr'][rdx], precision)


def test_traditional_allocation_price_batch_equality(precision=4):
    n_dmu, dim_input, dim_output =20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)
    costs = np.random.rand(n_dmu, dim_input) + 10
    prices = np.random.rand(n_dmu, dim_output) + 10

    instance =allocation.TraditionalAllocation(X,Y, costs, prices)

    # batch result
    res_bat = instance.solve_batch("price")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, prob_type="price")
        np.testing.assert_almost_equal(
            res['efficiency'], res_bat['efficiency_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['input'], res_bat['input_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['output'], res_bat['output_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['in_slack'], res_bat['in_slack_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['out_slack'], res_bat['out_slack_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['intensity'], res_bat['intensity_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['eff_price'], res_bat['eff_price_arr'][rdx], precision)



def test_traditional_allocation_profit_batch_equality(precision=4):
    n_dmu, dim_input, dim_output =20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)
    costs = np.random.rand(n_dmu, dim_input) + 10
    prices = np.random.rand(n_dmu, dim_output) + 10

    instance =allocation.TraditionalAllocation(X,Y, costs, prices)

    # batch result
    res_bat = instance.solve_batch("profit")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, prob_type="profit")
        np.testing.assert_almost_equal(
            res['efficiency'], res_bat['efficiency_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['input'], res_bat['input_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['output'], res_bat['output_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['in_slack'], res_bat['in_slack_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['out_slack'], res_bat['out_slack_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['intensity'], res_bat['intensity_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['eff_profit'], res_bat['eff_profit_arr'][rdx], precision)


def test_new_allocation_1(precision=3):
    """[Coopr07], table_8_3 """
    X = np.array([[10,10],[10, 10],[5, 2]])
    Y = np.array([[1,],[1,],[1,]])
    costs = np.array([[10,10], [1,1], [3,6] ])
    tech_eff = np.array([.1, 1, .8333])
    cost_eff = np.array([.1, 1, .7407])

    instance =allocation.Allocation(X,Y, costs, solver="cplex")

    for dmu_idx in xrange(3):
        # cost efficiency
        res = instance.solve(dmu_idx, prob_type="cost")
        # eff = res['cost']/np.dot(X[dmu_idx], costs[dmu_idx])
        assert round(res['efficiency'] - cost_eff[dmu_idx], precision) == 0

        # tech efficiency
        res2 = instance.solve(dmu_idx, prob_type="tech")
        assert round(res2['efficiency'] - tech_eff[dmu_idx], precision) == 0


def test_new_allocation_2(precision=3):
    """[Coopr07], table_8_4 """
    X = np.array([[20, 151], [19, 131], [25, 160], [27, 168], [22, 158],
                  [55, 255], [33, 235], [31, 206], [30, 244], [50, 268],
                  [53, 306], [38, 284],])
    Y = np.array([[100,90], [150, 50], [160, 55], [180, 72], [94, 66],
                  [230, 90], [220, 88], [152, 80], [190, 100], [250, 100],
                  [260, 147], [250, 120],])
    costs = np.array([[500, 100], [350, 80], [450, 90], [600, 120],
                      [300, 70], [450, 80], [500, 100], [450, 85],
                      [380, 76], [410, 75], [440, 80], [400, 70],])

    tech_eff = np.array([[.994,], [1, ], [.784, ], [.663, ], [1, ],
                         [.831, ], [.695, ], [.757, ], [.968, ], [.924, ],
                         [.995, ], [1, ],])

    cost_eff = np.array([[.959,], [1, ], [.724, ], [.624, ], [1, ],
                         [.634, ], [.693, ], [.726, ], [.953, ], [.776, ],
                         [.863, ], [1, ],])

    instance =allocation.Allocation(X,Y, costs, solver="cplex")

    for dmu_idx in xrange(X.shape[0]):
        # cost efficiency
        res = instance.solve(dmu_idx, prob_type="cost")
        assert round(res['efficiency'] - cost_eff[dmu_idx], precision) == 0

        # tech efficiency
        res2 = instance.solve(dmu_idx, prob_type="tech")
        assert round(res2['efficiency'] - tech_eff[dmu_idx], precision) == 0

def test_new_allocation_3(precision=4):
    n_dmu, dim_input, dim_output =20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)
    costs = np.random.rand(n_dmu, dim_input) + 10
    prices = np.random.rand(n_dmu, dim_output) + 10

    instance =allocation.Allocation(X,Y, costs, prices, solver="cplex")

    for dmu_idx in xrange(X.shape[0]):
        res = instance.solve(dmu_idx, prob_type="cost")
        res2 = instance.solve(dmu_idx, prob_type="tech")
        res3 = instance.solve(dmu_idx, prob_type="price")
        res4 = instance.solve(dmu_idx, prob_type="profit")

def test_new_allocation_tech_batch_equality(precision=4):
    n_dmu, dim_input, dim_output =20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)
    costs = np.random.rand(n_dmu, dim_input) + 10
    prices = np.random.rand(n_dmu, dim_output) + 10

    instance =allocation.Allocation(X,Y, costs, prices, solver="cplex")

    # batch result
    res_bat = instance.solve_batch("tech")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, prob_type="tech")
        np.testing.assert_almost_equal(
            res['efficiency'], res_bat['efficiency_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['input'], res_bat['input_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['output'], res_bat['output_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['in_slack'], res_bat['in_slack_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['out_slack'], res_bat['out_slack_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['intensity'], res_bat['intensity_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['eff_tech'], res_bat['eff_tech_arr'][rdx], precision)


def test_new_allocation_cost_batch_equality(precision=4):
    n_dmu, dim_input, dim_output =20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)
    costs = np.random.rand(n_dmu, dim_input) + 10
    prices = np.random.rand(n_dmu, dim_output) + 10

    instance =allocation.Allocation(X,Y, costs, prices, solver="cplex")

    # batch result
    res_bat = instance.solve_batch("cost")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, prob_type="cost")
        np.testing.assert_almost_equal(
            res['efficiency'], res_bat['efficiency_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['input'], res_bat['input_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['output'], res_bat['output_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['in_slack'], res_bat['in_slack_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['out_slack'], res_bat['out_slack_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['intensity'], res_bat['intensity_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['eff_cost'], res_bat['eff_cost_arr'][rdx], precision)


def test_new_allocation_price_batch_equality(precision=4):
    n_dmu, dim_input, dim_output =20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)
    costs = np.random.rand(n_dmu, dim_input) + 10
    prices = np.random.rand(n_dmu, dim_output) + 10

    instance =allocation.Allocation(X,Y, costs, prices, solver="cplex")

    # batch result
    res_bat = instance.solve_batch("price")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, prob_type="price")
        np.testing.assert_almost_equal(
            res['efficiency'], res_bat['efficiency_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['input'], res_bat['input_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['output'], res_bat['output_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['in_slack'], res_bat['in_slack_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['out_slack'], res_bat['out_slack_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['intensity'], res_bat['intensity_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['eff_price'], res_bat['eff_price_arr'][rdx], precision)


def test_new_allocation_profit_batch_equality(precision=4):
    n_dmu, dim_input, dim_output =20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)
    costs = np.random.rand(n_dmu, dim_input) + 10
    prices = np.random.rand(n_dmu, dim_output) + 10

    instance =allocation.Allocation(X,Y, costs, prices, solver="cplex")

    # batch result
    res_bat = instance.solve_batch("profit")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, prob_type="profit")
        np.testing.assert_almost_equal(
            res['efficiency'], res_bat['efficiency_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['input'], res_bat['input_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['output'], res_bat['output_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['in_slack'], res_bat['in_slack_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['out_slack'], res_bat['out_slack_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['intensity'], res_bat['intensity_arr'][rdx], precision)
        np.testing.assert_array_almost_equal(
            res['eff_profit'], res_bat['eff_profit_arr'][rdx], precision)
