# -*- coding: utf-8 -*-
"""
[Coopr07] Cooper, W. W., Seiford, L. M., & Tone, K. (2007). Data envelopment
analysis: a comprehensive text with models, applications, references and
DEA-solver software. Springer Science & Business Media.

"""

from __future__ import division
import numpy as np
from libdea import bcc


def test_bcc_primal_input(precision=2):
    """[Coopr07], table_4_2 """
    X = np.array([[2, ], [3, ], [2, ], [4, ], [6, ], [5, ], [6, ], [8, ]])
    Y = np.array([[1, ], [3, ], [2, ], [3, ], [5, ], [2, ], [3, ], [5, ]])
    eff = np.array(
        [[1, ], [1, ], [1, ], [.75, ], [1, ], [.4, ], [.5, ], [.75, ]])

    # single
    instance = bcc.BCC(X, Y)

    # batch
    res_bat = instance.solve_batch("primal", "input")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "input")
        assert eff[rdx, 0] == res['efficiency']
        assert eff[rdx, 0] == res_bat['efficiency_arr'][rdx]


def test_bcc_primal_input_2(precision=2):
    """[Coopr07], table_5_2 """
    X = np.array([
        [112, 1894, 2024205], [111, 1727, 1702017], [123, 1843, 2048014],
        [56, 792, 600796], [86, 1422, 1173778], [111, 1841, 2290274],
        [105, 1633, 1361498], [64, 889, 668236], [90, 1611, 1563930],
        [69, 1036, 654338], [73, 1011, 656794], [407, 17837, 53438938],
        [529, 18805, 52535262], [363, 15188, 51368976],
        [419, 20235, 78170694], [436, 13149, 29531193], [395, 13998, 52999340],
        [397, 15710, 56468571], [218, 8671, 16665573], [305, 11546, 31376180],
    ])

    Y = np.array([[13553, ],[16540, ], [14760, ], [3782, ], [8700, ],
                  [18590,], [9699, ], [5605, ], [9993, ], [3525, ], [3177, ],
                  [411149, ], [302901, ], [371364, ], [504422, ], [170835, ],
                  [399398, ], [353531, ], [112121, ], [186640, ],
                  ])

    eff = np.array([[.764, ],[1, ], [.8, ], [1, ], [.854, ], [.913, ],
                    [.777, ], [1, ], [.777, ], [.918, ], [.915, ], [1, ],
                    [.747, ], [.99, ], [1, ], [.743, ], [1, ], [.866, ],
                    [.854, ], [.766, ],
                    ])

    rts_arr = ['IRS', 'CRS', 'IRS', 'IRS', 'IRS', 'IRS', 'IRS', 'IRS', 'IRS',
           'IRS', 'IRS', 'CRS', 'CRS', 'IRS', 'CRS', 'CRS', 'CRS', 'IRS',
           'CRS', 'IRS']

    instance = bcc.BCC(X, Y)

    # batch
    res_bat = instance.solve_batch("primal", "input")
    res_bat_d = instance.solve_batch("dual", "input")

    bat_rts = instance.batch_return_to_scale( res_bat['projection_x_arr'],
                                            res_bat['projection_y_arr'],
                                            res_bat_d['boundary_slack_arr'])

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "input")
        assert round(eff[rdx, 0] - res['efficiency'], precision) == 0
        assert round(eff[rdx, 0] - res_bat['efficiency_arr'][rdx],
                     precision) == 0
        res_d = instance.solve(rdx, 'dual', 'input')

        rts = instance.return_to_scale(rdx, res['projection_x'],
        res['projection_y'], res_d['boundary_slack'])
        # print rts_arr[rdx], rts['rts'], bat_rts[rdx]['rts']
        assert rts_arr[rdx] == rts['rts'] == bat_rts[rdx]['rts']


def test_bcc_primal_input_output_1(precision=4):
    """[Coopr07], table 1-5, 5-1, 5-6 """
    X = np.array([
        [20, 151], [19, 131], [25, 160], [27, 168], [22, 158], [55, 255],
        [33, 235], [31, 206], [30, 244], [50, 268], [53, 306], [38, 284]
    ])

    Y = np.array([
        [100, 90], [150, 50], [160, 55], [180, 72], [94, 66], [230, 90],
        [220, 88], [152, 80], [190, 100], [250, 100], [260, 147], [250, 120]
    ])

    input_effs = np.array([
        1, 1, .8958,  1, .8818, .9389, 1, .7988, .9893, 1, 1, 1 ])

    output_effs = np.array([
        1, 1, .9245,  1, .7666, .9548, 1, .8259, .9899, 1, 1, 1 ])


    input_rts = ['CRS','CRS','CRS','CRS','CRS','DRS','DRS','DRS','DRS',
                 'DRS','DRS','DRS']
    output_rts = ['CRS','CRS','DRS','CRS','DRS','DRS','DRS','DRS',
                  'DRS','DRS','DRS','DRS']


    instance = bcc.BCC(X, Y)

    # batch
    res_bat = instance.solve_batch("primal", "input")
    res_bat_o = instance.solve_batch("primal", "output")

    for rdx in xrange(X.shape[0]):
        # input
        res = instance.solve(rdx, "primal", "input")
        res_d = instance.solve(rdx, "dual", "input")
        assert round(input_effs[rdx] - res['efficiency'], precision) == 0
        assert round(input_effs[rdx] - res_bat['efficiency_arr'][rdx],
                     precision) == 0

        rts_i = instance.return_to_scale(rdx, res['projection_x'],
                                         res['projection_y'],
                                         res_d['boundary_slack'])
        assert rts_i['rts'] == input_rts[rdx]

        # output
        res_o = instance.solve(rdx, "primal", "output")
        res_do = instance.solve(rdx, "dual", "output")
        assert round(output_effs[rdx] - res_o['efficiency'], precision) == 0
        assert round(output_effs[rdx] - res_bat_o['efficiency_arr'][rdx],
                     precision) == 0

        rts_o = instance.return_to_scale(rdx, res_o['projection_x'],
                                         res_o['projection_y'],
                                         res_do['boundary_slack'], "output")
        assert rts_o['rts'] == output_rts[rdx]


def test_bcc_dual_input_batch_equality(precision=4):
    """  test equality between single and batch mode """
    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = bcc.BCC(X, Y)
    res_bat = instance.solve_batch("dual", "input")
    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "dual", "input")
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                     precision) == 0
        assert round(res_bat['boundary_slack_arr'][rdx] - res['boundary_slack'],
                     precision) == 0
        np.testing.assert_array_almost_equal(res_bat['in_weight_arr'][rdx],
                                             res['in_weight'])
        np.testing.assert_array_almost_equal(res_bat['out_weight_arr'][rdx],
                                             res['out_weight'])


def test_bcc_dual_output_batch_equality(precision=4):
    """  test equality between single and batch mode """
    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = bcc.BCC(X, Y)
    res_bat = instance.solve_batch("dual", "output")
    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "dual", "output")
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                     precision) == 0
        assert round(res_bat['boundary_slack_arr'][rdx] - res['boundary_slack'],
                     precision) == 0
        np.testing.assert_array_almost_equal(res_bat['in_weight_arr'][rdx],
                                             res['in_weight'])
        np.testing.assert_array_almost_equal(res_bat['out_weight_arr'][rdx],
                                             res['out_weight'])


def test_bcc_primal_input_batch_equality(precision=4):
    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = bcc.BCC(X, Y)
    res_bat = instance.solve_batch("primal", "input")
    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "input")
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                     precision) == 0
        np.testing.assert_array_almost_equal(res_bat['intensity_arr'][rdx],
                                             res['intensity'])
        np.testing.assert_array_almost_equal(res_bat['in_slack_arr'][rdx],
                                             res['in_slack'])
        np.testing.assert_array_almost_equal(res_bat['out_slack_arr'][rdx],
                                             res['out_slack'])


def test_bcc_primal_output_batch_equality(precision=4):
    n_dmu, dim_input, dim_output =20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = bcc.BCC(X, Y)
    res_bat = instance.solve_batch("primal", "output")
    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "output")

        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                     precision) == 0
        np.testing.assert_array_almost_equal(res_bat['intensity_arr'][rdx],
                                             res['intensity'])
        np.testing.assert_array_almost_equal(res_bat['in_slack_arr'][rdx],
                                             res['in_slack'])
        np.testing.assert_array_almost_equal(res_bat['out_slack_arr'][rdx],
                                             res['out_slack'])

def test_bcc_rts_equaility():
    n_dmu, dim_input, dim_output =20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = bcc.BCC(X, Y)
    # input
    res_bat = instance.solve_batch("primal", "input")
    res_bat_d = instance.solve_batch("dual", "input")

    bat_rts = instance.batch_return_to_scale( res_bat['projection_x_arr'],
                                            res_bat['projection_y_arr'],
                                            res_bat_d['boundary_slack_arr'])

    # output
    res_bat_o = instance.solve_batch("primal", "output")
    res_bat_od = instance.solve_batch("dual", "output")


    bat_rts_o = instance.batch_return_to_scale( res_bat_o['projection_x_arr'],
                                            res_bat_o['projection_y_arr'],
                                            res_bat_od['boundary_slack_arr'],
                                                "output")

    for rdx in xrange(X.shape[0]):
        # input
        res = instance.solve(rdx, "primal", "input")
        res_d = instance.solve(rdx, 'dual', 'input')

        rts = instance.return_to_scale(rdx, res['projection_x'],
                res['projection_y'], res_d['boundary_slack'])
        assert rts['rts'] == bat_rts[rdx]['rts']

        # output
        res_o = instance.solve(rdx, "primal", "output")
        res_do = instance.solve(rdx, 'dual', 'output')

        rts_o = instance.return_to_scale(rdx, res_o['projection_x'],
                res_o['projection_y'], res_do['boundary_slack'], "output")
        assert rts_o['rts'] == bat_rts_o[rdx]['rts']



def test_inter_temporal_bcc_equality(precision=6):

    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = bcc.BCC(X, Y)
    instance2 = bcc.Temporal_BCC(X, Y, X, Y)

    # input oriented
    res_bat = instance.solve_batch("primal", "input")
    res_bat2 = instance2.solve_batch("input")

    np.testing.assert_array_almost_equal(
        res_bat['efficiency_arr'], res_bat2['efficiency_arr'], precision)

    # output oriented
    res_bat_o = instance.solve_batch("primal", "output")
    res_bat2_o = instance2.solve_batch("output")

    np.testing.assert_array_almost_equal(
        res_bat_o['efficiency_arr'], res_bat2_o['efficiency_arr'], precision)



if __name__ == '__main__':
#     # test_bcc_primal_input_output_1()
#     test_bcc_rts_equaility()
    test_inter_temporal_bcc_equality()