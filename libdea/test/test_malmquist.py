# -*- coding: utf-8 -*-
"""
Authors: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""


from __future__ import division
import numpy as np
from libdea import malmquist

def test_malmquist():
    X = np.arange(2*3*4).reshape((2,3,4))+1
    Y = np.arange(2*3*5).reshape((2,3,5))+1
    inst = malmquist.Malmquist(X, Y)

if __name__ == '__main__':
    test_malmquist()


