# -*- coding: utf-8 -*-
"""
[Coopr07] Cooper, W. W., Seiford, L. M., & Tone, K. (2007). Data envelopment
analysis: a comprehensive text with models, applications, references and
DEA-solver software. Springer Science & Business Media.

"""

from __future__ import division
import numpy as np
from libdea import ccr


def test_ccr_dual_input(precision=4):
    """ [Coopr07], example 2.1 """
    X = np.array([[2, ], [3, ], [3, ], [4, ], [5, ], [5, ], [6, ], [8, ]])
    Y = np.array([[1, ], [3, ], [2, ], [3, ], [4, ], [2, ], [3, ], [5, ]])

    # single mode
    instance = ccr.CCR(X, Y)

    # batch mode
    res_bat = instance.solve_batch("dual", "input")

    # node A
    res = instance.solve(0, "dual", "input")
    assert round(res['efficiency'] - 0.5, precision) == 0
    assert round(res['in_weight'][0] - 0.5, precision) == 0
    assert round(res['out_weight'][0] - 0.5, precision) == 0

    assert round(res_bat['efficiency_arr'][0] - 0.5, precision) == 0
    assert round(res_bat['in_weight_arr'][0,0] - 0.5, precision) == 0
    assert round(res_bat['out_weight_arr'][0,0] - 0.5, precision) == 0

    # node B
    res = instance.solve(1, "dual", "input")
    assert round(res['efficiency'] - 1, precision) == 0
    assert round(res['in_weight'][0] - 1 / 3, precision) == 0
    assert round(res['out_weight'][0] - 1 / 3, precision) == 0

    assert round(res_bat['efficiency_arr'][1] - 1, precision) == 0
    assert round(res_bat['in_weight_arr'][1,0] - 1 / 3, precision) == 0
    assert round(res_bat['out_weight_arr'][1,0] - 1 / 3, precision) == 0

    # node C
    res = instance.solve(2, "dual", "input")
    assert round(res['efficiency'] - 0.6667, precision) == 0
    assert round(res_bat['efficiency_arr'][2] - 0.6667, precision) == 0

    # node D
    res = instance.solve(3, "dual", "input")
    assert round(res['efficiency'] - 0.7500, precision) == 0
    assert round(res_bat['efficiency_arr'][3] - 0.7500, precision) == 0

    # node E
    res = instance.solve(4, "dual", "input")
    assert round(res['efficiency'] - 0.8000, precision) == 0
    assert round(res_bat['efficiency_arr'][4] - 0.8000, precision) == 0

    # node F
    res = instance.solve(5, "dual", "input")
    assert round(res['efficiency'] - 0.4000, precision) == 0
    assert round(res_bat['efficiency_arr'][5] - 0.4000, precision) == 0

    # node G
    res = instance.solve(6, "dual", "input")
    assert round(res['efficiency'] - 0.5000, precision) == 0
    assert round(res_bat['efficiency_arr'][6] - 0.5000, precision) == 0

    # node H
    res = instance.solve(7, "dual", "input")
    assert round(res['efficiency'] - 0.6250, precision) == 0
    assert round(res_bat['efficiency_arr'][7] - 0.6250, precision) == 0


def test_ccr_primal_dual_input(precision=4):
    """ [Coopr07],example2_2_and_3_1 """
    X = np.array([[4, 3], [7, 3], [8, 1], [4, 2], [2, 4], [10, 1]])
    Y = np.array([[1, ], [1, ], [1, ], [1, ], [1, ], [1, ]])

    in_weights = np.array([[.1429, .1429], [.0526, .2105], [0, 1],
                        [.1667, .1667], [.5, 0], [0, 1],])
    out_weights = np.array([[.8571, ], [.6316, ], [1, ], [1, ], [1, ], [1, ]])
    ref_sets = np.array([[3, 4], [2, 3], [2, ], [3, ], [4, ], [2, ]])
    in_slacks = np.array([[0,0], [0, 0], [0, 0], [0, 0], [0, 0], [2, 0],])
    out_slacks = np.zeros(6)[:, np.newaxis]

    instance = ccr.CCR(X, Y)
    # batch mode
    res_d_bat = instance.solve_batch("dual", "input")
    res_p_bat = instance.solve_batch("primal", "input")

    for rdx in xrange(X.shape[0]):
        # single mode
        res = instance.solve(rdx, "dual", "input")
        assert round(res['in_weight'][0] - in_weights[rdx, 0], precision) == 0
        assert round(res['in_weight'][1] - in_weights[rdx, 1], precision) == 0
        assert round(res['out_weight'][0] - out_weights[rdx, 0], precision) == 0

        # batch mode
        assert round(res_d_bat['in_weight_arr'][rdx, 0] - in_weights[rdx, 0],
                     precision) == 0
        assert round(res_d_bat['in_weight_arr'][rdx, 1] - in_weights[rdx, 1],
                     precision) == 0
        assert round(res_d_bat['out_weight_arr'][rdx, 0] - out_weights[rdx, 0],
                     precision) == 0

        # single mode
        res_d = instance.solve(rdx, "primal", "input")
        assert ref_sets[rdx] in res_d['reference_dmu']
        assert len(ref_sets[rdx]) == res_d['reference_dmu'].size
        assert round(res_d['in_slack'][0] - in_slacks[rdx, 0], precision) == 0
        assert round(res_d['in_slack'][1] - in_slacks[rdx, 1], precision) == 0
        assert round(res_d['out_slack'][0] - out_slacks[rdx, 0], precision) == 0

        # batch mode
        assert round(res_p_bat['in_slack_arr'][rdx, 0] - in_slacks[rdx, 0],
                     precision) == 0
        assert round(res_p_bat['in_slack_arr'][rdx, 1] - in_slacks[rdx, 1],
                     precision) == 0
        assert round(res_p_bat['out_slack_arr'][rdx, 0] - out_slacks[rdx, 0],
                     precision) == 0
        assert ref_sets[rdx] in res_p_bat['reference_dmu_arr'][rdx]




def test_ccr_dual_input_2(precision=3):
    """ [Coopr07] table_4_4 """
    X = np.array([[1,1,1],[1,2,1],[1,1,2],[1,1,1],
                  [1,1,1],[2,2,2]])
    Y = np.array([[2,2,2],[2,2,2],[1,2,2],[1,2,1],
                  [2,1,0.5],[.5, .5, 1]])

    eff = np.array([[1,],[1,],[1,],[1,],[1,],[.25,]])

    # single mode
    instance = ccr.CCR(X,Y)

    # batch mode
    res_bat = instance.solve_batch("dual", "input")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "dual", "input")
        assert round(eff[rdx, 0] - res['efficiency'], precision) == 0
        assert round(eff[rdx, 0] - res_bat['efficiency_arr'][rdx],
                     precision) == 0


def test_ccr_dual_input_3(precision=2):
    """ [Coopr07] table_5_2 """
    X = np.array([[112, 1894, 2024205], [111, 1727, 1702017],
                  [123, 1843, 2048014], [56, 792, 600796],
                  [86, 1422, 1173778], [111, 1841, 2290274],
                  [105, 1633, 1361498], [64, 889, 668236],
                  [90, 1611, 1563930], [69, 1036, 654338],
                  [73, 1011, 656794], [407, 17837, 53438938],
                  [529, 18805, 52535262], [363, 15188, 51368976],
                  [419, 20235, 78170694], [436, 13149, 29531193],
                  [395, 13998, 52999340], [397, 15710, 56468571],
                  [218, 8671, 16665573], [305, 11546, 31376180],])

    Y = np.array([[13553,], [16540, ], [14760, ], [3782, ], [8700, ],
                  [18590, ], [9699, ], [5605, ], [9993, ], [3525, ],
                  [3177, ], [411149, ], [302901, ], [371364, ], [504422, ],
                  [170835, ], [399398, ], [353531, ], [112121, ], [186640, ],])

    eff = np.array([[.714,], [1, ], [.768, ], [.648, ], [.763, ], [.89, ],
                    [.733, ], [.863, ], [.676, ], [.554, ], [.500, ], [1, ],
                    [.742, ], [.981, ], [1, ], [.728, ], [1, ], [.854, ],
                    [.853, ], [.766, ],])

    # single mode
    instance = ccr.CCR(X,Y)

    # batch mode
    res_bat = instance.solve_batch("dual", "input")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "dual", "input")
        assert round(eff[rdx, 0] - res['efficiency'], precision) == 0
        assert round(eff[rdx, 0] - res_bat['efficiency_arr'][rdx],
                     precision) == 0

def test_ccr_dual_input_4(precision=3):
    """ [Coopr07] tables 1-5, 5-1 """
    X = np.array([[20, 151], [19, 131], [25, 160], [27, 168], [22, 158],
                  [55, 255], [33, 235], [31, 206], [30, 244], [50, 268],
                  [53, 306], [38, 284]])

    Y = np.array([[100, 90], [150, 50], [160, 55], [180, 72], [94, 66],
                  [230, 90], [220, 88], [152, 80], [190, 100], [250, 100],
                  [260, 147], [250, 120]])

    eff = np.array([[1,], [1, ], [.883, ], [1, ], [.763, ], [.835, ],
                    [.902, ], [.796, ], [.96, ], [.871, ], [.955, ],
                    [.958, ],])
    # single mode
    instance = ccr.CCR(X,Y)

    # batch mode
    res_bat = instance.solve_batch("dual", "input")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "dual", "input")
        assert round(eff[rdx, 0] - res['efficiency'], precision) == 0
        assert round(eff[rdx, 0] - res_bat['efficiency_arr'][rdx],
                     precision) == 0


def test_ccr_primal_dual_input_5(precision=4):
    """[coopr07], Table 3.9, 3.10 """
    X = np.array([[10., 20.], [15, 15],[20, 30], [25, 15], [12, 9]])
    Y = np.array([[70., 6.], [100, 3], [80, 5], [100, 2], [90, 8]])

    effs = np.array([0.933333, 0.888889, 0.533333, 0.666667, 1])
    refs = np.array([[4,],[4,],[4,],[4,],[4,]])
    in_slacks = np.array([[0, 11.6667], [0., 3.3333], [0, 8],
                          [3.3333, 0], [0, 0]])
    out_slacks = np.array([[0, 0.2222], [0., 5.8889], [0, 2.1111],
                           [0, 6.8889], [0, 0]])

    # single mode
    instance = ccr.CCR(X,Y)

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "dual", "input")
        assert round(effs[rdx] - res['efficiency'], precision) == 0

        res_d = instance.solve(rdx, "primal", "input")
        assert refs[rdx] == res_d['reference_dmu']
        np.testing.assert_array_almost_equal(in_slacks[rdx],
                                              res_d['in_slack'], precision)
        np.testing.assert_array_almost_equal(out_slacks[rdx],
                                              res_d['out_slack'],precision)


def test_input_output_projection(precision=2):
    """  [Coopr07] table 3.11 """
    X = np.array([[4., 3.], [7, 3],[8, 1], [4, 2], [2, 4], [10,1], [3,7]])
    Y = np.array([[1,],[1,],[1,],[1,],[1,],[1,],[1,]])

    effs = np.array([0.8571, 0.6316, 1, 1, 1,1, 0.6667])

    # input projection
    in_proj_X = np.array([[3.43, 2.57], [4.42, 1.89], [8, 1], [4, 2],
                          [2, 4], [8,1], [2, 4]])
    in_proj_Y = np.array([[1,],[1,],[1,],[1,],[1,],[1,],[1,]])

    # output projection
    out_proj_X = np.array([[4, 3], [7, 3], [8, 1], [4, 2],
                          [2, 4], [8,1], [3, 6]])
    out_proj_Y = np.array([[1.17,],[1.58,],[1,],[1,],[1,],[1,],[1.5,]])


    # single mode
    instance = ccr.CCR(X,Y)

    for rdx in xrange(X.shape[0]):
        res_d = instance.solve(rdx, "dual", "input")
        assert round(effs[rdx] - res_d['efficiency'], precision) == 0

        res_p= instance.solve(rdx, "primal", "input")
        np.testing.assert_array_almost_equal(in_proj_X[rdx],
                                        res_p['projection_x'], precision)
        np.testing.assert_array_almost_equal(in_proj_Y[rdx],
                                        res_p['projection_y'], precision)

        res_po= instance.solve(rdx, "primal", "output")
        np.testing.assert_array_almost_equal(out_proj_X[rdx],
                                        res_po['projection_x'], precision)
        np.testing.assert_array_almost_equal(out_proj_Y[rdx],
                                        res_po['projection_y'], precision)



def test_ccr_dual_input_batch_equality(precision=4):
    """  test equality between single and batch mode """
    n_dmu, dim_input, dim_output = 10, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = ccr.CCR(X, Y)
    res_bat = instance.solve_batch("dual", "input")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "dual", "input")
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                                                precision) == 0
        np.testing.assert_array_almost_equal(res_bat['in_weight_arr'][rdx],
                                             res['in_weight'])
        np.testing.assert_array_almost_equal(res_bat['out_weight_arr'][rdx],
                                             res['out_weight'])


def test_ccr_dual_output_batch_equality(precision=4):
    """  test equality between single and batch mode """
    n_dmu, dim_input, dim_output = 10, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = ccr.CCR(X, Y)
    res_bat = instance.solve_batch("dual", "output")
    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "dual", "output")
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                                                precision) == 0
        np.testing.assert_array_almost_equal(res_bat['in_weight_arr'][rdx],
                                             res['in_weight'])
        np.testing.assert_array_almost_equal(res_bat['out_weight_arr'][rdx],
                                             res['out_weight'])


def test_ccr_primal_input_batch_equality(precision=4):
    """  test equality between single and batch mode """
    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = ccr.CCR(X, Y)
    res_bat = instance.solve_batch("primal", "input")
    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "input")
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                                                precision) == 0
        np.testing.assert_array_almost_equal(res_bat['intensity_arr'][rdx],
                                             res['intensity'])
        np.testing.assert_array_almost_equal(res_bat['in_slack_arr'][rdx],
                                             res['in_slack'])
        np.testing.assert_array_almost_equal(res_bat['out_slack_arr'][rdx],
                                             res['out_slack'])
        np.testing.assert_array_almost_equal(res_bat['is_efficient_arr'][rdx],
                                             res['is_efficient'])
        np.testing.assert_array_almost_equal(res_bat['projection_x_arr'][rdx],
                                             res['projection_x'])
        np.testing.assert_array_almost_equal(res_bat['projection_y_arr'][rdx],
                                             res['projection_y'])


def test_ccr_primal_output_batch_equality(precision=4):
    """  test equality between single and batch mode """
    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = ccr.CCR(X, Y)
    res_bat = instance.solve_batch("primal", "output")
    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "output")
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                                                precision) == 0
        np.testing.assert_array_almost_equal(res_bat['intensity_arr'][rdx],
                                             res['intensity'])
        np.testing.assert_array_almost_equal(res_bat['in_slack_arr'][rdx],
                                             res['in_slack'])
        np.testing.assert_array_almost_equal(res_bat['out_slack_arr'][rdx],
                                             res['out_slack'])
        np.testing.assert_array_almost_equal(res_bat['is_efficient_arr'][rdx],
                                             res['is_efficient'])
        np.testing.assert_array_almost_equal(res_bat['projection_x_arr'][rdx],
                                             res['projection_x'])
        np.testing.assert_array_almost_equal(res_bat['projection_y_arr'][rdx],
                                             res['projection_y'])


def test_ccr_input_output_properties(precision=4):
    """
    the optimal efficiency theta in input model is equal to 1/eta where eta
    is the optimal efficiency of the output model
    """
    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = ccr.CCR(X, Y)
    res_d_bat = instance.solve_batch("dual", "input")
    res_d_bat_o = instance.solve_batch("dual", "output")
    res_p_bat = instance.solve_batch("primal", "input")
    resd_p_bat_o = instance.solve_batch("primal", "output")

    for rdx in xrange(X.shape[0]):
        # efficiency
        eff_i = res_p_bat['efficiency_arr'][rdx]
        eff_o =  resd_p_bat_o['efficiency_arr'][rdx]
        assert round(eff_i- eff_o, precision) == 0

        # intensity
        intensity = res_p_bat['intensity_arr'][rdx]
        intensity_o = resd_p_bat_o['intensity_arr'][rdx]
        np.testing.assert_array_almost_equal(intensity/eff_i,
                                             intensity_o, precision)

        # in_weight
        in_weight = res_d_bat['in_weight_arr'][rdx]
        out_weight = res_d_bat_o['in_weight_arr'][rdx]
        assert round(np.dot(in_weight/eff_i, X[rdx]) -
                     np.dot(out_weight, X[rdx]), precision) == 0

        # out_weight
        out_weight = res_d_bat['out_weight_arr'][rdx]
        assert round(np.dot(out_weight/eff_i, Y[rdx]) - 1, precision) == 0

def test_inter_temporal_ccr_equality(precision=6):

    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = ccr.CCR(X, Y)
    instance2 = ccr.Temporal_CCR(X, Y, X, Y)

    # input oriented
    res_bat = instance.solve_batch("primal", "input")
    res_bat2 = instance2.solve_batch("input")

    np.testing.assert_array_almost_equal(
        res_bat['efficiency_arr'], res_bat2['efficiency_arr'], precision)

    # output oriented
    res_bat_o = instance.solve_batch("primal", "output")
    res_bat2_o = instance2.solve_batch("output")

    np.testing.assert_array_almost_equal(
        res_bat_o['efficiency_arr'], res_bat2_o['efficiency_arr'], precision)
