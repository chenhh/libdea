# -*- coding: utf-8 -*-
"""
[Coopr07] Cooper, W. W., Seiford, L. M., & Tone, K. (2007). Data envelopment
analysis: a comprehensive text with models, applications, references and
DEA-solver software. Springer Science & Business Media.

"""
from __future__ import division
import numpy as np
from libdea import mpss


def test_mpss_figure5_3(precision=4):
    """ [Coopr07], MPSS """
    X = np.array([[1, ], [1.5, ], [3, ], [4, ], [4, ]])
    Y = np.array([[1, ], [2, ], [4, ], [5, ], [4.5, ]])
    instance = mpss.MPSS(X, Y)

    # node B, MPSS
    res = instance.solve(1)
    assert round(res['ratio'] - 1, precision) == 0
    assert  res['is_MPSS'] == True
    print res['is_MPSS'], res['beta']/res['alpha'], (res['in_slack']+res[
        'out_slack']).sum()

    # node C, MPSS
    res = instance.solve(2)
    assert round(res['ratio'] - 1, precision) == 0
    assert  res['is_MPSS'] == True
    print res['is_MPSS'], res['beta']/res['alpha'], (res['in_slack']+res[
        'out_slack']).sum()

    # node D, not MPSS
    res = instance.solve(3)
    assert round(res['ratio'] - 16./15, precision) == 0
    assert  res['is_MPSS'] == False
    print res['is_MPSS'], res['beta']/res['alpha'], (res['in_slack']+res[
        'out_slack']).sum()