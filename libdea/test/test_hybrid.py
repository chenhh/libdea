# -*- coding: utf-8 -*-
"""
[Coopr07] Cooper, W. W., Seiford, L. M., & Tone, K. (2007). Data envelopment
analysis: a comprehensive text with models, applications, references and
DEA-solver software. Springer Science & Business Media.

"""

from __future__ import division
import numpy as np
from libdea import (sbm, ccr, bcc, mpss)

def test_ccr_sbm(precision=2):
    """ [Coopr07] Table 4.4 """
    X = np.array([[1,1,1], [1,2,1], [1,1,2], [1,1,1], [1,1,1], [2,2,2]])
    Y = np.array([[2,2,2], [2,2,2], [1,2,2], [1,2,1], [2,1,.5], [.5,.5,1]])
    ccr_eff = np.array([ 1. , 1., 1.,1.,1.,0.25])
    sbm_eff = np.array([1, 0.833,  0.625, 0.6 , 0.42857143,  0.15] )

    inst1 = ccr.CCR(X, Y)
    res1 = inst1.solve_batch("dual", "input")
    np.testing.assert_array_almost_equal(res1['efficiency_arr'],
                                             ccr_eff, precision)
    inst2 = sbm.SBM(X, Y)
    res2 = inst2.solve_batch("primal")
    np.testing.assert_array_almost_equal(res2['efficiency_arr'],
                                             sbm_eff, precision)


def test_decompoision_efficiency(precision=3):
    """ [Coopr07] _table_5_1 """
    X = np.array([[20, 151], [19, 131], [25, 160], [27, 168], [22, 158],
                  [55, 255], [33, 235], [31, 206], [30, 244], [50, 268],
                  [53, 306], [38, 284]])

    Y = np.array([[100, 90], [150, 50], [160, 55], [180, 72], [94, 66],
                  [230, 90], [220, 88], [152, 80], [190, 100], [250, 100],
                  [260, 147], [250, 120]])

    # input-oriented models
    ccr_effs = np.array([1, 1, .883, 1,.763, .835, .902, .796,  .96,
                              .871, .955, .958])
    bcc_effs = np.array([1,1, .896, 1, .882, .939, 1, .799, .989, 1,1,1 ])
    sbm_in_effs = np.array([1,1, .852 ,1, .756, .704, .895, .774, .905, .781,
                    .866, .936])

    inst_ccr = ccr.CCR(X,Y)
    inst_bcc= bcc.BCC(X,Y)
    inst_sbm = sbm.SBM(X,Y)

    ccr_res = inst_ccr.solve_batch("dual", "input")
    np.testing.assert_array_almost_equal(ccr_res['efficiency_arr'],
                                         ccr_effs, precision)

    bcc_res = inst_bcc.solve_batch("primal", "input")
    np.testing.assert_array_almost_equal(bcc_res['efficiency_arr'],
                                         bcc_effs, precision)

    sbm_in_res = inst_sbm.solve_batch("primal", "input")
    np.testing.assert_array_almost_equal(sbm_in_res['efficiency_arr'],
                                         sbm_in_effs, precision)

