# -*- coding: utf-8 -*-
"""
[Coopr07] Cooper, W. W., Seiford, L. M., & Tone, K. (2007). Data envelopment
analysis: a comprehensive text with models, applications, references and
DEA-solver software. Springer Science & Business Media.

TODO: lack of additive dual test
"""

from __future__ import division
import numpy as np
from libdea import additive


def test_additive_primal_input(precision=2):
    """[Coopr07], table_4_2
    (5,2) may ref to (3,3)=>obj=3, slack=(2,1) [cplex] or
                     (2,2)=>obj=3, slack=(3,0) [gurobi]
    """

    X = np.array([[2,],[3,],[2,],[4,],[6,],[5,],[6,],[8,]])
    Y = np.array([[1,],[3,],[2,],[3,],[5,],[2,],[3,],[5,]])
    ref_sets = np.array([[2,],[1,],[2,],[1,],[4,],[2,],[1,],[4,]])
    in_slacks = np.array([[0,],[0,],[0,],[1,],[0,],[3,],[3,],[2,]])
    out_slacks = np.array([[1,],[0,],[0,],[0,],[0,],[0,],[0,],[0,]])

    # single
    instance = additive.Additive(X,Y)

    # batch
    res_bat = instance.solve_batch('primal')

    if instance.solver == "cplex":
        ref_sets[5, 0] = 1
        in_slacks[5, 0] = 2
        out_slacks[5, 0] = 1


    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal")
        assert round(in_slacks[rdx, 0]- res['in_slack'][0], precision) == 0
        assert round(out_slacks[rdx, 0]- res['out_slack'][0], precision) == 0
        assert ref_sets[rdx, 0] in res['reference_dmu']

        assert round(in_slacks[rdx, 0]- res_bat['in_slack_arr'][rdx, 0],
                     precision) == 0
        assert round(out_slacks[rdx, 0]- res_bat['out_slack_arr'][rdx, 0],
                     precision) == 0
        assert ref_sets[rdx, 0] in res_bat['reference_dmu_arr'][rdx]


def test_additive_primal_batch_equality(precision=4):
    n_dmu, dim_input, dim_output = 10, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    # single
    instance = additive.Additive(X,Y)

    # batch
    res_bat = instance.solve_batch('primal')

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal")

        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                                                precision) == 0
        np.testing.assert_array_almost_equal(res_bat['intensity_arr'][rdx],
                                             res['intensity'])
        np.testing.assert_array_almost_equal(res_bat['in_slack_arr'][rdx],
                                             res['in_slack'])
        np.testing.assert_array_almost_equal(res_bat['out_slack_arr'][rdx],
                                             res['out_slack'])


def test_additive_dual_batch_equality(precision=4):
    """  test equality between single and batch mode """
    n_dmu, dim_input, dim_output = 10, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    # single
    instance = additive.Additive(X,Y)

    # batch
    res_bat = instance.solve_batch('dual')
    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "dual")
        assert round(res_bat['efficiency_arr'][rdx] - res['efficiency'],
                                                precision) == 0
        assert round(res_bat['boundary_slack_arr'][rdx] - res['boundary_slack'],
                                                precision) == 0
        np.testing.assert_array_almost_equal(res_bat['in_weight_arr'][rdx],
                                             res['in_weight'])
        np.testing.assert_array_almost_equal(res_bat['out_weight_arr'][rdx],
                                             res['out_weight'])
