# -*- coding: utf-8 -*-
"""
[Coopr07] Cooper, W. W., Seiford, L. M., & Tone, K. (2007). Data envelopment
analysis: a comprehensive text with models, applications, references and
DEA-solver software. Springer Science & Business Media.
"""

from __future__ import division
import numpy as np
from libdea import super_efficiency, sbm

def test_super_efficiency_ccr_1(precision=3):
    """ [Coopr07] Table 10.1 """
    X = np.array([[2,12],[2,8],[5,5],[10,4],[10,6],[3.5,6.5]])
    Y = np.array([[1,],[1,],[1,],[1,],[1,],[1,]])

    eff = np.array([[1,],[1.261,],[1.133,],[1.25,],[0.75,],[1.,]])

    # single
    instance = super_efficiency.SuperEfficiencyCCR(X, Y)
    # batch
    res_bat = instance.solve_batch("primal", "input")

    for idx in xrange(X.shape[0]):
        res = instance.solve(idx, "primal", "input")
        assert round(res['efficiency'] - eff[idx,0], precision) == 0
        assert round(res['efficiency'] - res_bat['efficiency_arr'][idx],
                     precision) == 0


def test_super_efficiency_ccr_batch_input_equality(precision=4):

    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    # single
    instance = super_efficiency.SuperEfficiencyCCR(X, Y)

    # batch
    res_bat = instance.solve_batch("primal", "input")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "input")
        # efficiency
        assert round(res['efficiency'] - res_bat['efficiency_arr'][rdx],
                     precision) == 0

        # intensity
        np.testing.assert_array_almost_equal(res_bat['intensity_arr'][rdx],
                                             res['intensity'])

        # slack
        np.testing.assert_array_almost_equal(res_bat['in_slack_arr'][rdx],
                                             res['in_slack'])
        np.testing.assert_array_almost_equal(res_bat['out_slack_arr'][rdx],
                                             res['out_slack'])

        # projection
        np.testing.assert_array_almost_equal(res_bat['projection_x_arr'][rdx],
                                             res['projection_x'])
        np.testing.assert_array_almost_equal(res_bat['projection_y_arr'][rdx],
                                             res['projection_y'])


def test_super_efficiency_ccr_batch_output_equality(precision=4):

    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    # single
    instance = super_efficiency.SuperEfficiencyCCR(X, Y)

    # batch
    res_bat = instance.solve_batch("primal", "output")
    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "output")
        # efficiency
        assert round(res['efficiency'] - res_bat['efficiency_arr'][rdx],
                     precision) == 0

        # intensity
        np.testing.assert_array_almost_equal(res_bat['intensity_arr'][rdx],
                                             res['intensity'])

        # slack
        np.testing.assert_array_almost_equal(res_bat['in_slack_arr'][rdx],
                                             res['in_slack'])
        np.testing.assert_array_almost_equal(res_bat['out_slack_arr'][rdx],
                                             res['out_slack'])

        # projection
        np.testing.assert_array_almost_equal(res_bat['projection_x_arr'][rdx],
                                             res['projection_x'])
        np.testing.assert_array_almost_equal(res_bat['projection_y_arr'][rdx],
                                             res['projection_y'])


def test_super_efficiency_bcc_batch_input_equality(precision=4):

    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    # single
    instance = super_efficiency.SuperEfficiencyBCC(X, Y)

    # batch
    res_bat = instance.solve_batch("primal", "input")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "input")
        if res is None:
            assert res_bat['feasible_arr'][rdx] is False
        else:
            # efficiency
            assert round(res['efficiency'] - res_bat['efficiency_arr'][rdx],
                         precision) == 0

            # intensity
            np.testing.assert_array_almost_equal(res_bat['intensity_arr'][rdx],
                                                 res['intensity'])

            # slack
            np.testing.assert_array_almost_equal(res_bat['in_slack_arr'][rdx],
                                                 res['in_slack'])
            np.testing.assert_array_almost_equal(res_bat['out_slack_arr'][rdx],
                                                 res['out_slack'])

            # projection
            np.testing.assert_array_almost_equal(
                res_bat['projection_x_arr'][rdx], res['projection_x'])
            np.testing.assert_array_almost_equal(
                res_bat['projection_y_arr'][rdx], res['projection_y'])



def test_super_efficiency_bcc_batch_output_equality(precision=4):

    n_dmu, dim_input, dim_output = 20, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    # single
    instance = super_efficiency.SuperEfficiencyBCC(X, Y)

    # batch
    res_bat = instance.solve_batch("primal", "output")

    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "output")
        if res is None:
            assert res_bat['feasible_arr'][rdx] is False
        else:
            # efficiency
            # print rdx, res['efficiency'], res_bat['efficiency_arr'][rdx]
            assert round(res['efficiency'] - res_bat['efficiency_arr'][rdx],
                         precision) == 0

            # intensity
            np.testing.assert_array_almost_equal(res_bat['intensity_arr'][rdx],
                                                 res['intensity'])

            # slack
            np.testing.assert_array_almost_equal(res_bat['in_slack_arr'][rdx],
                                                 res['in_slack'])
            np.testing.assert_array_almost_equal(res_bat['out_slack_arr'][rdx],
                                                 res['out_slack'])

            # projection
            np.testing.assert_array_almost_equal(
                res_bat['projection_x_arr'][rdx], res['projection_x'])
            np.testing.assert_array_almost_equal(
                res_bat['projection_y_arr'][rdx], res['projection_y'])

def test_super_efficiency_sbm_1(precision=3):
    """ [Coopr07] Table 10.3 """
    X = np.array([[2,12],[2,8],[5,5],[10,4],[10,6],[3.5,6.5]])
    Y = np.array([[1,],[1,],[1,],[1,],[1,],[1,]])

    eff = np.array([[.833,],[1.25,],[1.092,],[1.125,],[0.667,],[1.,]])

    instance = sbm.SBM(X, Y)
    instance2 = super_efficiency.SuperEfficiencySBM(X, Y)

    for idx in xrange(X.shape[0]):
        res = instance.solve(idx, "primal", "input")
        equity = round(res['efficiency'] -1, precision) == 0
        if not equity:
            assert round(eff[idx] - res['efficiency'], precision) == 0
        else:
            res2 = instance2.solve(idx, "primal", "input")
            assert round(res2['efficiency'] - eff[idx], precision) == 0


def test_super_efficiency_sbm_2(precision=2):
    """ [Coopr07] Table 10.4, 10.6 """
    X = np.array([[2,12],[2,8],[5,5],[10,4],[10,6],[3.5,6.5]])
    Y = np.array([[4,1],[3,1],[2,1],[2,1],[1,1],[1,1]])

    # CRS
    super_ccr_in_effs = np.array([1.33, 1.26, 1.17, 1.25, .75, 1])
    super_ccr_out_effs = np.array([1.33, 1.26, 1.17, 1.25, .75, 1])
    super_sbm_in_effs = np.array([1.17, 1.25, 1.12, 1.13, .67, 1])
    super_sbm_out_effs = np.array([1.14, 1.2, 1.09, 1.25, .5, .57])
    super_sbm_effs = np.array([1.14, 1.16, 1.09, 1.13, .42, .57])

    # VRS
    super_sbm_vrs_effs = np.array([1.14, 1.25, 1.12, 1.13, .42, .57])
    super_bcc_in_effs = [None, 1.26, 1.17, 1.25, 0.75, 1]
    super_bcc_out_effs = [1.33, None, None, None, 1, 1]
    super_sbm_vrs_in_effs = [None, 1.25, 1.12, 1.13, 0.66, 1]
    super_sbm_vrs_out_effs = [1.14, None ,None, None, 0.57, 0.57]

    instance = sbm.SBM(X, Y)
    inst_super_sbm = super_efficiency.SuperEfficiencySBM(X, Y)
    inst_super_ccr = super_efficiency.SuperEfficiencyCCR(X,Y)
    inst_super_bcc = super_efficiency.SuperEfficiencyBCC(X, Y)
    for idx in xrange(X.shape[0]):
        # Super CCR input
        res_ccr_in = inst_super_ccr.solve(idx, "primal", "input")
        assert round(res_ccr_in['efficiency'] - super_ccr_in_effs[idx],
                     precision) == 0

        # Super CCR output
        res_ccr_out = inst_super_ccr.solve(idx, "primal", "output")
        assert round(1./res_ccr_out['efficiency'] - super_ccr_out_effs[idx],
                     precision) == 0

        # super and orig SBM input
        super_sbm_in = inst_super_sbm.solve(idx, "primal", "input")
        orig_sbm_in = instance.solve(idx, "primal", "input")
        if  round(super_sbm_in['efficiency'], precision) > 1:
            assert round( super_sbm_in['efficiency'] -super_sbm_in_effs[idx],
                     precision) == 0
        else:
            assert round(orig_sbm_in['efficiency'] - super_sbm_in_effs[idx],
                     precision) == 0

        # super and orig SBM output
        super_sbm_out = inst_super_sbm.solve(idx, "primal", "output")
        orig_sbm_out = instance.solve(idx, "primal", "output")
        if  round(super_sbm_out['efficiency'], precision) > 1:
            assert round( super_sbm_out['efficiency'] -super_sbm_out_effs[idx],
                     precision) == 0
        else:
            assert round(1./orig_sbm_out['efficiency'] -
                         super_sbm_out_effs[idx], precision) == 0

        # Super and orig SBM
        super_sbm = inst_super_sbm.solve(idx, "primal", None)
        orig_sbm = instance.solve(idx, "primal", None)

        if  round(super_sbm['efficiency'], precision) > 1:
            assert round( super_sbm['efficiency'] -super_sbm_effs[idx],
                     precision) == 0
        else:
            assert round(orig_sbm['efficiency'] - super_sbm_effs[idx],
                     precision) == 0

        # super SBM VRS
        super_sbm_vrs = inst_super_sbm.solve_vrs(idx, "primal", None)
        if  round(super_sbm_vrs['efficiency'], precision) > 1:
            assert round( super_sbm_vrs['efficiency'] -super_sbm_vrs_effs[idx],
                     precision) == 0
        else:
            assert round(orig_sbm['efficiency'] - super_sbm_vrs_effs[idx],
                     precision) == 0

        # super SBM VRS input
        super_sbm_vrs_in = inst_super_sbm.solve_vrs(idx, "primal", "input")
        if super_sbm_vrs_in is None:
            assert super_sbm_vrs_in == super_sbm_vrs_in_effs[idx]
        elif round(super_sbm_vrs_in['efficiency'], precision) > 1:
            assert round( super_sbm_vrs_in['efficiency'] -
                          super_sbm_vrs_in_effs[idx], precision) == 0

        # super SBM VRS output
        super_sbm_vrs_out = inst_super_sbm.solve_vrs(idx, "primal", "output")
        # print idx, super_sbm_vrs_out
        if super_sbm_vrs_out is None:
            assert super_sbm_vrs_out == super_sbm_vrs_out_effs[idx]
        elif round(super_sbm_vrs_out['efficiency'], precision) > 1:
            assert round( super_sbm_vrs_out['efficiency'] -
                          super_sbm_vrs_out_effs[idx], precision) == 0


        # super BCC input, output
        super_bcc_in = inst_super_bcc.solve(idx, "primal", "input")
        if super_bcc_in is None:
            assert super_bcc_in == super_bcc_in_effs[idx]
        else:
            round(super_bcc_in['efficiency'] - super_bcc_in_effs[idx],
                  precision) == 0

        super_bcc_out = inst_super_bcc.solve(idx, "primal", "output")
        if super_bcc_out is None:
            assert super_bcc_out == super_bcc_out_effs[idx]
        else:
            round(super_bcc_out['efficiency'] - super_bcc_out_effs[idx],
                  precision) == 0


def test_super_efficiency_sbm_crs_batch_primal_equality():
    """  test equality between single and batch mode """
    n_dmu, dim_input, dim_output = 10, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = super_efficiency.SuperEfficiencySBM(X, Y)
    res_bat = instance.solve_batch("primal")
    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal")
        np.testing.assert_almost_equal(
            res_bat['efficiency_arr'][rdx], res['efficiency'])
        np.testing.assert_almost_equal(
            res_bat["auxiliary_arr"][rdx], res['auxiliary'])
        np.testing.assert_array_almost_equal(
            res_bat['intensity_arr'][rdx], res['intensity'])
        np.testing.assert_array_almost_equal(
            res_bat['in_ratio_arr'][rdx], res['in_ratio'])
        np.testing.assert_array_almost_equal(
            res_bat['out_ratio_arr'][rdx], res['out_ratio'])
        np.testing.assert_array_almost_equal(
            res_bat['projection_x_arr'][rdx], res['projection_x'])
        np.testing.assert_array_almost_equal(
            res_bat['projection_y_arr'][rdx],res['projection_y'])


def test_super_efficiency_sbm_crs_batch_primal_input_equality():
    """  test equality between single and batch mode """
    n_dmu, dim_input, dim_output = 10, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = super_efficiency.SuperEfficiencySBM(X, Y)
    res_bat = instance.solve_batch("primal", "input")
    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "input")
        np.testing.assert_almost_equal(
            res_bat['efficiency_arr'][rdx], res['efficiency'])
        np.testing.assert_array_almost_equal(
            res_bat['intensity_arr'][rdx], res['intensity'])
        np.testing.assert_array_almost_equal(
            res_bat['in_ratio_arr'][rdx], res['in_ratio'])
        np.testing.assert_array_almost_equal(
            res_bat['out_ratio_arr'][rdx], res['out_ratio'])
        np.testing.assert_array_almost_equal(
            res_bat['projection_x_arr'][rdx], res['projection_x'])
        np.testing.assert_array_almost_equal(
            res_bat['projection_y_arr'][rdx],res['projection_y'])


def test_super_efficiency_sbm_crs_batch_primal_output_equality():
    """  test equality between single and batch mode """
    n_dmu, dim_input, dim_output = 10, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = super_efficiency.SuperEfficiencySBM(X, Y)
    res_bat = instance.solve_batch("primal", "output")
    for rdx in xrange(X.shape[0]):
        res = instance.solve(rdx, "primal", "output")
        np.testing.assert_almost_equal(
            res_bat['efficiency_arr'][rdx], res['efficiency'])
        np.testing.assert_array_almost_equal(
            res_bat['intensity_arr'][rdx], res['intensity'])
        np.testing.assert_array_almost_equal(
            res_bat['in_ratio_arr'][rdx], res['in_ratio'])
        np.testing.assert_array_almost_equal(
            res_bat['out_ratio_arr'][rdx], res['out_ratio'])
        np.testing.assert_array_almost_equal(
            res_bat['projection_x_arr'][rdx], res['projection_x'])
        np.testing.assert_array_almost_equal(
            res_bat['projection_y_arr'][rdx],res['projection_y'])


def test_super_efficiency_sbm_vrs_batch_primal_equality():
    """  test equality between single and batch mode """
    n_dmu, dim_input, dim_output = 10, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = super_efficiency.SuperEfficiencySBM(X, Y)
    res_bat = instance.solve_batch_vrs("primal")
    for rdx in xrange(X.shape[0]):
        res = instance.solve_vrs(rdx, "primal")
        np.testing.assert_almost_equal(
            res_bat['efficiency_arr'][rdx], res['efficiency'])
        np.testing.assert_almost_equal(
            res_bat["auxiliary_arr"][rdx], res['auxiliary'])
        np.testing.assert_array_almost_equal(
            res_bat['intensity_arr'][rdx], res['intensity'])
        np.testing.assert_array_almost_equal(
            res_bat['in_ratio_arr'][rdx], res['in_ratio'])
        np.testing.assert_array_almost_equal(
            res_bat['out_ratio_arr'][rdx], res['out_ratio'])
        np.testing.assert_array_almost_equal(
            res_bat['projection_x_arr'][rdx], res['projection_x'])
        np.testing.assert_array_almost_equal(
            res_bat['projection_y_arr'][rdx],res['projection_y'])


def test_super_efficiency_sbm_vrs_batch_primal_input_equality():
    """  test equality between single and batch mode """
    n_dmu, dim_input, dim_output = 10, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = super_efficiency.SuperEfficiencySBM(X, Y)
    res_bat = instance.solve_batch_vrs("primal", "input")
    for rdx in xrange(X.shape[0]):
        res = instance.solve_vrs(rdx, "primal", "input")
        if res is None:
            assert res_bat['feasible_arr'][rdx] is False
        else:
            np.testing.assert_almost_equal(
                res_bat['efficiency_arr'][rdx], res['efficiency'])
            np.testing.assert_array_almost_equal(
                res_bat['intensity_arr'][rdx], res['intensity'])
            np.testing.assert_array_almost_equal(
                res_bat['in_ratio_arr'][rdx], res['in_ratio'])
            np.testing.assert_array_almost_equal(
                res_bat['out_ratio_arr'][rdx], res['out_ratio'])
            np.testing.assert_array_almost_equal(
                res_bat['projection_x_arr'][rdx], res['projection_x'])
            np.testing.assert_array_almost_equal(
                res_bat['projection_y_arr'][rdx],res['projection_y'])


def test_super_efficiency_sbm_vrs_batch_primal_output_equality():
    """  test equality between single and batch mode """
    n_dmu, dim_input, dim_output = 10, 4, 3
    X = np.random.rand(n_dmu, dim_input)
    Y = np.random.rand(n_dmu, dim_output)

    instance = super_efficiency.SuperEfficiencySBM(X, Y)
    res_bat = instance.solve_batch_vrs("primal", "output")
    for rdx in xrange(X.shape[0]):
        res = instance.solve_vrs(rdx, "primal", "output")
        if res is None:
            assert res_bat['feasible_arr'][rdx] is False
        else:
            np.testing.assert_almost_equal(
                res_bat['efficiency_arr'][rdx], res['efficiency'])
            np.testing.assert_array_almost_equal(
                res_bat['intensity_arr'][rdx], res['intensity'])
            np.testing.assert_array_almost_equal(
                res_bat['in_ratio_arr'][rdx], res['in_ratio'])
            np.testing.assert_array_almost_equal(
                res_bat['out_ratio_arr'][rdx], res['out_ratio'])
            np.testing.assert_array_almost_equal(
                res_bat['projection_x_arr'][rdx], res['projection_x'])
            np.testing.assert_array_almost_equal(
                res_bat['projection_y_arr'][rdx],res['projection_y'])


