# -*- coding: utf-8 -*-
"""
Authors: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

from __future__ import division
import numpy as np
from pyomo.environ import *
from .basic_model import AbstractDEAModel
from . import (DEFAULT_SOLVER,)

class Additive(AbstractDEAModel):
    def __init__(self, X, Y, solver=DEFAULT_SOLVER):
        """
        Parameters
        -----------
        X: numpy.array, shape: (n_dmu, dim_input)
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        Y: numpy.array, shape: (n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        solver: str,  the solver supported by pyomo (default: cplex)
        """
        super(Additive, self).__init__(X, Y, solver, "semi_positive")

    def solve_primal(self, dmu_idx, oriented=None):
        """
        Parameters:
        -------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}

        Returns
        ---------
        model_name: str, model_name,
        dmu_idx: integer, index of the DMU,
        efficiency": float, efficiency value of the DMU
        intensity, numpy.array, shape:(n_dmu,),
            projection vector for input and output matrix to efficient frontier
        in_slack, numpy.array, shape:(dim_input,)
            distance of each dimension of the ith input from current position
            to the efficient frontier.
        out_slack, numpy.array, shape:(dim_output,)
            distance of each dimension of the jth output from current position
            to the efficient frontier.
        reference_dmu, list, reference DMUs on the efficient frontier.
        is_efficient, integer,
            - 1: efficient frontier
            - -1: inefficient
        projection_x, numpy.array, shape: (dim_input,)
        projection_y, numpy.array, shape: (dim_output,)
        """
        model_name = "Additive_primal"
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # constraint
        def input_constraint_rule(model, idx):
            frontier = sum(self.X[rdx, idx] * model.intensity[rdx]
                           for rdx in self.dmu_arr)
            return (self.X[dmu_idx, idx] - frontier - model.in_slack[idx] == 0)

        instance.input_constraint = Constraint(
            self.input_arr, rule=input_constraint_rule)

        # constraint
        def output_constraint_rule(model, jdx):
            frontier = sum(self.Y[rdx, jdx] * model.intensity[rdx]
                           for rdx in self.dmu_arr)
            return (self.Y[dmu_idx, jdx] - frontier +
                    model.out_slack[jdx] == 0)

        instance.output_constraint = Constraint(
            self.output_arr, rule=output_constraint_rule)

        # constraint
        def cvx_boundary_constraint_rule(model):
            return (sum(model.intensity[rdx]
                        for rdx in self.dmu_arr) == 1)

        instance.cvx_boundary_constraint = Constraint(
            rule=cvx_boundary_constraint_rule)

        # objective
        def objective_rule(model):
            in_slack_sum = sum(model.in_slack[idx]
                               for idx in self.input_arr)
            out_slack_sum = sum(model.out_slack[jdx]
                                for jdx in self.output_arr)
            slack_sum = in_slack_sum + out_slack_sum
            return slack_sum

        instance.objective = Objective(rule=objective_rule, sense=maximize)

        # solve
        instance = self._solve_model_feasible(instance, model_name)

        # extract results
        intensity = np.fromiter((
            instance.intensity[idx].value
            for idx in  self.dmu_arr), np.float)
        reference_dmu = self.dmu_arr[intensity > 0]

        in_slack = np.fromiter((
            instance.in_slack[idx].value
            for idx in self.input_arr), np.float)
        out_slack = np.fromiter(
            (instance.out_slack[jdx].value
             for jdx in self.output_arr), np.float)

        projection_x = self.X[dmu_idx] - in_slack
        projection_y = self.Y[dmu_idx] + out_slack

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "intensity": intensity,
            "in_slack": in_slack,
            "out_slack": out_slack,
            "reference_dmu": reference_dmu,
            "efficiency": instance.objective(),
            "is_efficient": self.is_efficient(in_slack, out_slack),
            "projection_x": projection_x,
            "projection_y": projection_y,
        }

    def solve_batch_primal(self, oriented=None):
        """
        Parameters:
        -------------
        oriented: None, for matching the interface
        """

        model_name = "Additive_batch_primal"
        instance = ConcreteModel(name=model_name)

        # result arrays
        efficiency_arr = np.zeros(self.n_dmu)
        intensity_arr = np.zeros((self.n_dmu, self.n_dmu))
        reference_dmu_arr = []
        in_slack_arr = np.zeros((self.n_dmu, self.dim_input))
        out_slack_arr = np.zeros((self.n_dmu, self.dim_output))
        is_efficient_arr = np.zeros(self.n_dmu)
        projection_x_arr = np.zeros((self.n_dmu, self.dim_input))
        projection_y_arr = np.zeros((self.n_dmu, self.dim_output))

        # decision variables
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # common data in constraint
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        # common constraint
        def cvx_boundary_constraint_rule(model):
            return (sum(model.intensity[rdx]
                        for rdx in self.dmu_arr) == 1)

        instance.cvx_boundary_constraint = Constraint(
            rule=cvx_boundary_constraint_rule)

        # common objective
        def objective_rule(model):
            in_slack_sum = sum(model.in_slack[idx]
                               for idx in self.input_arr)
            out_slack_sum = sum(model.out_slack[jdx]
                                for jdx in self.output_arr)
            slack_sum = in_slack_sum + out_slack_sum
            return slack_sum

        instance.objective = Objective(rule=objective_rule, sense=maximize)


        for dmu_idx in self.dmu_arr:
            # constraint
            def input_constraint_rule(model, idx):
                return (self.X[dmu_idx, idx] - frontier_X[idx] -
                        model.in_slack[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                        model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # solve
            instance = self._solve_model_feasible(instance, model_name)

            # extract results
            efficiency_arr[dmu_idx] = instance.objective()
            intensity_arr[dmu_idx] = np.fromiter((
                instance.intensity[rdx].value
                for rdx in self.dmu_arr), np.float)
            reference_dmu_arr.append(self.dmu_arr[intensity_arr[dmu_idx] > 0])

            in_slack_arr[dmu_idx] = np.fromiter((
                instance.in_slack[idx].value
                for idx in self.input_arr), np.float)
            out_slack_arr[dmu_idx] = np.fromiter((
                instance.out_slack[jdx].value
                for jdx in self.output_arr), np.float)

            is_efficient_arr[dmu_idx] = self.is_efficient(
               in_slack_arr[dmu_idx], out_slack_arr[dmu_idx])

            projection_x_arr[dmu_idx] = self.X[dmu_idx] - in_slack_arr[dmu_idx]
            projection_y_arr[dmu_idx] = self.Y[dmu_idx] + out_slack_arr[dmu_idx]

            # delete constraint and resolve the problem
            instance.del_component("input_constraint")
            instance.del_component("input_constraint_index")
            instance.del_component("output_constraint")
            instance.del_component("output_constraint_index")

        return {
            "model_name": model_name,
            "efficiency_arr": efficiency_arr,
            "intensity_arr": intensity_arr,
            "in_slack_arr": in_slack_arr,
            "out_slack_arr": out_slack_arr,
            "reference_dmu_arr": reference_dmu_arr,
            "is_efficient_arr": is_efficient_arr,
             "projection_x_arr": projection_x_arr,
            "projection_y_arr": projection_y_arr,
        }


    def solve_dual(self, dmu_idx, oriented=None):
        """
        Parameters
        --------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}

        Returns
        ---------
        model_name: str, model_name,
        dmu_idx: integer, index of the DMU,
        in_weight: numpy.array, shape:(dim_input,),
        out_weight: numpy.array, shape:(dim_output,),
        boundary_slack: float, slack value of projection exceed or surplus
        efficiency": float, efficiency value of the DMU
        """
        # concrete model
        model_name = "Additive_dual"
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.in_weight = Var(self.input_arr, within=NonNegativeReals)
        instance.out_weight = Var(self.output_arr, within=NonNegativeReals)
        instance.boundary_slack = Var(within=Reals)

        # common data
        in_sum_arr = [sum(instance.in_weight[idx] * self.X[rdx, idx]
                         for idx in self.input_arr)
                      for rdx in self.dmu_arr]
        out_sum_arr = [sum(instance.out_weight[jdx] * self.Y[rdx, jdx]
                          for jdx in self.output_arr)
                      for rdx in self.dmu_arr]

        # constraint
        def in_weight_lower_bound_constraint_rule(model, idx):
            return model.in_weight[idx] >= 1.

        instance.in_weight_lower_bound_constraint = Constraint(
            self.input_arr, rule = in_weight_lower_bound_constraint_rule)

        # constraint
        def out_weight_lower_bound_constraint_rule(model, idx):
            return model.in_weight[idx] >= 1.

        instance.out_weight_lower_bound_constraint = Constraint(
            self.output_arr, rule = out_weight_lower_bound_constraint_rule)

        # constraint
        def efficient_constraint_rule(model, rdx):
            return (out_sum_arr[rdx] - in_sum_arr[rdx] -
                    model.boundary_slack <= 0)

        instance.efficient_constraint = Constraint(
            self.dmu_arr, rule=efficient_constraint_rule)

        # objective
        def objective_rule(model):
            return (in_sum_arr[dmu_idx] - out_sum_arr[dmu_idx] +
                   model.boundary_slack)

        instance.objective = Objective(rule=objective_rule, sense=minimize)

        # solve
        instance = self._solve_model_feasible(instance, model_name)

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "in_weight": np.fromiter((
                instance.in_weight[idx].value
                for idx in self.input_arr), np.float),
            "out_weight": np.fromiter((
                instance.out_weight[idx].value
                for idx in self.output_arr), np.float),
            "boundary_slack": instance.boundary_slack.value,
            "efficiency": instance.objective(),
        }

    def solve_batch_dual(self, oriented=None):
        """
        Parameters:
        -------------
        oriented: None, for matching the interface

        """
        model_name = "Additive_batch_dual"
        instance = ConcreteModel(name=model_name)

        # result arrays
        efficiency_arr = np.zeros(self.n_dmu)
        in_weight_arr = np.zeros((self.n_dmu, self.dim_input))
        out_weight_arr = np.zeros((self.n_dmu, self.dim_output))
        boundary_slack_arr = np.zeros(self.n_dmu)

        # decision variables
        instance.in_weight = Var(self.input_arr, within=NonNegativeReals)
        instance.out_weight = Var(self.output_arr, within=NonNegativeReals)
        instance.boundary_slack = Var(within=Reals)

        # common data
        in_sum_arr = [sum(instance.in_weight[idx] * self.X[rdx, idx]
                         for idx in self.input_arr)
                      for rdx in self.dmu_arr]
        out_sum_arr = [sum(instance.out_weight[jdx] * self.Y[rdx, jdx]
                          for jdx in self.output_arr)
                      for rdx in self.dmu_arr]

        # common constraint
        def efficient_constraint_rule(model, rdx):
            return (out_sum_arr[rdx] - in_sum_arr[rdx] -
                    model.boundary_slack <= 0)

        instance.efficient_constraint = Constraint(
            self.dmu_arr, rule=efficient_constraint_rule)

        # common constraint
        def in_weight_lower_bound_constraint_rule(model, idx):
            return model.in_weight[idx] >= 1.

        instance.in_weight_lower_bound_constraint = Constraint(
            self.input_arr, rule = in_weight_lower_bound_constraint_rule)

        # common constraint
        def out_weight_lower_bound_constraint_rule(model, idx):
            return model.in_weight[idx] >= 1.

        instance.out_weight_lower_bound_constraint = Constraint(
            self.output_arr, rule = out_weight_lower_bound_constraint_rule)

        for dmu_idx in self.dmu_arr:
            # objective
            def objective_rule(model):
                return (in_sum_arr[dmu_idx] - out_sum_arr[dmu_idx] +
                       model.boundary_slack)

            instance.objective = Objective(rule=objective_rule, sense=minimize)

            # solve
            instance = self._solve_model_feasible(instance, model_name)

            # extract result
            in_weight_arr[dmu_idx] = np.fromiter((
                instance.in_weight[idx].value
                for idx in self.input_arr), np.float)

            out_weight_arr[dmu_idx] = np.fromiter((
                instance.out_weight[jdx].value
                for jdx in self.output_arr), np.float)

            boundary_slack_arr[dmu_idx] = instance.boundary_slack.value
            efficiency_arr[dmu_idx] = instance.objective()

            # delete constraint and resolve the problem
            instance.del_component("objective")

        return {
            "model_name": model_name,
            "in_weight_arr": in_weight_arr,
            "out_weight_arr": out_weight_arr,
            "boundary_slack_arr": boundary_slack_arr,
            "efficiency_arr": efficiency_arr,
        }


    @staticmethod
    def is_efficient(in_slack, out_slack, precision=4):
        """
        Parameters
        -------------
        in_slack, numpy.array, float, shape:(dim_input,)
        out_slack, numpy.array, float, shape:(dim_output,)

        Returns
        --------------
        return 1: efficient frontier
               -1: inefficient
        """
        # technical efficiency
        slack_sum = in_slack.sum() + out_slack.sum()
        if round(slack_sum, precision) == 0:
                return 1
        else:
            return -1
