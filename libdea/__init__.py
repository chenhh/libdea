# -*- coding: utf-8 -*-
"""
Authors: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

# pyomo global solver configuration

# SOLVER_IO = {"lp", "nl", "os", "python"}
# "python" requires to install python interface for corresponding solver
# the solvers cplex, gurobi, and glpk support the python IO.
SOLVER_IO = "python"

# KEEPFILES = {True, False}, keep temp files or not
# in win10, it may have problems with KEEPFILES = False
KEEPFILES = True

# DEFAULT_SOLVER = {"cplex", "gurobi", "glpk", "ipopt"}
DEFAULT_SOLVER = "cplex"


__all__ = ['SOLVER_IO', 'KEEPFILES', 'DEFAULT_SOLVER',]