# -*- coding: utf-8 -*-
"""
Authors: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

from __future__ import division
import numpy as np
from pyomo.environ import *
from .basic_model import (AbstractDEAModel, AbstractTemporalDEAModel)
from . import (DEFAULT_SOLVER,)


class CCR(AbstractDEAModel):
    """
    data envelopment analysis model, which was proposed by Charnes, Cooper and
    Rhodes in 1978.
    """

    def __init__(self, X, Y, solver=DEFAULT_SOLVER):
        """
        Parameters
        -----------
        X: numpy.array, shape: (n_dmu, dim_input)
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        Y: numpy.array, shape: (n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        solver: str,  the solver supported by pyomo (default: cplex)
        """
        super(CCR, self).__init__(X, Y, solver, "semi_positive")

    def solve_primal(self, dmu_idx, oriented="input"):
        """
        the solution in stage1 may not uniquely solutions of the in_slack and
        the out_slack. To determine whether the DMU is efficient or not, it
        requires to run the stage2 to ensure the sum of the in_slack and
        the out_slack is zero.

        Parameters:
        -------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}

        Returns
        ---------
        model_name: str, model_name,
        dmu_idx: integer, index of the DMU,
        efficiency": float, efficiency value of the DMU, 0< eff <=1
        intensity, numpy.array, shape:(n_dmu,),
            projection vector for input and output matrix to efficient frontier
        in_slack, numpy.array, shape:(dim_input,)
            distance of each dimension of the ith input from current position
            to the efficient frontier.
        out_slack, numpy.array, shape:(dim_output,)
            distance of each dimension of the jth output from current position
            to the efficient frontier.
        reference_dmu, list, reference DMUs on the efficient frontier.
        is_efficient, integer,
            - 1: efficient frontier
            - 0: technical efficiency
            - -1: inefficient
        projection_x, numpy.array, shape: (dim_input,)
        projection_y, numpy.array, shape: (dim_output,)
        """
        if oriented in ("input", "output"):
            model_name = "CCR_primal_{}_oriented".format(oriented)
        else:
            raise ValueError("unknown CCR primal oriented: {}".format(oriented))

        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.efficiency = Var(within=Reals)
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # common expression
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        # common objective
        def stage1_objective_rule(model):
            return model.efficiency

        # stage 1
        if oriented == "input":
            # constraint
            def input_constraint_rule(model, idx):
                return (model.efficiency * self.X[dmu_idx, idx] -
                        model.in_slack[idx] - frontier_X[idx] == 0.)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                        model.out_slack[jdx] == 0.)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # objective
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                            sense=minimize)

        elif oriented == "output":
            # constraint
            def input_constraint_rule(model, idx):
                return (self.X[dmu_idx, idx] - frontier_X[idx] -
                        model.in_slack[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (model.efficiency * self.Y[dmu_idx, jdx] -
                        frontier_Y[jdx] + model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            # stage 1 objective
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                            sense=maximize)

        # solve stage 1
        instance = self._solve_model_feasible(instance, model_name)

        # get efficiency
        efficiency = instance.efficiency.value

        # reset constraint and objective
        instance.efficiency.fixed = True
        instance.objective1.deactivate()

        def stage2_objective_rule(model):
            in_slack_sum = sum(model.in_slack[idx] for idx in self.input_arr)
            out_slack_sum = sum(model.out_slack[jdx] for jdx in self.output_arr)
            return in_slack_sum + out_slack_sum

        # both input and output oriented are to maximize slack sum
        instance.objective2 = Objective(rule=stage2_objective_rule,
                                        sense=maximize)
        instance.objective2.activate()

        # solve stage 2
        instance = self._solve_model_feasible(instance, model_name)

        # release variable
        instance.efficiency.fixed = False

        # extract the results
        intensity = np.fromiter((
            instance.intensity[rdx].value
            for rdx in self.dmu_arr), np.float)
        reference_dmu = self.dmu_arr[intensity > 0]

        in_slack = np.fromiter((
            instance.in_slack[idx].value
            for idx in self.input_arr), np.float)
        out_slack = np.fromiter((
            instance.out_slack[jdx].value
            for jdx in self.output_arr), np.float)

        # projection
        if oriented == "input":
            projection_x = efficiency * self.X[dmu_idx] - in_slack
            projection_y = self.Y[dmu_idx] + out_slack
        elif oriented == "output":
            projection_x = self.X[dmu_idx] - in_slack
            projection_y = efficiency * self.Y[dmu_idx] + out_slack
            efficiency = 1./efficiency

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "efficiency": efficiency,
            "intensity": intensity,
            "in_slack": in_slack,
            "out_slack": out_slack,
            "reference_dmu": reference_dmu,
            "is_efficient": self.is_efficient(efficiency, in_slack, out_slack),
            "projection_x": projection_x,
            "projection_y": projection_y,
        }

    def solve_batch_primal(self, oriented):

        if oriented in ("input", "output"):
            model_name = "CCR_batch_primal_{}_oriented".format(oriented)
        else:
            raise ValueError("unknown CCR batch primal oriented: {}".format(
                oriented))

        instance = ConcreteModel(name=model_name)

        # result arrays
        efficiency_arr = np.zeros(self.n_dmu)
        intensity_arr = np.zeros((self.n_dmu, self.n_dmu))
        reference_dmu_arr = []
        in_slack_arr = np.zeros((self.n_dmu, self.dim_input))
        out_slack_arr = np.zeros((self.n_dmu, self.dim_output))
        is_efficient_arr = np.zeros(self.n_dmu)
        projection_x_arr = np.zeros((self.n_dmu, self.dim_input))
        projection_y_arr = np.zeros((self.n_dmu, self.dim_output))

        # decision variables
        instance.efficiency = Var(within=Reals)
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # common expression
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        # stage 1 common objective
        def stage1_objective_rule(model):
            return model.efficiency

        def stage2_objective_rule(model):
            in_slack_sum = sum(model.in_slack[idx]
                               for idx in self.input_arr)
            out_slack_sum = sum(model.out_slack[jdx]
                                for jdx in self.output_arr)
            return in_slack_sum + out_slack_sum

        if oriented == "input":
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                                sense=minimize)
        elif oriented == "output":
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                                sense=maximize)

        instance.objective2 = Objective(rule=stage2_objective_rule,
                                            sense=maximize)

        for dmu_idx in self.dmu_arr:
            if oriented == "input":
                # constraint
                def input_constraint_rule(model, idx):
                    return (model.efficiency * self.X[dmu_idx, idx] -
                            frontier_X[idx] - model.in_slack[idx] == 0.)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                            model.out_slack[jdx] == 0.)

            elif oriented == "output":
                # constraint
                def input_constraint_rule(model, idx):
                    return (self.X[dmu_idx, idx] - frontier_X[idx] -
                            model.in_slack[idx] == 0)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (model.efficiency * self.Y[dmu_idx, jdx] -
                            frontier_Y[jdx] + model.out_slack[jdx] == 0)

            # build constraints
            instance.input_constraint = Constraint(
                    self.input_arr, rule=input_constraint_rule)
            instance.output_constraint = Constraint(
                    self.output_arr, rule=output_constraint_rule)

            # solve stage 1
            instance.objective1.activate()
            instance.objective2.deactivate()
            instance = self._solve_model_feasible(instance, model_name)

            # get efficiency
            efficiency = instance.efficiency.value

            # reset constraint and objective
            instance.efficiency.fixed = True
            instance.objective1.deactivate()

            # both input and output oriented are to maximize slack sum
            instance.objective2.activate()

            # solve stage 2
            instance = self._solve_model_feasible(instance, model_name)

            # release variable
            instance.efficiency.fixed = False

            # extract result
            intensity_arr[dmu_idx] = np.fromiter((
                instance.intensity[rdx].value
                for rdx in self.dmu_arr), np.float)
            reference_dmu_arr.append(self.dmu_arr[intensity_arr[dmu_idx] > 0])

            in_slack_arr[dmu_idx] = np.fromiter((
                instance.in_slack[idx].value
                for idx in self.input_arr), np.float)
            out_slack_arr[dmu_idx] = np.fromiter((
                instance.out_slack[jdx].value
                for jdx in self.output_arr), np.float)

            is_efficient_arr[dmu_idx] = self.is_efficient(
                efficiency, in_slack_arr[dmu_idx],
                out_slack_arr[dmu_idx])

            # projection
            if oriented == "input":
                projection_x_arr[dmu_idx] = (efficiency *
                                             self.X[dmu_idx] -
                                             in_slack_arr[dmu_idx])
                projection_y_arr[dmu_idx] = (self.Y[dmu_idx] +
                                             out_slack_arr[dmu_idx])
                efficiency_arr[dmu_idx] = efficiency
            elif oriented == "output":
                projection_x_arr[dmu_idx] = (self.X[dmu_idx] -
                                             in_slack_arr[dmu_idx])
                projection_y_arr[dmu_idx] = (efficiency *
                                             self.Y[dmu_idx] +
                                             out_slack_arr[dmu_idx])
                efficiency_arr[dmu_idx] = 1./efficiency

            # delete constraint and resolve the problem
            instance.del_component("input_constraint")
            instance.del_component("input_constraint_index")
            instance.del_component("output_constraint")
            instance.del_component("output_constraint_index")

        return {
            "model_name": model_name,
            "efficiency_arr": efficiency_arr,
            "intensity_arr": intensity_arr,
            "in_slack_arr": in_slack_arr,
            "out_slack_arr": out_slack_arr,
            "reference_dmu_arr": reference_dmu_arr,
            "is_efficient_arr": is_efficient_arr,
            "projection_x_arr": projection_x_arr,
            "projection_y_arr": projection_y_arr,
        }

    def solve_dual(self, dmu_idx, oriented="input"):
        """
        only solve the input_weight and output_weight vectors, and
        the objective value is the efficiency.

        input-oriented means given fixed output, comparing the ratio of the
        input among others, and the smaller input the better it is.

        Parameters
        --------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}

        Returns
        ---------
        model_name: str, model_name,
        dmu_idx: integer, index of the DMU,
        in_weight: numpy.array, shape:(dim_input,),
        out_weight: numpy.array, shape:(dim_output,),
        efficiency": float, efficiency value of the DMU
        """
        if oriented in ("input", "output"):
            model_name = "CCR_dual_{}_oriented".format(oriented)
        else:
            raise ValueError("unknown CCR dual oriented: {}".format(oriented))

        # Concrete model
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.in_weight = Var(self.input_arr, within=NonNegativeReals)
        instance.out_weight = Var(self.output_arr, within=NonNegativeReals)

        # common expression
        in_sum_arr = [sum(instance.in_weight[idx] * self.X[rdx, idx]
                          for idx in self.input_arr)
                      for rdx in self.dmu_arr]
        out_sum_arr = [sum(instance.out_weight[jdx] * self.Y[rdx, jdx]
                           for jdx in self.output_arr)
                       for rdx in self.dmu_arr]

        # common constraint
        def efficient_constraint_rule(model, rdx):
            return out_sum_arr[rdx] - in_sum_arr[rdx] <= 0.

        instance.efficient_constraint = Constraint(
            self.dmu_arr, rule=efficient_constraint_rule)

        if oriented == "input":
            # input model constraint
            def normalized_constraint_rule(model):
                return in_sum_arr[dmu_idx] == 1.

            instance.normalized_constraint = Constraint(
                rule=normalized_constraint_rule)

            # input objective
            def objective_rule(model):
                return out_sum_arr[dmu_idx]

            instance.objective = Objective(rule=objective_rule, sense=maximize)

        elif oriented == "output":
            # output model constraint
            def normalized_constraint_rule(model):
                return out_sum_arr[dmu_idx] == 1.

            instance.normalized_constraint = Constraint(
                rule=normalized_constraint_rule)

            # output objective
            def objective_rule(model):
                return in_sum_arr[dmu_idx]

            instance.objective = Objective(rule=objective_rule, sense=minimize)

        # solve
        instance = self._solve_model_feasible(instance, model_name)

        efficiency = instance.objective()

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "in_weight": np.fromiter((
                instance.in_weight[idx].value
                for idx in self.input_arr), np.float),
            "out_weight": np.fromiter((
                instance.out_weight[jdx].value
                for jdx in self.output_arr), np.float),
            "efficiency": efficiency,
        }

    def solve_batch_dual(self, oriented="input"):
        """
        Parameters:
        -------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {"input", "output"}

        Returns
        ---------
        model_name: str, model_name,
        in_weight_arr: numpy.array, shape:(n_dmu, dim_input),
        out_weight_arr: numpy.array, shape:(n_dmu, dim_output),
        efficiency_arr: numpy.array, shape: (n_dmu,),
            efficiency value of the DMU
        """

        if oriented in ("input", "output"):
            model_name = "CCR_batch_dual_{}_oriented".format(oriented)
        else:
            raise ValueError("unknown CCR dual oriented: {}".format(oriented))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # result arrays
        in_weight_arr = np.zeros((self.n_dmu, self.dim_input))
        out_weight_arr = np.zeros((self.n_dmu, self.dim_output))
        efficiency_arr = np.zeros(self.n_dmu)

        # decision variables
        instance.in_weight = Var(self.input_arr, within=NonNegativeReals)
        instance.out_weight = Var(self.output_arr, within=NonNegativeReals)

        # common expression
        in_sum_arr = [sum(instance.in_weight[idx] * self.X[rdx, idx]
                          for idx in self.input_arr)
                      for rdx in self.dmu_arr]
        out_sum_arr = [sum(instance.out_weight[jdx] * self.Y[rdx, jdx]
                           for jdx in self.output_arr)
                       for rdx in self.dmu_arr]

        # constraint
        def efficient_constraint_rule(model, rdx):
            return out_sum_arr[rdx] - in_sum_arr[rdx] <= 0.

        instance.efficient_constraint = Constraint(
            self.dmu_arr, rule=efficient_constraint_rule)

        # iterative all DMUs
        for dmu_idx in self.dmu_arr:
            if oriented == "input":
                # input model constraint
                def normalized_constraint_rule(model):
                    return in_sum_arr[dmu_idx] == 1.

                instance.normalized_constraint = Constraint(
                    rule=normalized_constraint_rule)

                # input objective
                def objective_rule(model):
                    return out_sum_arr[dmu_idx]

                instance.objective = Objective(rule=objective_rule,
                                               sense=maximize)

            elif oriented == "output":
                # output model constraint
                def normalized_constraint_rule(model):
                    return out_sum_arr[dmu_idx] == 1.

                instance.normalized_constraint = Constraint(
                    rule=normalized_constraint_rule)

                # output objective
                def objective_rule(model):
                    return in_sum_arr[dmu_idx]

                instance.objective = Objective(rule=objective_rule,
                                               sense=minimize)

            # solve
            instance = self._solve_model_feasible(instance, model_name)

            # extract result
            in_weight_arr[dmu_idx] = np.fromiter((
                instance.in_weight[idx].value
                for idx in self.input_arr), np.float)

            out_weight_arr[dmu_idx] = np.fromiter((
                instance.out_weight[jdx].value
                for jdx in self.output_arr), np.float)

            efficiency_arr[dmu_idx] = instance.objective()

            # delete constraint and resolve the problem
            instance.del_component("normalized_constraint")
            instance.del_component("objective")

        return {
            "model_name": model_name,
            "in_weight_arr": in_weight_arr,
            "out_weight_arr": out_weight_arr,
            "efficiency_arr": efficiency_arr,
        }

    @staticmethod
    def is_efficient(efficiency, in_slack, out_slack, precision=4):
        """
        Parameters
        -------------
            - efficiency, float, efficiency value of an DMU
            - in_slack, numpy.array, float, shape:(dim_input,)
            - out_slack, numpy.array, float, shape:(dim_output,)

        Returns
        --------------
        return 1: efficient frontier
               0: technical efficiency
               -1: inefficient
        """
        # technical efficiency
        if round(efficiency - 1, precision) == 0:
            slack_sum = in_slack.sum() + out_slack.sum()
            if round(slack_sum, precision) == 0:
                return 1
            else:
                return 0
        else:
            # inefficient
            return -1


class Temporal_CCR(AbstractTemporalDEAModel):

     def __init__(self, X, Y, relative_X, relative_Y, solver=DEFAULT_SOLVER):
        """
        Parameters
        -----------
        X: numpy.array, shape: (n_dmu, dim_input)
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        Y: numpy.array, shape: (n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        relative_X: None or numpy.array, shape: (n_dmu, dim_input)
        relative_Y: None or numpy.array, shape: (n_dmu, dim_output)
            both variables are for providing reference frontier for current
            data X and Y.

        solver: str,  the solver supported by pyomo (default: cplex)
        """
        super(Temporal_CCR, self).__init__(X, Y, relative_X, relative_Y,
                                                solver, "semi_positive")

     def solve_batch(self, oriented):

        if oriented in ("input", "output"):
            model_name = "InterTemporal_CCR_batch_{}_oriented".format(oriented)
        else:
            raise ValueError("unknown InterTemporal_CCR "
                             "batch oriented: {}".format(oriented))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # result arrays
        efficiency_arr = np.zeros(self.n_dmu)

        # decision variables
        instance.efficiency = Var(within=Reals)
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)

        # common expression
        frontier_X = [sum(self.relative_X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.relative_Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        # stage 1 common objective
        def stage1_objective_rule(model):
            return model.efficiency

        if oriented == "input":
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                                sense=minimize)
        elif oriented == "output":
            instance.objective1 = Objective(rule=stage1_objective_rule,
                                                sense=maximize)

        for dmu_idx in self.dmu_arr:
            if oriented == "input":
                # constraint
                def input_constraint_rule(model, idx):
                    return (model.efficiency * self.X[dmu_idx, idx] -
                            frontier_X[idx] >= 0.)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] <= 0.)

            elif oriented == "output":
                # constraint
                def input_constraint_rule(model, idx):
                    return (self.X[dmu_idx, idx] - frontier_X[idx] >= 0)

                # constraint
                def output_constraint_rule(model, jdx):
                    return (model.efficiency * self.Y[dmu_idx, jdx] -
                            frontier_Y[jdx]  <= 0)

            # build constraints
            instance.input_constraint = Constraint(
                    self.input_arr, rule=input_constraint_rule)
            instance.output_constraint = Constraint(
                    self.output_arr, rule=output_constraint_rule)

            # solve stage 1
            instance.objective1.activate()
            instance = self._solve_model_feasible(instance, model_name)

            # get efficiency
            efficiency = instance.efficiency.value

            # projection
            if oriented == "input":
                efficiency_arr[dmu_idx] = efficiency
            elif oriented == "output":
                efficiency_arr[dmu_idx] = 1./efficiency

            # delete constraint and resolve the problem
            instance.del_component("input_constraint")
            instance.del_component("input_constraint_index")
            instance.del_component("output_constraint")
            instance.del_component("output_constraint_index")

        return {
            "model_name": model_name,
            "efficiency_arr": efficiency_arr,
        }