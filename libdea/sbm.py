# -*- coding: utf-8 -*-
"""
Authors: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

from __future__ import division
import numpy as np
from pyomo.environ import *
from .basic_model import AbstractDEAModel
from . import (DEFAULT_SOLVER,)

class SBM(AbstractDEAModel):
    """ Slack-based measure """

    def __init__(self, X, Y, solver=DEFAULT_SOLVER):
        """
        Parameters
        -----------
        X: numpy.array, shape: (n_dmu, dim_input)
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        Y: numpy.array, shape: (n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        solver: str,  the solver supported by pyomo (default: cplex)
        """
        super(SBM, self).__init__(X, Y, solver, "positive")

    def solve_primal(self, dmu_idx, oriented=None):
        """
        because the primal input and the primal output model are very
        different from the primal model, we separate them in different function.

        Parameters:
        -------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {None, "input", "output"}

        Returns
        ---------
        model_name: str, model_name,
        dmu_idx: integer, index of the DMU,
        aux: float, auxiliary variable for solving SBM
        efficiency": float, efficiency value of the DMU
        intensity, numpy.array, shape:(n_dmu,),
            projection vector for input and output matrix to efficient frontier
        in_slack, numpy.array, shape:(dim_input,)
            distance of each dimension of the ith input from current position
            to the efficient frontier.
        out_slack, numpy.array, shape:(dim_output,)
            distance of each dimension of the jth output from current position
            to the efficient frontier.
        reference_dmu, list, reference DMUs on the efficient frontier.
        is_efficient, integer,
            - 1: efficient frontier
            - -1: inefficient
        projection_x, numpy.array, shape: (dim_input,)
        projection_y, numpy.array, shape: (dim_output,)
        """

        if oriented is None:
            model_name = "SBM_primal"
        elif oriented in ("input", "output"):
            # dispatch function
            return self._solve_primal_oriented(dmu_idx, oriented)
        else:
            raise ValueError("unknown SBM primal oriented: {}".format(oriented))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.auxiliary = Var(within=PositiveReals)
        instance.aux_intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.aux_in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.aux_out_slack = Var(self.output_arr, within=NonNegativeReals)

        # constraint
        def aux_out_slack_constraint_rule(model):
            out_slack_sum = sum(model.aux_out_slack[jdx] / self.Y[dmu_idx, jdx]
                                for jdx in self.output_arr)
            out_slack_mean = out_slack_sum / self.dim_output
            return model.auxiliary + out_slack_mean == 1

        instance.aux_out_slack_constraint = Constraint(
            rule=aux_out_slack_constraint_rule)

        # constraint
        def aux_input_constraint_rule(model, idx):
            aux_frontier = sum(self.X[rdx, idx] * model.aux_intensity[rdx]
                               for rdx in self.dmu_arr)
            return (model.auxiliary * self.X[dmu_idx, idx] -
                    model.aux_in_slack[idx] - aux_frontier == 0)

        instance.aux_input_constraint = Constraint(
            self.input_arr, rule=aux_input_constraint_rule)

        # constraint
        def aux_output_constraint_rule(model, jdx):
            aux_frontier = sum(self.Y[rdx, jdx] * model.aux_intensity[rdx]
                               for rdx in self.dmu_arr)
            return (model.auxiliary * self.Y[dmu_idx, jdx] - aux_frontier +
                    model.aux_out_slack[jdx] == 0)

        instance.aux_output_constraint = Constraint(
            self.output_arr, rule=aux_output_constraint_rule)

        def objective_rule(model):
            in_slack_sum = sum(model.aux_in_slack[idx] / self.X[dmu_idx, idx]
                               for idx in self.input_arr)
            in_slack_mean = in_slack_sum / self.dim_input
            return model.auxiliary - in_slack_mean

        instance.objective = Objective(rule=objective_rule, sense=minimize)

        # solve
        instance = self._solve_model_feasible(instance, model_name)

        # extract results
        aux = instance.auxiliary.value
        intensity = np.fromiter((
            instance.aux_intensity[rdx].value / aux
            for rdx in self.dmu_arr), np.float)
        reference_dmu = self.dmu_arr[intensity > 0]

        in_slack = np.fromiter((
            instance.aux_in_slack[idx].value / aux
            for idx in self.input_arr), np.float)
        out_slack = np.fromiter(
            (instance.aux_out_slack[jdx].value / aux
             for jdx in self.output_arr), np.float)

        efficiency = instance.objective()

        # data projection
        projection_x = self.X[dmu_idx] - in_slack
        projection_y = self.Y[dmu_idx] + out_slack

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "aux": aux,
            "intensity": intensity,
            "in_slack": in_slack,
            "out_slack": out_slack,
            "reference_dmu": reference_dmu,
            "efficiency": efficiency,
            "is_efficient": self.is_efficient(efficiency),
            "projection_x": projection_x,
            "projection_y": projection_y,
        }

    def solve_batch_primal(self, oriented=None):
        if oriented is None:
            model_name = "SBM_batch_primal"
        elif oriented in ("input", "output"):
            return self._solve_batch_primal_oriented(oriented)
        else:
            raise ValueError("unknown SBM batch primal oriented: {}".format(
                oriented))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # result arrays
        efficiency_arr = np.zeros(self.n_dmu)
        intensity_arr = np.zeros((self.n_dmu, self.n_dmu))
        reference_dmu_arr = []
        aux_arr = np.zeros(self.n_dmu)
        in_slack_arr = np.zeros((self.n_dmu, self.dim_input))
        out_slack_arr = np.zeros((self.n_dmu, self.dim_output))
        is_efficient_arr = np.zeros(self.n_dmu)
        projection_x_arr = np.zeros((self.n_dmu, self.dim_input))
        projection_y_arr = np.zeros((self.n_dmu, self.dim_output))

        # decision variables
        instance.auxiliary = Var(within=PositiveReals)
        instance.aux_intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.aux_in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.aux_out_slack = Var(self.output_arr, within=NonNegativeReals)

        # common data in constraint
        aux_frontier_X = [sum(self.X[rdx, idx] * instance.aux_intensity[rdx]
                              for rdx in self.dmu_arr)
                          for idx in self.input_arr]
        aux_frontier_Y = [sum(self.Y[rdx, jdx] * instance.aux_intensity[rdx]
                              for rdx in self.dmu_arr)
                          for jdx in self.output_arr]

        for dmu_idx in self.dmu_arr:
            # constraint
            def aux_out_slack_constraint_rule(model):
                out_slack_sum = sum(model.aux_out_slack[jdx] /
                                    self.Y[dmu_idx, jdx]
                                    for jdx in self.output_arr)
                out_slack_mean = out_slack_sum / self.dim_output
                return model.auxiliary + out_slack_mean == 1

            instance.aux_out_slack_constraint = Constraint(
                rule=aux_out_slack_constraint_rule)

            # constraint
            def aux_input_constraint_rule(model, idx):
                return (model.auxiliary * self.X[dmu_idx, idx] -
                        aux_frontier_X[idx] -
                        model.aux_in_slack[idx] == 0)

            instance.aux_input_constraint = Constraint(
                self.input_arr, rule=aux_input_constraint_rule)

            # constraint
            def aux_output_constraint_rule(model, jdx):
                return (model.auxiliary * self.Y[dmu_idx, jdx] -
                        aux_frontier_Y[jdx] +
                        model.aux_out_slack[jdx] == 0)

            instance.aux_output_constraint = Constraint(
                self.output_arr, rule=aux_output_constraint_rule)

            def objective_rule(model):
                in_slack_sum = sum(
                    model.aux_in_slack[idx] / self.X[dmu_idx, idx]
                    for idx in self.input_arr)
                in_slack_mean = in_slack_sum / self.dim_input
                return model.auxiliary - in_slack_mean

            instance.objective = Objective(rule=objective_rule, sense=minimize)

            # solve
            instance = self._solve_model_feasible(instance, model_name)

            # extract results
            aux = instance.auxiliary.value
            aux_arr[dmu_idx] = aux
            intensity_arr[dmu_idx] = np.fromiter((
                instance.aux_intensity[rdx].value / aux
                for rdx in self.dmu_arr), np.float)
            # reference_dmu = self.dmu_arr[intensity > 0]

            in_slack_arr[dmu_idx] = np.fromiter((
                instance.aux_in_slack[idx].value / aux
                for idx in self.input_arr), np.float)
            out_slack_arr[dmu_idx] = np.fromiter(
                (instance.aux_out_slack[jdx].value / aux
                 for jdx in self.output_arr), np.float)
            efficiency_arr[dmu_idx] = instance.objective()

            is_efficient_arr[dmu_idx] = self.is_efficient(
                efficiency_arr[dmu_idx])

            projection_x_arr[dmu_idx] = self.X[dmu_idx] - in_slack_arr[dmu_idx]
            projection_y_arr[dmu_idx] = self.Y[dmu_idx] + out_slack_arr[dmu_idx]

            # delete constraint and resolve the problem
            instance.del_component("aux_out_slack_constraint")
            instance.del_component("aux_input_constraint")
            instance.del_component("aux_input_constraint_index")
            instance.del_component("aux_output_constraint")
            instance.del_component("aux_output_constraint_index")
            instance.del_component("objective")

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "aux_arr": aux_arr,
            "intensity_arr": intensity_arr,
            "in_slack_arr": in_slack_arr,
            "out_slack_arr": out_slack_arr,
            "reference_dmu_arr": reference_dmu_arr,
            "efficiency_arr": efficiency_arr,
            "is_efficient_arr": is_efficient_arr,
            "projection_x_arr": projection_x_arr,
            "projection_y_arr": projection_y_arr,
        }

    def solve_dual(self, dmu_idx, oriented=None):
        """
        Parameters:
        -------------
        dmu_idx: integer, index of the DMU
        oriented: str,  {None, "input", "output"}

        Returns:
        ------------
        model_name: str, model_name,
        dmu_idx: integer, index of the DMU,
        in_weight: numpy.array, shape:(dim_input,),
        out_weight: numpy.array, shape:(dim_output,),
        efficiency": float, efficiency value of the DMU
        """
        if oriented is None:
            model_name = "SBM_dual"
        elif oriented in ("input", "output"):
            raise ValueError("SBM dual {} is not implemented.".format(oriented))
        else:
            raise ValueError("unknown SBM dual oriented: {}".format(oriented))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.in_weight = Var(self.input_arr, within=NonNegativeReals)
        instance.out_weight = Var(self.output_arr, within=NonNegativeReals)

        # common data
        in_sum_arr = [sum(instance.in_weight[idx] * self.X[rdx, idx]
                         for idx in self.input_arr)
                      for rdx in self.dmu_arr]
        out_sum_arr = [sum(instance.out_weight[jdx] * self.Y[rdx, jdx]
                          for jdx in self.output_arr)
                      for rdx in self.dmu_arr]

        # constraint
        def efficient_constraint_rule(model, rdx):
            return out_sum_arr[rdx] - in_sum_arr[rdx] <= 0

        instance.efficient_constraint = Constraint(
            self.dmu_arr, rule=efficient_constraint_rule)

        # constraint
        def input_constraint_rule(model, idx):
            return (model.in_weight[idx] -
                    1. / (self.dim_input * self.X[dmu_idx, idx]) >= 0)

        instance.input_constraint = Constraint(
            self.input_arr, rule=input_constraint_rule)

        # constraint
        def output_constraint_rule(model, jdx):
            return (model.out_weight[jdx] -
                    (1. - in_sum_arr[dmu_idx] + out_sum_arr[dmu_idx]) /
                    (self.dim_output * self.Y[dmu_idx, jdx]) >= 0)

        instance.output_constraint = Constraint(
            self.output_arr, rule=output_constraint_rule)

        def objective_rule(model):
            return out_sum_arr[dmu_idx] - in_sum_arr[dmu_idx]

        instance.objective = Objective(rule=objective_rule, sense=maximize)

        # solve
        instance = self._solve_model_feasible(instance, model_name)

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "in_weight": np.fromiter((
                instance.in_weight[idx].value
                for idx in self.input_arr), np.float),
            "out_weight": np.fromiter((
                instance.out_weight[jdx].value
                for jdx in self.output_arr), np.float),
            "efficiency": instance.objective(),
        }

    def solve_batch_dual(self, oriented=None):
        if oriented is None:
            model_name = "SBM_batch_dual"
        elif oriented in ("input", "output"):
            raise ValueError("SBM batch dual {} is not implemented.".format(
                oriented))
        else:
            raise ValueError("unknown SBM batch primal oriented: {}".format(
                oriented))

        # concrete model
        instance = ConcreteModel(name=model_name)

        # result arrays
        in_weight_arr = np.zeros((self.n_dmu, self.dim_input))
        out_weight_arr = np.zeros((self.n_dmu, self.dim_output))
        efficiency_arr = np.zeros(self.n_dmu)

        # decision variables
        instance.in_weight = Var(self.input_arr, within=NonNegativeReals)
        instance.out_weight = Var(self.output_arr, within=NonNegativeReals)

        # common data
        in_sum_arr = [sum(instance.in_weight[idx] * self.X[rdx, idx]
                         for idx in self.input_arr)
                      for rdx in self.dmu_arr]
        out_sum_arr = [sum(instance.out_weight[jdx] * self.Y[rdx, jdx]
                          for jdx in self.output_arr)
                      for rdx in self.dmu_arr]

        # common constraint
        def efficient_constraint_rule(model, rdx):
            return out_sum_arr[rdx] - in_sum_arr[rdx] <= 0

        instance.efficient_constraint = Constraint(
            self.dmu_arr, rule=efficient_constraint_rule)

        for dmu_idx in self.dmu_arr:
            # constraint
            def input_constraint_rule(model, idx):
                return (model.in_weight[idx] -
                    1. / (self.dim_input * self.X[dmu_idx, idx]) >= 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # constraint
            def output_constraint_rule(model, jdx):
                return (model.out_weight[jdx] -
                        (1. - in_sum_arr[dmu_idx] + out_sum_arr[dmu_idx]) /
                        (self.dim_output * self.Y[dmu_idx, jdx]) >= 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            def objective_rule(model):
                return out_sum_arr[dmu_idx] - in_sum_arr[dmu_idx]

            instance.objective = Objective(rule=objective_rule, sense=maximize)

            # solve
            instance = self._solve_model_feasible(instance, model_name)

            # extract result
            in_weight_arr[dmu_idx] = np.fromiter((
                instance.in_weight[idx].value
                for idx in self.input_arr), np.float)

            out_weight_arr[dmu_idx] = np.fromiter((
                instance.out_weight[jdx].value
                for jdx in self.output_arr), np.float)

            efficiency_arr[dmu_idx] = instance.objective()

            # delete constraint and resolve the problem
            instance.del_component("input_constraint")
            instance.del_component("input_constraint_index")
            instance.del_component("output_constraint")
            instance.del_component("output_constraint_index")
            instance.del_component("objective")

        return {
            "model_name": model_name,
            "in_weight_arr": in_weight_arr,
            "out_weight_arr": out_weight_arr,
            "efficiency_arr": efficiency_arr,
        }



    def _solve_primal_oriented(self, dmu_idx, oriented="input"):
        if oriented in ("input", "output"):
            model_name = "SBM_primal_{}_oriented".format(oriented)

        # concrete model
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # common constraint
        def input_constraint_rule(model, idx):
            frontier = sum(self.X[rdx, idx] * model.intensity[rdx]
                           for rdx in self.dmu_arr)
            return (self.X[dmu_idx, idx] - frontier - model.in_slack[idx] == 0)

        instance.input_constraint = Constraint(
            self.input_arr, rule=input_constraint_rule)

        # common constraint
        def output_constraint_rule(model, jdx):
            frontier = sum(self.Y[rdx, jdx] * model.intensity[rdx]
                           for rdx in self.dmu_arr)
            return (self.Y[dmu_idx, jdx] - frontier + model.out_slack[jdx] == 0)

        instance.output_constraint = Constraint(
            self.output_arr, rule=output_constraint_rule)

        if oriented == "input":
            def objective_rule(model):
                in_slack_sum = sum(model.in_slack[jdx] / self.X[dmu_idx, jdx]
                                   for jdx in self.input_arr)
                in_slack_mean = in_slack_sum / self.dim_input
                return 1. - in_slack_mean

            instance.objective = Objective(rule=objective_rule, sense=minimize)
        elif oriented == "output":
            def objective_rule(model):
                out_slack_sum = sum(model.out_slack[jdx] / self.Y[dmu_idx, jdx]
                                    for jdx in self.output_arr)
                out_slack_mean = out_slack_sum / self.dim_output
                return 1. + out_slack_mean

            instance.objective = Objective(rule=objective_rule, sense=maximize)

        # solve
        instance = self._solve_model_feasible(instance, model_name)

        # extract results
        intensity = np.fromiter((
            instance.intensity[rdx].value
            for rdx in self.dmu_arr), np.float)

        in_slack = np.fromiter((
            instance.in_slack[idx].value
            for idx in self.input_arr), np.float)
        out_slack = np.fromiter((
            instance.out_slack[jdx].value
            for jdx in self.output_arr), np.float)

        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "intensity": intensity,
            "in_slack": in_slack,
            "out_slack": out_slack,
            "efficiency": instance.objective(),
        }

    def _solve_batch_primal_oriented(self, oriented="input"):

        model_name = "SBM_batch_primal_{}_oriented".format(oriented)

        # concrete model
        instance = ConcreteModel(name=model_name)

        # result arrays
        efficiency_arr = np.zeros(self.n_dmu)
        intensity_arr = np.zeros((self.n_dmu, self.n_dmu))
        reference_dmu_arr = []
        in_slack_arr = np.zeros((self.n_dmu, self.dim_input))
        out_slack_arr = np.zeros((self.n_dmu, self.dim_output))

        # decision variables
        instance.intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.out_slack = Var(self.output_arr, within=NonNegativeReals)

        # common data in constraint
        frontier_X = [sum(self.X[rdx, idx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        frontier_Y = [sum(self.Y[rdx, jdx] * instance.intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        for dmu_idx in self.dmu_arr:
            # common constraint
            def input_constraint_rule(model, idx):
                return (self.X[dmu_idx, idx] - frontier_X[idx] -
                        model.in_slack[idx] == 0)

            instance.input_constraint = Constraint(
                self.input_arr, rule=input_constraint_rule)

            # common constraint
            def output_constraint_rule(model, jdx):
                return (self.Y[dmu_idx, jdx] - frontier_Y[jdx] +
                        model.out_slack[jdx] == 0)

            instance.output_constraint = Constraint(
                self.output_arr, rule=output_constraint_rule)

            if oriented == "input":
                def objective_rule(model):
                    in_slack_sum = sum(model.in_slack[jdx] /
                                       self.X[dmu_idx, jdx]
                                       for jdx in self.input_arr)
                    in_slack_mean = in_slack_sum / self.dim_input
                    return 1. - in_slack_mean

                instance.objective = Objective(rule=objective_rule,
                                               sense=minimize)
            elif oriented == "output":
                def objective_rule(model):
                    out_slack_sum = sum(model.out_slack[jdx] /
                                        self.Y[dmu_idx, jdx]
                                        for jdx in self.output_arr)
                    out_slack_mean = out_slack_sum / self.dim_output
                    return 1. + out_slack_mean

                instance.objective = Objective(rule=objective_rule,
                                               sense=maximize)
            # solve
            instance = self._solve_model_feasible(instance, model_name)

            # extract results
            intensity_arr[dmu_idx] = np.fromiter((
                instance.intensity[rdx].value
                for rdx in self.dmu_arr), np.float)
            reference_dmu_arr.append(self.dmu_arr[intensity_arr[dmu_idx] > 0])
            in_slack_arr[dmu_idx] = np.fromiter((
                instance.in_slack[idx].value
                for idx in self.input_arr), np.float)
            out_slack_arr[dmu_idx] = np.fromiter((
                instance.out_slack[jdx].value
                for jdx in self.output_arr), np.float)
            efficiency_arr[dmu_idx] = instance.objective()

            # delete constraint and resolve the problem
            instance.del_component("input_constraint")
            instance.del_component("input_constraint_index")
            instance.del_component("output_constraint")
            instance.del_component("output_constraint_index")
            instance.del_component("objective")

        return {
            "model_name": model_name,
            "efficiency_arr": efficiency_arr,
            "intensity_arr": intensity_arr,
            "in_slack_arr": in_slack_arr,
            "out_slack_arr": out_slack_arr,
            "reference_dmu_arr": reference_dmu_arr,
        }

    @staticmethod
    def is_efficient(efficiency, precision=4):
        """
        Parameters
        -------------
            - efficiency, float, efficiency value of an DMU
        Returns
        --------------
        return 1: efficient frontier
               -1: inefficient
        """
        # technical efficiency
        if round(efficiency - 1, precision) == 0:
            return 1
        else:
            return -1
