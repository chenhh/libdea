# -*- coding: utf-8 -*-
"""
Authors: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

from __future__ import division
import numpy as np
from pyomo.environ import *
from .basic_model import AbstractDEAModel
from . import (DEFAULT_SOLVER,)

class MPSS(AbstractDEAModel):
    """ It was proposed by Ahn, Charnes, and Cooper in 1989. """

    def __init__(self, X, Y, solver=DEFAULT_SOLVER):
        """
        Parameters
        -----------
        X: numpy.array, shape: (n_dmu, dim_input)
            - n_dmu is the number of data (DMU).
            - dim_input is the input dimension of an observation.

        Y: numpy.array, shape: (n_dmu, dim_output)
            - dim_output is the output dimension of an observation.

        solver: str,  the solver supported by pyomo (default: cplex)
        """
        super(MPSS, self).__init__(X, Y, solver, "semi_positive")

    def solve(self, dmu_idx, precision=4):
        """
        MPSS conditions:
        1. beta/alpha = 1 and
        2. slack sum = 0

        if there is any condition which is not satisfied in first stage
        optimization, the DMU must not a MPSS.

        if both conditions are satisfied in 1st optimization, it requires 2nd
        optimization with the same constraints as 1st, but fixed the variable
        alpha and beta, and we change the objective function to maximize the
        slack sum in 2nd optimization.

        if the slack sum in 2nd optimization is equal to zero, then the DMU
        is MPSS.
        """
        # concrete model
        model_name = "MPSS"
        instance = ConcreteModel(name=model_name)

        # decision variables
        instance.auxiliary = Var(within=PositiveReals)
        instance.aux_beta = Var(within=NonNegativeReals)
        instance.aux_intensity = Var(self.dmu_arr, within=NonNegativeReals)
        instance.aux_in_slack = Var(self.input_arr, within=NonNegativeReals)
        instance.aux_out_slack = Var(self.output_arr, within=NonNegativeReals)

        # common expression
        aux_frontier_X = [sum(self.X[rdx, idx] * instance.aux_intensity[rdx]
                          for rdx in self.dmu_arr)
                      for idx in self.input_arr]
        aux_frontier_Y = [sum(self.Y[rdx, jdx] * instance.aux_intensity[rdx]
                          for rdx in self.dmu_arr)
                      for jdx in self.output_arr]

        # constraint
        def input_constraint_rule(model, idx):
            return (self.X[dmu_idx, idx] - aux_frontier_X[idx] -
                    instance.aux_in_slack[idx] == 0)

        instance.input_constraint = Constraint(
            self.input_arr, rule=input_constraint_rule)

        # constraint
        def output_constraint_rule(model, jdx):
            return (model.aux_beta * self.Y[dmu_idx, jdx] -
                    aux_frontier_Y[jdx] + instance.aux_out_slack[jdx] == 0)

        instance.output_constraint = Constraint(
            self.output_arr, rule=output_constraint_rule)

        # constraint
        def cvx_boundary_constraint_rule(model):
            return (sum(model.aux_intensity[rdx] for rdx in self.dmu_arr) -
                    instance.auxiliary == 0)

        instance.cvx_boundary_constraint = Constraint(
            rule=cvx_boundary_constraint_rule)

        # objective
        def objective_rule(model):
            return model.aux_beta

        instance.objective = Objective(rule=objective_rule, sense=maximize)

        # solve
        instance = self._solve_model_feasible(instance, model_name)

        # extract results
        aux = instance.objective()
        intensity = np.fromiter((
            instance.aux_intensity[rdx].value / aux
            for rdx in self.dmu_arr), np.float)
        alpha = 1. / aux
        beta = instance.aux_beta.value / aux
        ratio = instance.objective()

        in_slack = np.fromiter(
            (instance.aux_in_slack[idx].value / aux
             for idx in self.input_arr), np.float)
        out_slack = np.fromiter(
            (instance.aux_out_slack[jdx].value / aux
             for jdx in self.output_arr), np.float)
        slack_sum = in_slack.sum() + out_slack.sum()

        # MPSS conditions
        if (round(instance.objective() - 1, precision) == 0 and
                    round(slack_sum, precision) == 0):
            # it requires 2nd optimization to ensure max_slack == 0

            # fixed variable
            instance.auxiliary.fixed = True
            instance.aux_beta.fixed = True

            # deactivate old objective
            instance.objective.deactivate()

            # new objective
            def objective2_rule(model):
                in_slack_sum = sum(model.aux_in_slack[idx]
                                   for idx in self.input_arr)
                out_slack_sum = sum(model.aux_out_slack[jdx]
                                    for jdx in self.output_arr)
                return in_slack_sum + out_slack_sum

            instance.objective2 = Objective(rule=objective2_rule,
                                            sense=maximize)
            # solve
            instance = self._solve_model_feasible(instance, model_name)

            # reset variables
            instance.auxiliary.fixed = False
            instance.aux_beta.fixed = False

            # extract results
            in_slack = np.fromiter(
                (instance.aux_in_slack[idx].value /aux
                 for idx in self.input_arr), np.float)
            out_slack = np.fromiter(
                (instance.aux_out_slack[jdx].value / aux
                 for jdx in self.output_arr), np.float)
            slack_sum = in_slack.sum() + out_slack.sum()

            if round(slack_sum, precision) == 0:
                is_MPSS = True
            else:
                is_MPSS = False
        else:
            is_MPSS = False

        # MPSS projection
        if is_MPSS:
            projection_x = alpha * self.X[dmu_idx] - in_slack
            projection_y = beta * self.Y[dmu_idx] + out_slack
        else:
            projection_x, projection_y = None, None


        return {
            "model_name": model_name,
            "dmu_idx": dmu_idx,
            "auxiliary": aux,
            "intensity": intensity,
            "alpha": alpha,
            "beta": beta,
            "ratio": ratio,
            "in_slack": in_slack,
            "out_slack": out_slack,
            "is_MPSS": is_MPSS,
            "projection_x": projection_x,
            "projection_y": projection_y,
        }
