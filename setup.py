#! /usr/bin/env python
"""
Authors: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

from distutils.core import setup

setup(
    name='libdea',
    version='0.1',
    packages=['libdea', 'libdea.test'],
    url='',
    license='GPLv2',
    author='Hung-Hsin Chen',
    author_email='chenhh@par.cse.nsysu.edu.tw',
    description='library for data envelopment analysis'
)
