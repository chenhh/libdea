libdea: A library for data envelopment analysis
=============================================

python packages
-----------------
 
* scientific computing
    - numpy 
    - pyomo
    - matplotlib (optional)
    
* test
    - pytest (optional)
    
* solver: cplex, gurobi, glpk, ipopt or other pyomo supported solvers.
    
   
 AMPL suffix
 ---------------
 http://www.ampl.com/NEW/suffbuiltin.html
 